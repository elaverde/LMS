/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lms;

import Jframes.configuracion;
import Jframes.frm_principal;
import Jframes.splash;
import clases.conexion.SqliteConexion;
import clases.configuration;
import clases.controFormulario.Lom;
import clases.controFormulario.LomGeneral;
import clases.utileriasLMS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author edi
 */
public class LMS {

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void lauchApp() throws SQLException {
        splash ventana = new splash();
        ventana.setVisible(true);
//        Statement stmt = null;
//        SqliteConexion nombre = SqliteConexion.iniConexion();
//        stmt = nombre.getConexion().createStatement();
//        ResultSet rs = stmt.executeQuery("SELECT name, sql FROM sqlite_master WHERE type='table' ORDER BY name;");
//        while (rs.next()) {
//            System.out.println(rs.getString("name"));
//        }
    }

    public static void main(String[] args) {
        try {

            configuration leer = new configuration();
            utileriasLMS leer2 = new utileriasLMS();

            String[] myconfig;
            myconfig = leer.getConfig();

            File wordSpace = new File(myconfig[0]);
            File bd = new File(myconfig[1]);
            File conf = new File(leer2.getURLIOConfig());
            System.out.println("Revision " + myconfig[0] + " Wordspace " + wordSpace.exists() + " " + myconfig[1] + " bd " + bd.exists());

            if (wordSpace.exists() && bd.exists() && conf.exists()) {
                lauchApp();
            } else {
                String[] opciones = {"Aceptar"};
                int opcion = JOptionPane.showOptionDialog(
                        null //componente
                        , "El sistema no detecta ninguna base de datos o no ha encontrado el espacio de trabajo para continuar por favor configúrelos" // Mensaje
                        , "Error Configuración" // Titulo en la barra del cuadro
                        , JOptionPane.DEFAULT_OPTION // Tipo de opciones
                        , JOptionPane.INFORMATION_MESSAGE // Tipo de mensaje (icono)
                        , null // Icono (ninguno)
                        , opciones // Opciones personalizadas
                        , null // Opcion por defecto
                );
                final configuracion confi = new configuracion();
                confi.setVisible(true);
                confi.jButton3.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        try {
                            // TODO add your handling code here:
                            File pat = new File(confi.jTextField1.getText());

                            if (Files.exists(pat.toPath())) {
                                if (confi.jTextField1.getText().length() != 0 && confi.jTextField2.getText().length() != 0) {
                                    configuration guardar = new configuration(confi.jTextField1.getText(), confi.jTextField2.getText());
                                    try {
                                        guardar.setConfig();
                                    } catch (IOException ex) {
                                        Logger.getLogger(configuracion.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    confi.setVisible(false);
                                } else {
                                    Object msj = "Por favor llene los campos";
                                    JOptionPane.showMessageDialog(null,
                                            msj, //Mensaje
                                            "Mensaje de Error", //Título
                                            JOptionPane.ERROR_MESSAGE);
                                }
                                lauchApp();
                            } else {
                                 Object msj = "La carpeta del proyecto no existe";
                                    JOptionPane.showMessageDialog(null,
                                            msj, //Mensaje
                                            "Mensaje de Error", //Título
                                            JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(LMS.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            }
        } catch (ClassNotFoundException ex) {
            /*   JOptionPane.showMessageDialog(null,
                                        ""+ex, //Mensaje
                                        "Mensaje de Error", //Título
                                        JOptionPane.ERROR_MESSAGE);*/
            System.out.println("aca" + ex);
        } catch (IOException ex) {
            /* JOptionPane.showMessageDialog(null,
                                        ""+ex, //Mensaje
                                        "Mensaje de Error", //Título
                                        JOptionPane.ERROR_MESSAGE);*/
            System.out.println("aca" + ex);
        } catch (SQLException ex) {
            /*     JOptionPane.showMessageDialog(null,
                                        ""+ex, //Mensaje
                                        "Mensaje de Error", //Título
                                        JOptionPane.ERROR_MESSAGE);*/
            System.out.println("aca" + ex);
        }
    }
}
