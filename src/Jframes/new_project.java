/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jframes;

import clases.DAO.ProjectDAO;
import clases.DTO.ArchivoDTO;
import clases.DTO.ProjectDTO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.ImageIcon;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author edi
 */
public class new_project extends javax.swing.JFrame {

    /**
     * Creates new form new_project
     */
    URL res;
    Map index_vals = new HashMap();
    public Map dataForm = new HashMap();
    private ArrayList<ArchivoDTO> arrArchiCreados= new ArrayList<>();
    private int idDelArchivoAlInsertarALABD=0;
    public void generated(){
        String name=name_ID().toString();
        ProjectDTO createProject = new ProjectDTO();
        ProjectDAO saveProject = new ProjectDAO();
        createProject.setname_pro(name);
        ProjectDTO read = (ProjectDTO) saveProject.read(createProject.getname_pro());
        System.out.println(read);
        if(read == null){
            jTextField1.setText(name);
            setChoice();
        }else{
            generated();
        }
    }
    public new_project() {
        initComponents();
        res = getClass().getResource("/templateJson/new_project.json");
        setIconImage(new ImageIcon(getClass().getResource("/img/logo_sena.png")).getImage());
        setLocationRelativeTo(null);
        generated();
        
    }

    //generamos codigo aletorio falta adicionar que se guarde base de datos y que ademas evalue que no se repita//
    public static Integer name_ID() {
        Random rnd = new Random();
        Integer nameID = 1000000 + rnd.nextInt(9000000);
        return nameID;
    }

    //agrupamos los datos del formulario//
    public void get_dates() {
        dataForm.put("id", jTextField1.getText());
        dataForm.put("ocupacional-text", choice1.getSelectedItem());
        dataForm.put("ocupacional-value", gatValChoice(choice1.getSelectedItem()));
    }

    //obtenemos el key del choice//
    public String gatValChoice(String value) {
        String valor = "";
        for (Object key : index_vals.keySet()) {
            if (index_vals.get(key).equals(value)) {
                valor = key.toString();
            }
        }
        return valor;
    }
    public void permitions(String url){
        final File file = new File(url);
        file.setReadable(true, false);
        file.setExecutable(true, false);
        file.setWritable(true, false);
    }
    public void create_folders(String folder, String proyecto) {
        JSONParser parser = new JSONParser();
        try {
            InputStream inputStream = res.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
            JSONObject jsonObject = (JSONObject) obj;
            boolean success = (new File(folder + "" + proyecto)).mkdirs();
            if (success) {
                permitions(folder + "" + proyecto);
                // Directory creation failed
                //creamos objetos pricipal//
                JSONObject datos = (JSONObject) jsonObject.get("organization");
                JSONObject values = (JSONObject) datos.get("folders");
                System.out.println(values);
                ArrayList<String> folders = new ArrayList<String>(values.keySet());

                for (int i = 0; i < folders.size(); i++) { 
                    boolean success2 = (new File(folder + proyecto + folders.get(i))).mkdirs();
                    arrArchiCreados.add(new ArchivoDTO(idDelArchivoAlInsertarALABD,  proyecto + folders.get(i)));////-<<<<<<<<<<<<<<<<<<<<<<<<<<
                    if (success2) {
                        permitions(folder + proyecto + folders.get(i));
                        JSONArray subfolders = (JSONArray) values.get(folders.get(i));
                        for (int j = 0; j < subfolders.size(); j++) {
                            arrArchiCreados.add(new ArchivoDTO(idDelArchivoAlInsertarALABD, proyecto + folders.get(i) + "/" + subfolders.get(j)));//////////->>>>>>>>>
                            boolean success3 = (new File(folder + proyecto + folders.get(i) + "/" + subfolders.get(j))).mkdirs();
                            if (success3) {
                                 permitions(folder + proyecto + folders.get(i) + "/" + subfolders.get(j));
                            }
                        }
                    }
                }
//                for (int i = 0; i < arrArchiCreados.size(); i++) {
//                    System.out.println(arrArchiCreados.get(i));
//                }
                
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArchivoDTO> getCarpetasCreadas(){  
        return arrArchiCreados;
    }
    //seteamos choice con los valores del json//
    public void setChoice() {
        JSONParser parser = new JSONParser();
        try {
            InputStream inputStream = res.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
            JSONObject jsonObject = (JSONObject) obj;
            //creamos objetos pricipal//
            JSONObject datos = (JSONObject) jsonObject.get("new_project");
            JSONArray values = (JSONArray) datos.get("ocupacional");
            for (int i = 0; i < values.size(); i++) {
                JSONObject dates = (JSONObject) values.get(i);
                for (int j = 0; j < dates.values().size(); j++) {
                    index_vals.put(dates.keySet().toArray()[j], dates.values().toArray()[j]);
                    choice1.add(dates.values().toArray()[j].toString());
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        choice1 = new java.awt.Choice();
        jButton1 = new javax.swing.JButton();

        setTitle("Nuevo Proyecto");

        jTextField1.setEditable(false);

        label1.setText("Código");

        label2.setText("Área Ocupacional");

        jButton1.setText("Crear Proyecto");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(choice1, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
                            .addComponent(jTextField1))))
                .addGap(23, 23, 23))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        label1.getAccessibleContext().setAccessibleName("Codigo");
        label1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:


    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(new_project.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(new_project.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(new_project.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(new_project.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new new_project().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Choice choice1;
    public javax.swing.JButton jButton1;
    private javax.swing.JTextField jTextField1;
    private java.awt.Label label1;
    private java.awt.Label label2;
    // End of variables declaration//GEN-END:variables
}
