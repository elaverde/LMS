/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jframes;

import clases.fileSize;
import clases.Comprimir;
import clases.DAO.ArchivoDAO;
import clases.DAO.GroupByFilesDAO;
import clases.DAO.MetadataDao;
import clases.DAO.ProjectDAO;
import clases.DAO.UsersDAO;
import clases.DTO.ArchivoDTO;
import clases.DTO.GroupByFilesDTO;
import clases.DTO.MetadataDTO;
import clases.DTO.ProjectDTO;
import clases.DTO.UsersDTO;
import clases.JTreeFile;
import clases.utileriasLMS;
import clases.controFormulario.LomGeneral;
import clases.controFormulario.PanelCatalog;
import clases.controFormulario.Validar;
import clases.guardarCargar.ImportarMetadata;
import clases.guardarCargar.ExportarMetadata;
import clases.guardarCargar.InfoFormulario;
import clases.metadataCreator;
import clases.New_Proyecto;
import clases.conexion.SessionUsuario;
import clases.guardarCargar.CargaMetadataBDArchivo;
import clases.guardarCargar.GuardarMetadataBDArchivo;
import clases.guardarCargar.VerificacionArchivo;
import clases.hilos.HiloTareaGuarda;
import clases.progressBar;
import clases.report_Required;
import clases.usuarios.Crear;
import clases.usuarios.Metodos;
import static clases.utileriasLMS.archivoENMemo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import javax.swing.JFileChooser;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author edi
 */
public class frm_principal extends javax.swing.JFrame {

    /**
     * Creates new form frm_principal
     */
    //directorio variable para almacenar url del folder o de file pero file es temporal
    String directorio;
    ArrayList listFiles = new ArrayList();
    JTreeFile jTreeFilesme = new JTreeFile();
    // private Formulario ventanaFormulario = new Formulario(jTreeFilesme);
    //variables formulario

    public String headName = "";
    private static ArrayList<Clasificacion> arrLom = new ArrayList<>();//cambio a compartir
    private ArrayList<PanelCatalog> arrPanel = new ArrayList<>();
    private int ofSetPanels = 10;
    private boolean ulrState = true;
    private String URLarchivoActual = "";
    private ArrayList<ArchivoDTO> arrArchivoMemTemp = new ArrayList<>();
    private int rol;
    boolean seleccion = false;
    //archivo y metadata vacio
    MetadataDTO metadtaVacia;

//    private String urlJson;
    //file
    URL res;
    utileriasLMS utils = new utileriasLMS();

    public void activarTextFields(JPanel j) {

    }

    public void diabled_element() {
        jMenuItem2.setEnabled(false);
        jMenuItem3.setEnabled(false);
        jMenuItem9.setEnabled(false);
        jScrollPane2.setVisible(false);
        jScrollPane3.setVisible(false);
        jScrollPane4.setVisible(false);
        jScrollPane5.setVisible(false);
        jScrollPane6.setVisible(false);
        jScrollPane7.setVisible(false);
        jScrollPane8.setVisible(false);
        jScrollPane9.setVisible(false);
        jScrollPane10.setVisible(false);
        jButton8.setEnabled(false);
        jButton9.setEnabled(false);
        jButton1.setEnabled(false);
    }

    public void enabled_element() {
        jMenuItem2.setEnabled(true);
        jMenuItem3.setEnabled(true);
        jMenuItem9.setEnabled(true);
        jScrollPane2.setVisible(true);
        jScrollPane3.setVisible(true);
        jScrollPane4.setVisible(true);
        jScrollPane5.setVisible(true);
        jScrollPane6.setVisible(true);
        jScrollPane7.setVisible(true);
        jScrollPane8.setVisible(true);
        jScrollPane9.setVisible(true);
        jScrollPane10.setVisible(true);
        jButton8.setEnabled(true);
        jButton9.setEnabled(true);
        jButton1.setEnabled(true);
    }

    public frm_principal() {
        initComponents();

        setIconImage(new ImageIcon(getClass().getResource("/img/logo_sena.png")).getImage());
        setLocationRelativeTo(null);


        
        jTreeFiles.setJTree(jTree1);
        jTreeFilesme.setJTree(jTree1);
        fileChooser.setDoubleBuffered(true);
        jTree1.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Selecciona...")));
        saveGroup.setVisible(false);
        //inica estad componentes
        //iniciamos el boton de gurdar metadatos de archivo en false

        // jTabbedPane1.setVisible(false);
        //jButton4.setEnabled(false);

        String homeDir = System.getProperty("user.home");

        // System.out.println(homeDir);
        Comprimir ii = new Comprimir();
        ii.UsersMetas();
        //creacion del formulario
        ////////////////////////////////////////

        res = getClass().getResource("/templateJson/template.json");
        ///para la clase Validar.....
//        urlJson = new File("src/templateJson/template.json").getAbsolutePath();

        arrLom.add(new Clasificacion("General", jPanel5, 0));
        arrLom.add(new Clasificacion("LifeCycle", jPanel13, 1));
        arrLom.add(new Clasificacion("Meta-Metadata", jPanel14, 2));
        arrLom.add(new Clasificacion("Technical", jPanel16, 3));
        arrLom.add(new Clasificacion("Educational", jPanel17, 4));
        arrLom.add(new Clasificacion("Rights", jPanel18, 5));
        arrLom.add(new Clasificacion("Relation", jPanel19, 6));
        arrLom.add(new Clasificacion("Annotation", jPanel20, 7));
        arrLom.add(new Clasificacion("Classification", jPanel21, 8));

        jPanel5.setBackground(java.awt.Color.WHITE);
        jPanel13.setBackground(java.awt.Color.WHITE);
        jPanel14.setBackground(java.awt.Color.WHITE);
        jPanel16.setBackground(java.awt.Color.WHITE);
        jPanel17.setBackground(java.awt.Color.WHITE);
        jPanel18.setBackground(java.awt.Color.WHITE);
        jPanel19.setBackground(java.awt.Color.WHITE);
        jPanel20.setBackground(java.awt.Color.WHITE);
        jPanel21.setBackground(java.awt.Color.WHITE);
        initPerfil();
        saveOnly.setVisible(false);
        metadtaVacia = new MetadataDTO(1234, getDatosForm());
        // System.out.println(metadtaVacia.getDatosMeta());
        diabled_element();

    }

    public void initPerfil() {
        // para almacenar los datos
        UsersDTO usuarioInicial = SessionUsuario.getSession();
        // System.out.println(" en frm_principal.java , en el constructor" + usuarioInicial);
        //usar los getter de UsersDTO
        //usuarioInicial.getId();
        // usuarioInicial.getName();
        if (usuarioInicial!=null) {
             Metodos usuario = new Metodos();
        rol = usuario.convertRol(usuarioInicial.getTipoUsuario());
        switch (rol) {
            case 1:
                /*jTabbedPane1.setEnabledAt(1, false);
                jTabbedPane1.setEnabledAt(3, false);
                jTabbedPane1.setEnabledAt(5, false);
                jTabbedPane1.setEnabledAt(6, false);*/
                jMenuItem8.setVisible(true);
                break;
            case 2:
                jTabbedPane1.setEnabledAt(1, false);
                jTabbedPane1.setEnabledAt(3, false);
                jTabbedPane1.setEnabledAt(6, false);
                jTabbedPane1.setEnabledAt(7, false);
                jTabbedPane1.setEnabledAt(8, false);
                jMenuItem8.setVisible(false);
                break;
            case 3:
                jTabbedPane1.setEnabledAt(0, false);
                jTabbedPane1.setEnabledAt(1, false);
                jTabbedPane1.setEnabledAt(2, false);
                jTabbedPane1.setEnabledAt(3, false);
                jTabbedPane1.setSelectedIndex(4);
                jTabbedPane1.setEnabledAt(5, false);
                jMenuItem8.setVisible(false);
                break;
            case 4:
                jTabbedPane1.setEnabledAt(0, false);
                jTabbedPane1.setSelectedIndex(1);
                jTabbedPane1.setEnabledAt(2, false);
                jTabbedPane1.setEnabledAt(4, false);
                jTabbedPane1.setEnabledAt(5, false);
                jTabbedPane1.setEnabledAt(6, false);
                jTabbedPane1.setEnabledAt(8, false);
                jMenuItem8.setVisible(false);
                break;

        }
        System.out.println("ROL: " + rol);
        }
       
    }

    ///get los datos de los formularios vacio
    public static Map getDatosForm() {
        HashMap datos = new HashMap();
        for (int i = 0; i < arrLom.size(); i++) {
            datos.putAll(arrLom.get(i).getDato());
        }
        return datos;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        id = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dni = new javax.swing.JLabel();
        ruta = new javax.swing.JLabel();
        tipoA = new javax.swing.JLabel();
        tamanoA = new javax.swing.JLabel();
        grupo = new javax.swing.JLabel();
        tipoA1 = new javax.swing.JLabel();
        tipoA2 = new javax.swing.JLabel();
        tipoA3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel13 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel14 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel17 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel18 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel19 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jPanel20 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jPanel21 = new javax.swing.JPanel();
        saveGroup = new javax.swing.JButton();
        saveOnly = new javax.swing.JButton();
        areaOcu = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jButton8 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jButton9 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LMS SENA");
        setBackground(new java.awt.Color(255, 255, 255));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        id.setText("Código:");

        jLabel2.setText("Área Ocupacional:");

        dni.setText("ID");

        ruta.setText("Ruta:");

        tipoA.setText("Tipo:");

        tamanoA.setText("Tamaño:");

        grupo.setText("Metadatos Agrupados:");

        tipoA1.setText("id Elemento:");

        tipoA2.setText("id Grupo:");

        tipoA3.setText("Tipo de Recurso:");

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setVerifyInputWhenFocusTarget(false);

        jScrollPane2.setAlignmentX(2.0F);
        jScrollPane2.setAlignmentY(2.0F);
        jScrollPane2.setMinimumSize(new java.awt.Dimension(23, 90));

        jPanel5.setLayout(null);
        jScrollPane2.setViewportView(jPanel5);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("General", jPanel4);

        jPanel6.setBackground(new java.awt.Color(189, 212, 222));

        jPanel13.setLayout(null);
        jScrollPane3.setViewportView(jPanel13);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Ciclo de Vida", jPanel6);

        jPanel7.setBackground(new java.awt.Color(189, 212, 222));

        jPanel14.setLayout(null);
        jScrollPane4.setViewportView(jPanel14);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Meta-Metadata", jPanel7);

        jPanel8.setBackground(new java.awt.Color(189, 212, 222));
        jPanel8.setVerifyInputWhenFocusTarget(false);

        jPanel16.setLayout(null);
        jScrollPane5.setViewportView(jPanel16);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Técnico", jPanel8);

        jPanel9.setBackground(new java.awt.Color(189, 212, 222));

        jPanel17.setLayout(null);
        jScrollPane6.setViewportView(jPanel17);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Educativo", jPanel9);

        jPanel10.setBackground(new java.awt.Color(189, 212, 222));

        jPanel18.setLayout(null);
        jScrollPane7.setViewportView(jPanel18);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Derechos", jPanel10);

        jPanel11.setBackground(new java.awt.Color(189, 212, 222));

        jPanel19.setLayout(null);
        jScrollPane8.setViewportView(jPanel19);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Relación", jPanel11);

        jPanel12.setBackground(new java.awt.Color(189, 212, 222));

        jPanel20.setLayout(null);
        jScrollPane9.setViewportView(jPanel20);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Anotación", jPanel12);

        jPanel15.setBackground(new java.awt.Color(189, 212, 222));

        jPanel21.setLayout(null);
        jScrollPane10.setViewportView(jPanel21);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 1285, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Clasificación", jPanel15);

        saveGroup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        saveGroup.setText("Generar Metadata a Grupo");
        saveGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveGroupActionPerformed(evt);
            }
        });

        saveOnly.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        saveOnly.setText("Guardar");
        saveOnly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveOnlyActionPerformed(evt);
            }
        });

        areaOcu.setText("None");

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/mas.png"))); // NOI18N
        jButton8.setText("Agregar Recurso");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jTree1.addTreeExpansionListener(new javax.swing.event.TreeExpansionListener() {
            public void treeCollapsed(javax.swing.event.TreeExpansionEvent evt) {
            }
            public void treeExpanded(javax.swing.event.TreeExpansionEvent evt) {
                jTree1TreeExpanded(evt);
            }
        });
        jTree1.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTree1ValueChanged(evt);
            }
        });
        jTree1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTree1PropertyChange(evt);
            }
        });
        jScrollPane1.setViewportView(jTree1);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/deleted.png"))); // NOI18N
        jButton9.setText("Eliminar Recurso");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        jButton1.setText("Exportar Selección");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(63, Short.MAX_VALUE))
        );

        jMenu1.setText("Archivo");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/new.png"))); // NOI18N
        jMenuItem4.setText("Nuevo Proyecto");
        jMenuItem4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem4MouseClicked(evt);
            }
        });
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/edit.png"))); // NOI18N
        jMenuItem7.setText("Abrir Proyecto");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/1463440273_Excel_D.png"))); // NOI18N
        jMenuItem2.setText("Exportar Metadata");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/1463440273_Excel_D.png"))); // NOI18N
        jMenuItem3.setText("Importar Metadata");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        jMenuItem9.setText("Exportar Proyecto");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem9);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/exit.png"))); // NOI18N
        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitEvent(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Cofiguración");

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/config.png"))); // NOI18N
        jMenuItem5.setText("Configuración Preferencias");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem5);

        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/profile.png"))); // NOI18N
        jMenuItem8.setText("Usuarios");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem8);

        jMenuBar1.add(jMenu4);

        jMenu2.setText("Ayuda");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/about.png"))); // NOI18N
        jMenu3.setText("Acerca de");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu2.add(jMenu3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(id)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dni, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(areaOcu, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ruta)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(tipoA, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tamanoA, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(tipoA1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tipoA3, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(grupo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tipoA2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(saveGroup)
                                .addGap(18, 18, 18)
                                .addComponent(saveOnly))
                            .addComponent(jTabbedPane1))
                        .addGap(10, 10, 10))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(ruta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tipoA)
                            .addComponent(tamanoA)
                            .addComponent(grupo)
                            .addComponent(tipoA3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tipoA1)
                            .addComponent(tipoA2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTabbedPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(areaOcu))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(id)
                                    .addComponent(dni))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2)
                                .addGap(33, 33, 33)
                                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveGroup)
                    .addComponent(saveOnly))
                .addGap(79, 79, 79))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //almacena si el usuario esta cargando por folder o files//
    public String operacion = "";

    private JTreeFile jTreeFiles = new JTreeFile();
    private JFileChooser fileChooser = new JFileChooser();
    private int poscicionCheck = 0;
    private int factorDistancia = 20;
    private File selectedFile;
    private String tipo;
    private boolean validarArchivo = false;
    fileSize tamano = new fileSize();
    double size;
    String extension;

    private void cargarARchivo(String value) {
        operacion = value;
        tipo = value;
//        System.out.println(value+"-------este es el value");
        if ("folder".equals(value)) {
            fileChooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
            int returnValue = fileChooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                jTreeFiles.init(selectedFile.getPath());
                directorio = selectedFile.getPath().toString();
                validarArchivo = true;
                ///
                //System.out.println(selectedFile.getPath().toString()+"-------este es el path");
                //System.out.println(FilenameUtils.getExtension(selectedFile.getPath().toString())+"-------este es el tipo");
                tamano.size(selectedFile.getPath().toString());
                size = tamano.size(selectedFile.getPath().toString());
                autoComplete(String.valueOf(size), 25);
            }
        } else {
            fileChooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
            int returnValue = fileChooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                directorio = selectedFile.getPath().toString();
                ///
                //System.out.println(selectedFile.getPath().toString()+"-------este es el path");
                System.out.println(FilenameUtils.getExtension(selectedFile.getPath().toString()) + "-------este es el tipo");
                tamano.size(selectedFile.getPath().toString());
                size = tamano.size(selectedFile.getPath().toString());
                autoComplete(String.valueOf(size), 25);
                extension = FilenameUtils.getExtension(selectedFile.getPath().toString());
                autoComplete(String.valueOf(extension), 24);

                if (jTreeFilesme.validate_archivo(listFiles, directorio)) {

                    System.out.println(selectedFile.getName());
                    if (selectedFile.getName().toString().equals("metadata.xml")) {
                        JOptionPane.showMessageDialog(null,
                                "Lo sentimos no puede agregar este archivo este nombre de archivo es reservado por el sistema", //Mensaje
                                "Mensaje de Error", //Título
                                JOptionPane.ERROR_MESSAGE); //Tipo de mensaje

                    } else {
                        validarArchivo = true;
                        listFiles.add(selectedFile.getPath());
                        jTreeFilesme.listme(listFiles);
                    }

                    //System.out.println(listFiles);
                } else {

                    JOptionPane.showMessageDialog(null,
                            "Lo sentimos no puede agregar este archivo", //Mensaje
                            "Mensaje de Error", //Título
                            JOptionPane.ERROR_MESSAGE); //Tipo de mensaje
                }
            }
        }

    }

    private void cargarARchivoXML(String value) {

        File selectedFile = new File(new File("").getAbsolutePath() + "/" + value);
        directorio = selectedFile.getAbsolutePath();
        System.out.println(selectedFile.getPath());
        if (jTreeFilesme.validate_archivo(listFiles, directorio)) {
            validarArchivo = true;
            listFiles.add(selectedFile.getPath());
            jTreeFilesme.listme(listFiles);
        } else {

            JOptionPane.showMessageDialog(null,
                    "Lo sentimos no puede agregar este archivo", //Mensaje
                    "Mensaje de Error", //Título
                    JOptionPane.ERROR_MESSAGE); //Tipo de mensaje

        }

    }

    public void validarBotonZIP() {

        if (XMLGenerado) {

        }
    }
    private boolean XMLGenerado = false;

    public void completeData(boolean value) {
        XMLGenerado = value;
    }
private int i=0;    //private String XMLUrl;
    public void setData(String value) {
        cargarARchivoXML(value);
    }

    public boolean eliminar(String url) {
        File paq = new File(url);
        if (paq.exists()) {
            paq.delete();
            return true;
        } else {
            return true;
        }

    }

    public void folders_list(String UrlWrite) {
        File dirme = new File(UrlWrite);
        File[] directions = dirme.listFiles();

        for (int l = 0; l < directions.length; l++) {

            listFiles.add(directions[l].toString());
            if (directions[l].isDirectory()) {
                folders_list(directions[l].toString());
                continue;
            }

        }
    }

    public void rePintarValidacion(int value) {
        arrPanel.get(value).cambioFondo(true);
        arrPanel.get(value).obtenerPanel().setBackground(new java.awt.Color(255, 50, 50));
        // System.out.println("Error P: " + value);
    }

    public void autoComplete(String data, int value) {
        //System.out.println(arrPanel.get(25)+"------>"+arrPanel.get(25).getId());
        arrPanel.get(value).setDato(data);
        // System.out.println("Error P: " + value);
    }

    public void rePintarDefault(int value) {
        arrPanel.get(value).cambioFondo(false);
        arrPanel.get(value).obtenerPanel().setBackground(new java.awt.Color(205, 217, 243));
        //System.out.println("Re P: " + value);
    }
    private int indexOld = 0;

    private void jTree1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTree1PropertyChange
        // TODO add your handling code here:
//        System.out.println("ya............");

    }//GEN-LAST:event_jTree1PropertyChange

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        // TODO add your handling code here:
        new creditos().setVisible(true);

    }//GEN-LAST:event_jMenu3MouseClicked

    public static TreePath findByName(JTree tree, String[] names) {
        TreeNode root = (TreeNode) tree.getModel().getRoot();
        return find(tree, new TreePath(root), names, 0);
    }

    private static TreePath find(JTree tree, TreePath parent, Object[] nodes, int depth) {
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        Object o = node;

        if (o.equals(nodes[depth])) {
            if (depth == nodes.length - 1) {
                return parent;
            }
            if (node.getChildCount() >= 0) {
                for (Enumeration e = node.children(); e.hasMoreElements();) {
                    TreeNode n = (TreeNode) e.nextElement();
                    TreePath path = parent.pathByAddingChild(n);
                    TreePath result = find(tree, path, nodes, depth + 1);
                    if (result != null) {
                        return result;
                    }
                }
            }
        }
        return null;
    }


    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        configuracion confi = new configuracion();
        confi.setVisible(true);

    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:

        File fichero = new File(utils.getURLIOConfig());
        if (!fichero.exists()) {
            configuracion confi = new configuracion();
            confi.setVisible(true);
            JOptionPane.showMessageDialog(null,
                    "Por favor configure la preferencias", //Mensaje
                    "Mensaje de Error", //Título
                    JOptionPane.ERROR_MESSAGE); //Tipo de mensaje
        }
    }//GEN-LAST:event_formWindowOpened
   
    

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        final add_resourse newResourse = new add_resourse();
        newResourse.bar.setVisible(false);
        newResourse.jLabel5.setVisible(false);
        newResourse.setVisible(true);
        
        newResourse.jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
              if(newResourse.mostrar())  {
             SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                 
                    if (newResourse.jTextField1.getText().length() != 0) {
                    newResourse.progress();
                    
                    try {
                        utileriasLMS utilname = new utileriasLMS();
                        GroupByFilesDAO groupGuardarBd = new GroupByFilesDAO();

                        File folder = new File(newResourse.jTextField1.getText());
                        if (folder.isDirectory()) {

                            int id;
                            GroupByFilesDTO groupAgregar = new GroupByFilesDTO(Integer.parseInt(utilname.getProject()), 1, newResourse.choice3.getSelectedItem().toString());
                            id = groupGuardarBd.insertAutoIndex(groupAgregar);
                            utils.copyFolder(folder, new File(utils.Get_workspace().toString() + File.separator + utils.getProject() + File.separator + newResourse.choice1.getSelectedItem() + File.separator + newResourse.choice2.getSelectedItem() + File.separator), id);

                        } else {//si es un archivo
                            int id;
                            GroupByFilesDTO groupAgregar = new GroupByFilesDTO(Integer.parseInt(utilname.getProject()), 0, newResourse.choice3.getSelectedItem().toString());
                            id = groupGuardarBd.insertAutoIndex(groupAgregar);
                            utils.copyFolder(folder, new File(utils.Get_workspace().toString() + File.separator + utils.getProject() + File.separator + newResourse.choice1.getSelectedItem() + File.separator + newResourse.choice2.getSelectedItem() + File.separator + folder.getName()), id);
                        }
                        System.out.println("Mi id insertado " + id);

                     String url = utils.Get_workspace().toString() + "/" + utils.getProject() + "/";
                        jTreeFiles.init(url);
                    } catch (Exception ex) {
                        Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   newResourse.setVisible(false);
                  
                   newResourse.jTextField1.setText("");
                    } else {
                        Object msj = "Por favor seleccione el documento o carpeta que desea adicionar";
                        JOptionPane.showMessageDialog(null,
                                msj, //Mensaje
                                "Mensaje de Error", //Título
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            });   
              }
       /*             SwingWorker<String, Object> worker = new SwingWorker<String, Object>() {
            @Override
            protected String doInBackground() throws Exception {                
                return ""; // call a REST API
            }
            @Override
            protected void done() {
                try {
                
            
        
                 newResourse.jLabel5.repaint();


                 newResourse.jLabel5.setText(result);
                } catch (Exception e) {
                    //ignore
                }
            }
        };      
        worker.execute();
                java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
              newResourse.jLabel5.setVisible(true);
            }
        });
                */
               

            }
        });
    }//GEN-LAST:event_jButton8ActionPerformed
///evento a cambiar seleccion de carpeta
    private ArchivoDTO archivoTempAnte = new ArchivoDTO();
    private ArchivoDTO archivoactual = new ArchivoDTO();
    private boolean primeraVez = true;

    private void jTree1ValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTree1ValueChanged
        if (jTree1.getSelectionPath() != null) {
            String jTreeVarSelectedPath = "";
            Object[] paths = jTree1.getSelectionPath().getPath();

            if (paths.length > 1) {
                for (int i = 1; i < paths.length; i++) {
                    jTreeVarSelectedPath += paths[i];
                    if (i + 1 < paths.length) {
                        jTreeVarSelectedPath += File.separator;
                    }
                }
            }
            if (paths.length > 3) {
                seleccion = true;
            } else {
                seleccion = false;
            }
            String Index = utils.Get_workspace() + File.separator + utils.getProject() + File.separator + jTreeVarSelectedPath;
//            System.out.println("valor "+utils.getProject() + File.separator + jTreeVarSelectedPath);
            //System.out.println(Index);
            saveGroup.setVisible(false);
            ArchivoDAO arcGuardarBd = new ArchivoDAO();
            ArchivoDTO arcRead;
            arcRead = (ArchivoDTO) arcGuardarBd.readGroup(utils.getProject() + File.separator + jTreeVarSelectedPath);
            //  System.out.println("My query " + utils.getProject() + File.separator + jTreeVarSelectedPath);
            if (arcRead != null) {

                saveOnly.setVisible(true);
                tipoA1.setText("id Elemento: " + Integer.toString(arcRead.getId_arc()));
                tipoA2.setText("id Grupo: " + Integer.toString(arcRead.getIdGbF_arc()));
                tipoA3.setText("Tipo de Recurso: " + arcRead.gettipoSource());
                if (arcRead.gettipo() == 1) {
                    saveGroup.setVisible(true);
                    grupo.setText("Metadatos Agrupados: Si");
                } else {
                    grupo.setText("Metadatos Agrupados: No");
                    saveGroup.setVisible(false);
                }

            } else {

                saveOnly.setVisible(false);
            }

            ruta.setText("Ruta: " + utils.Get_workspace() + File.separator + utils.getProject() + File.separator + jTreeVarSelectedPath);

            size = tamano.size(Index);

            tamanoA.setText("Tamaño: " + Double.toString(size));
            extension = FilenameUtils.getExtension(Index);
            tipoA.setText("Tipo: " + extension);

            //System.out.println(Index+"<---------INDEX--"+size+"--"+extension);
            //tendria q hacer un commit cada vez q se mueva o preguntar si se quire guardar antes de moverse de directorio
            URLarchivoActual = utils.getProject() + File.separator + jTreeVarSelectedPath;
            archivoactual = arcRead;
            //si es un archivo haga la consulta....

            File fileCompro = new File(utils.Get_workspace() + "\\" + utils.getProject() + File.separator + jTreeVarSelectedPath);
            System.out.println(utils.Get_workspace() + "\\" + utils.getProject() + File.separator + jTreeVarSelectedPath);
            if (fileCompro.getName().lastIndexOf('.') != -1 && arcRead == null) {
                VerificacionArchivo archBor = new VerificacionArchivo();
                archBor.borrarArchivo(fileCompro);
                System.out.println("El archivo no esta en la bd");
                String url = utils.Get_workspace().toString() + "/" + utils.getProject() + "/";
                jTreeFiles.init(url);

            }
//                if (fileCompro.getName().lastIndexOf('.') != -1 && !fileCompro.getName().equals("Project.lms")) {//si es archivo por la extencion
            if (actualizaMetadataDeArchivoVisualizadoBd(arcRead)) {///se guarda y se carga en BD                       

            }
//                } 

            autoComplete(String.valueOf(size), 25);
            autoComplete(String.valueOf(FilenameUtils.getExtension(Index)), 24);

        }
    }//GEN-LAST:event_jTree1ValueChanged
    private boolean actualizaMetadataDeArchivoVisualizadoBd(ArchivoDTO archi) {
        boolean seActualizo = false;
//        System.out.println(archi);
        if (archi != null) {//si es diferente de null se trata de aactualizar
            if (archivoTempAnte.getId_arc() != archi.getId_arc()) {//si cambie de archivo seleccionado
                MetadataDao metaGuarda = new MetadataDao();
                MetadataDTO metaguarda = new MetadataDTO(archivoTempAnte.getId_arc(), getDatosForm());
                seActualizo = metaGuarda.update(metaguarda);
            }

            archivoTempAnte = archi;
            MetadataDao metacarga = new MetadataDao();//me carga los emta de los archivos
            MetadataDTO meta = metacarga.read(archi.getId_arc());
            // System.out.println(meta.getDatosMeta());
            for (int i = 0; i < arrLom.size(); i++) {
                arrLom.get(i).setDatos(meta.getDatosMeta());
            }
        } else {//como es archivo q no esta en la bd los metadta se ponen vacios
            if (archivoTempAnte.getId_arc() != 0) {//si el archivo anterior no esta vacio , trate de actualizarlo
                MetadataDao metaGuarda = new MetadataDao();
                MetadataDTO metaguarda = new MetadataDTO(archivoTempAnte.getId_arc(), getDatosForm());
                seActualizo = metaGuarda.update(metaguarda);
            }

            archivoTempAnte = new ArchivoDTO();
            for (int i = 0; i < arrLom.size(); i++) {
                arrLom.get(i).setDatos(metadtaVacia.getDatosMeta());
            }
        }

        return seActualizo;
    }

    private boolean actualizaMetadataDeArchivoVisualizado() {///se guarda y se carga en BD,, sin uso
        for (ArchivoDTO archivo : archivoENMemo) {
            if (archivo.getUrlComp_arc().equals(URLarchivoActual)) {//
                System.out.println(archivoTempAnte.getId_arc() + ", " + archivo.getId_arc());
                if (archivoTempAnte.getId_arc() != archivo.getId_arc()) {//si cambie de archivo seleccionado
                    for (int i = 0; i < archivoENMemo.size(); i++) {
                        if (archivoENMemo.get(i).getUrlComp_arc().equals(archivoTempAnte.getUrlComp_arc())) {//busque el anterior archivo y gaurde los datos
                            //cargue la metadata
                            /// System.out.println("desde entra "+getDatosForm());
                            archivoENMemo.get(i).getMeta().setDatosMeta(getDatosForm());//guardelos en memo
                            //guardelos en bd
//                        System.out.println(archivoENMemo.get(i));

                        };//si es el mismo archivo         
                    }
                }

            };
        }
        //despues cambielos
        //guarda primera vez

        for (ArchivoDTO archivo : archivoENMemo) {
            if (archivo.getUrlComp_arc().equals(URLarchivoActual)) {//si es el mismo archivo 
                archivoTempAnte = archivo;
                if (archivo.getMeta() != null) {//tiene metadatos
                    if (archivo.getMeta().getDatosMeta() != null) {
                        for (int i = 0; i < arrLom.size(); i++) {
                            arrLom.get(i).setDatos(archivo.getMeta().getDatosMeta());
                        }
                    }
                }
            };
        }

        return true;
    }

    //iner class tarea 2
    class TareaCargar implements Runnable {

        @Override
        public void run() {
            ejecutar();
        }

        private synchronized void ejecutar() {
            for (ArchivoDTO archivo : archivoENMemo) {
                if (archivo.getUrlComp_arc().equals(URLarchivoActual)) {
                    //cargue la metadata
//                        System.out.println(archivo);
                    if (archivo.getMeta() != null) {//tiene metadatos
                        if (archivo.getMeta().getDatosMeta() != null) {
                            for (int i = 0; i < arrLom.size(); i++) {
                                arrLom.get(i).setDatos(archivo.getMeta().getDatosMeta());
                                System.out.println("ejecuta antes");
                            }
                        }

                    }

                };//si es el mismo archivo
            }
        }
    }
    private void jTree1TreeExpanded(javax.swing.event.TreeExpansionEvent evt) {//GEN-FIRST:event_jTree1TreeExpanded
        // TODO add your handling code here:
    }//GEN-LAST:event_jTree1TreeExpanded
    private boolean actualizarSinEvento() {//actualiza cuando se le da el boton de guardar , los guardados en memoria..
        for (int i = 0; i < archivoENMemo.size(); i++) {

            if (archivoENMemo.get(i).getUrlComp_arc().equals(URLarchivoActual)) {//

                archivoENMemo.get(i).getMeta().setDatosMeta(getDatosForm());//guardelos en memo
            };
        }
        return true;
    }

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
    }
    private void saveOnlyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveOnlyActionPerformed
        //si es nesesario???
        System.out.print("Mis datos " + getDatosForm());
        MetadataDao metacarga = new MetadataDao();
        MetadataDTO metaguarda = new MetadataDTO(archivoactual.getId_arc(), getDatosForm());
        boolean guarda = metacarga.update(metaguarda);
        System.out.println("guarda" + guarda);
        rePintarDefault(indexOld);
    }//GEN-LAST:event_saveOnlyActionPerformed

    private void saveGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveGroupActionPerformed
        // TODO add your handling code here:

        MetadataDao archivoGuarda = new MetadataDao();
        MetadataDTO metaAcarga = new MetadataDTO(archivoactual.getId_arc(), getDatosForm());
        archivoGuarda.updateInGroup(metaAcarga, archivoactual);

    }//GEN-LAST:event_saveGroupActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        try {
            String jTreeVarSelectedPath = "";
            Object[] paths = jTree1.getSelectionPath().getPath();
            System.out.println("esto es" + paths);
            if (paths.length > 1) {
                for (int i = 1; i < paths.length; i++) {
                    jTreeVarSelectedPath += paths[i];
                    if (i + 1 < paths.length) {
                        jTreeVarSelectedPath += File.separator;
                    }
                }

                String url = utils.Get_workspace() + File.separator + utils.getProject() + File.separator + jTreeVarSelectedPath;
                String urlP = utils.getProject() + File.separator + jTreeVarSelectedPath;
                metadataCreator datearXML = new metadataCreator();
                System.out.println("Mi url de busqueda" + urlP);
                report_Required report = datearXML.metadataCreatorSelectPartProject(urlP);
                //if retorna true si esta completo si no reporta false con sus repectiva infomacion anexada acontinuacion// 
                if (!report.data_valid()) {
                    //Reporte Para Nicolas//
                    rePintarDefault(indexOld);
                    rePintarValidacion(Integer.parseInt(report.get("index")));
                    indexOld = Integer.parseInt(report.get("index"));
                    jTabbedPane1.setSelectedIndex(arrPanel.get(indexOld).obtenerTab());
                    String error =  "El campo " + report.get("id") + " es requerido<br/>Archivo: "+report.get("url")+"<br/>Categoría: "+report.get("lom")+"<br/>" + "<p style='font-size:12px'>Ayuda:</p> " + report.get("help") + "\n";
                    JFrame jf=new JFrame("Error");
                    jf.setBackground(Color.BLACK);
                     jf.setSize(new Dimension(600,350));
                     Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                     jf.setLocation(dim.width/2-jf.getSize().width/2, dim.height/2-jf.getSize().height/2);
                     JLabel textoAyuda;
                     String[] arrHelpText = error.split(";");
                     textoAyuda = new JLabel("<html><body style='margin:10px;'><p style='font-size:12px'>Error:</p> "+arrHelpText[0]+"<br/></html>");
                     
                     JScrollPane scrollPane1 = new JScrollPane(textoAyuda);
                     scrollPane1.setSize(new Dimension(550,350));
                     JLabel labelBeingUsed = textoAyuda;
                     View view = (View) labelBeingUsed.getClientProperty(BasicHTML.propertyKey);
                     view.setSize(scrollPane1.getWidth(), 0.0f);
                     float w = view.getPreferredSpan(View.X_AXIS);
                     float h = view.getPreferredSpan(View.Y_AXIS);
                     labelBeingUsed.setSize((int) w, (int) h);

                     textoAyuda.setVerticalAlignment(JLabel.TOP);
                     jf.add(scrollPane1);

                     jf.setVisible(true);
                    
                    
                    //String error = "Error: el campo " + report.get("id") + " es requerido\n" + "Ayuda: " + report.get("help") + "\n";
                    //JOptionPane.showMessageDialog(null, error, "Error: Campo requerido en " + report.get("title"), JOptionPane.ERROR_MESSAGE);
                    System.out.println("---------------------------------------------------------------------------------------------\nFALTA: \nID: " + report.get("id"));
                    System.out.println("Help: " + report.get("help"));
                     System.out.println("Index: "+report.get("index"));
                    System.out.println("Tipo: " + report.get("tipo"));
                    System.out.println("LOM: " + report.get("lom"));
                    System.out.println("Title: " + report.get("title"));
                    System.out.println("Dirección: " + report.get("url") + "\n---------------------------------------------------------------------------------------------");
                } else {
                    rePintarDefault(0);
                    Comprimir zip = new Comprimir();
                    File dirObj = new File(url);
                    String texto = JOptionPane.showInputDialog(null, "Por favor ingresar el nombre del proyecto", "Nombre del proyecto", JOptionPane.QUESTION_MESSAGE);
                    if (texto != null && !texto.equals("")) {
                        
                      Object msj = "Estamos trabajando, por favor espere mientras generamos el zip";
                JOptionPane.showMessageDialog(null,
                        msj, //Mensaje
                        "Generando", //Título
                        JOptionPane.WARNING_MESSAGE);   
                    
                    String zipFileName = utils.get_publish_project() + texto + "_pqt.zip";
                    eliminar(zipFileName);
                    ZipOutputStream out = null;
                    System.out.println("La ruta" + zipFileName);
                    listFiles.removeAll(listFiles);

                    try {
                        out = new ZipOutputStream(new FileOutputStream(zipFileName));
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("Creating : " + zipFileName);
                    try {
                        if (dirObj.isDirectory()) {
                            folders_list(url);
                            listFiles.add(utils.Get_workspace() + File.separator + utils.getProject() + File.separator + "metadata.xml");
                            System.out.println(dirObj + "          Mi Absoluto");
                            zip.addFoldersSelect(dirObj.toString(), dirObj, listFiles, out);
                        } else {
                            listFiles.add(url);
                            listFiles.add(utils.Get_workspace() + File.separator + utils.getProject() + File.separator + "metadata.xml");
                            zip.addFiles(listFiles, out);
                        }
                        System.out.println("Mis archivos" + listFiles);

                        out.close();
                    } catch (IOException ex) {
                        Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                     Desktop.getDesktop().open(new File(utils.get_publish_project()));
                     }
                }
            } else {
                Object msj = "Con esta opción solo puede exportar una parte del proyecto no todo el proyecto";
                JOptionPane.showMessageDialog(null,
                        msj, //Mensaje
                        "Mensaje de Error", //Título
                        JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("ERRROR " + e);
        } finally {
            System.out.println("Instrucciones a ejecutar finalmente tanto si se producen errores como si no.");
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void exitEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitEvent
        // TODO add your handling code here:
        System.exit(1);
    }//GEN-LAST:event_exitEvent

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:
        metadataCreator datearXML = new metadataCreator();
        report_Required report = datearXML.metadataCreatorAllProject(utils.getProject());
        //if retorna true si esta completo si no reporta false con sus repectiva infomacion anexada acontinuacion//
        if (!report.data_valid()) {
            //Reporte Para Nicolas//
             rePintarDefault(indexOld);
                    rePintarValidacion(Integer.parseInt(report.get("index")));
                    indexOld = Integer.parseInt(report.get("index"));
                    jTabbedPane1.setSelectedIndex(arrPanel.get(indexOld).obtenerTab());
            String error =  "El campo " + report.get("id") + " es requerido<br/>Archivo: "+report.get("url")+"<br/>Categoría: "+report.get("lom")+"<br/>" + "<p style='font-size:12px'>Ayuda:</p> " + report.get("help") + "\n";
            JFrame jf=new JFrame("Error");
            jf.setBackground(Color.BLACK);
            jf.setSize(new Dimension(600,350));
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            jf.setLocation(dim.width/2-jf.getSize().width/2, dim.height/2-jf.getSize().height/2);
            JLabel textoAyuda;
            String[] arrHelpText = error.split(";");
            textoAyuda = new JLabel("<html><body style='margin:10px;'><p style='font-size:12px'>Error:</p> "+arrHelpText[0]+"<br/></html>");

            JScrollPane scrollPane1 = new JScrollPane(textoAyuda);
            scrollPane1.setSize(new Dimension(550,350));
            JLabel labelBeingUsed = textoAyuda;
            View view = (View) labelBeingUsed.getClientProperty(BasicHTML.propertyKey);
            view.setSize(scrollPane1.getWidth(), 0.0f);
            float w = view.getPreferredSpan(View.X_AXIS);
            float h = view.getPreferredSpan(View.Y_AXIS);
            labelBeingUsed.setSize((int) w, (int) h);

            textoAyuda.setVerticalAlignment(JLabel.TOP);
            jf.add(scrollPane1);

            jf.setVisible(true);

            //JOptionPane.showMessageDialog(null, "Error: el campo " + report.get("id") + " es requerido\n"
                //+ "Ayuda: " + report.get("help") + "\n", "Error: Campo requerido en " + report.get("title"), JOptionPane.INFORMATION_MESSAGE);
            System.out.println("---------------------------------------------------------------------------------------------\nFALTA: \nID: " + report.get("id"));
            System.out.println("Help: " + report.get("help"));
            System.out.println("Tipo: " + report.get("tipo"));
            System.out.println("LOM: " + report.get("lom"));
            System.out.println("Index: "+report.get("index"));
            System.out.println("Title: " + report.get("title"));
            System.out.println("Dirección: " + report.get("url") + "\n---------------------------------------------------------------------------------------------");
        } else {
            System.out.println("Debo Comprimir Esta todo en orden");
            Comprimir zip = new Comprimir();
            File dirObj = new File(utils.Get_workspace() + File.separator + utils.getProject());

            String texto = JOptionPane.showInputDialog(null, "Por favor ingresar el nombre del proyecto", "Nombre del proyecto", JOptionPane.QUESTION_MESSAGE);
            if (texto != null && !texto.equals("")) {
                           Object msj = "Estamos trabajando, por favor espere mientras generamos el zip";
                JOptionPane.showMessageDialog(null,
                        msj, //Mensaje
                        "Generando", //Título
                        JOptionPane.WARNING_MESSAGE);   
                String zipFileName =  utils.get_publish_project() + texto + "_pqt.zip";
                eliminar(zipFileName);
                ZipOutputStream out = null;
                listFiles.removeAll(listFiles);
                folders_list(utils.Get_workspace() + File.separator + utils.getProject());
                try {
                    out = new ZipOutputStream(new FileOutputStream(zipFileName));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Creating : " + zipFileName);
                try {
                    zip.addFolders(dirObj, listFiles, out);
                    Desktop.getDesktop().open(new File(utils.get_publish_project()));
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "Archives", "xlsx");

        JFileChooser file = new JFileChooser();
        file.setFileFilter(filter);

        file.addChoosableFileFilter(filter);
        file.setAcceptAllFileFilterUsed(false);
        file.showOpenDialog(this);
        File abre = file.getSelectedFile();
        if (abre != null) {
            Map datos = utils.importExcel(abre.getAbsolutePath());

            String url = datos.get("url-file").toString();

            String pattern = Pattern.quote(System.getProperty("file.separator"));
            String[] splittedFileName = url.split(pattern);

            List<String> myList = new ArrayList<String>();
            myList.add("Recurso SENA");
            for (int i = 1; i < splittedFileName.length; i++) {
                myList.add(splittedFileName[i]);
            }

            System.out.println(myList);
            if (archivoactual != null) {//saber si es otro archivo
                JLabel label1 = new JLabel("Importar metadata al Archivo:");
                //
                String[] buttons = {"Seleccionado", "Indicado en el Excel"};
                Object[] message = {"Seleccione:", label1};
                JOptionPane optionPane = new JOptionPane(message,
                    JOptionPane.QUESTION_MESSAGE,
                    JOptionPane.YES_NO_OPTION, null, buttons);

                JDialog dialog = optionPane.createDialog(this, "Importar");
                dialog.setDefaultCloseOperation(
                    JDialog.DO_NOTHING_ON_CLOSE);
                final List<String> myListTemp = myList;

                optionPane.addPropertyChangeListener(new PropertyChangeListener() {

                    @Override

                    public void propertyChange(PropertyChangeEvent evt) {
                        System.out.println(evt.getNewValue());
                        if (evt.getNewValue() != null) {
                            if (evt.getNewValue().equals("Archivo seleccionado")) {
                                //borra

                            }
                            if (evt.getNewValue().equals("Indicado en el Excel")) {
                                boolean correcto = utils.openFile(jTree1, myListTemp);
                                System.out.println("archivo ante" + archivoTempAnte);
                                if (correcto) {
                                    JOptionPane.showMessageDialog(null,
                                        "Se cargado el archivo correctamente",
                                        "Cargado", JOptionPane.INFORMATION_MESSAGE);
                                } else {
                                    JOptionPane.showMessageDialog(null,
                                        "Se cargado el archivo pero la url del archivo no se encontro",
                                        "Cargado", JOptionPane.INFORMATION_MESSAGE);
                                }

                            }
                        }

                    }
                });
                dialog.setVisible(true);
            } else {
                boolean correcto = utils.openFile(jTree1, myList);
                System.out.println("archivo ante" + archivoTempAnte);
                if (correcto) {
                    JOptionPane.showMessageDialog(null,
                        "Se cargado el archivo correctamente",
                        "Cargado", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,
                        "Se cargado el archivo pero la url del archivo no se encontro",
                        "Cargado", JOptionPane.INFORMATION_MESSAGE);
                }
            }

            for (int i = 0; i < arrLom.size(); i++) {
                arrLom.get(i).setDatos(datos);
            }
        }
        /*

        final TreePath[] paths = jTree1.getSelectionPaths();
        //Modifying the selection makes it run a whole lot faster
        jTree1.setSelectionPath(paths[0]);
        for(int i = 0; i < paths.length; i++){
            jTree1.expandPath(paths[i]);
            System.out.print(paths[i]);
        }

        */

        /*    String
        utils.openFile(jTree1,["",""]);
        */
        /*  ;

        jTree1.expandPath(path);
        //   jTree1.scrollPathToVisible(path);

        TreeModel      model = getModel();
        List<TreeNode> path  = new ArrayList<TreeNode>();
        if (model.getRoot() != null) {
            path.add((TreeNode)model.getRoot());
        }

        TreePath treePath = new TreePath(path.toArray(new TreeNode[path.size()]));
        */
        /*if (matcherCArgaArchivo.find()) {
            ImportarMetadata carga = new ImportarMetadata(abre);
            carga.obtenerArchivo();
            for (int i = 0; i < arrLom.size(); i++) {
                arrLom.get(i).setDatos(carga.getMap());
            }
        } else {
            JOptionPane.showMessageDialog(null,
                "Formato invalido",
                "El archivo debe tener extensión .xml, y creado con el aplicativo", JOptionPane.INFORMATION_MESSAGE);
        }*/
        //        if () {
            //
            //        }
        //        System.out.println(arrLom.get(0).getDato());
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        //unir todos los maps
        ///para el guardado de los datos un map general

        fileChooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
        if (utils.Get_workspace().isDirectory()) {
            fileChooser.setCurrentDirectory(utils.Get_workspace());
        }
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();

            if (seleccion == true) {
                String url = ruta.getText();
                url = url.replace("Ruta: ", "");
                url = url.replace(utils.Get_workspace() + File.separator, "");
                utils.exportExcel(utils.getProject() + "_1#" + url, getDatosForm(), selectedFile + File.separator);
                String open = selectedFile + File.separator;
                try {
                    Desktop.getDesktop().open(new File(open));
                } catch (IOException ex) {
                    Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        /*   InfoFormulario formulaInf = new InfoFormulario();

        String nombre = "";
        FileFilter filter = new FileNameExtensionFilter("XML file", "xml");

        JFileChooser file = new JFileChooser();
        file.addChoosableFileFilter(filter);
        file.setAcceptAllFileFilterUsed(false);
        file.showSaveDialog(this);
        File guarda = file.getSelectedFile();
        for (int i = 0; i < arrLom.size(); i++) {
            formulaInf.unirHashMap(arrLom.get(i).getDato());
        }
        if (guarda != null) {
            ExportarMetadata g = new ExportarMetadata("1023558695", formulaInf.getInfoForm(), "", guarda);
            g.generarArchivo();
        }
        */
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        final new_project nuevo = new new_project();
        nuevo.setVisible(true);

        nuevo.jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {

                nuevo.get_dates();
                dni.setText(nuevo.dataForm.get("id").toString());
                autoComplete(String.valueOf(nuevo.dataForm.get("id").toString()), 0);
                utils.setProject(nuevo.dataForm.get("id").toString());

                String area = nuevo.dataForm.get("ocupacional-text").toString() + " - " + nuevo.dataForm.get("ocupacional-value").toString();
                areaOcu.setText(area);
                utileriasLMS folder = new utileriasLMS();
                //  utils.CopiarDirectorio(new File("C:/Users/edi/Documents/error"),new File());
                if (folder.Get_workspace() != null) {
                    enabled_element();
                    nuevo.create_folders(folder.Get_workspace().toString() + "/", nuevo.dataForm.get("id").toString() + "/");
                    nuevo.setVisible(false);
                    New_Proyecto crear = new New_Proyecto(nuevo.dataForm.get("id").toString(), nuevo.dataForm.get("ocupacional-text").toString() + " - " + nuevo.dataForm.get("ocupacional-value").toString(), nuevo.dataForm.get("id").toString());
                    try {
                        crear.setWritten(nuevo.dataForm.get("id").toString());
                        String url = folder.Get_workspace().toString() + "/" + nuevo.dataForm.get("id").toString() + "/";
                        jTreeFiles.init(url);

                        //GUARDAR EN LA BASE DE DATOS//
                        ProjectDTO createProject = new ProjectDTO();
                        ProjectDAO saveProject = new ProjectDAO();
                        createProject.setname_pro(nuevo.dataForm.get("id").toString());
                        createProject.settipo_pro(area);
                        saveProject.create(createProject);

                        /*  arcAgregar.setIdGbF_arc(1212);//guarda id grup by files
                        arcAgregar.setUrlComp_arc(nuevoPat);//url desde el proyecto , crear funcion para dividirla o pasarle solo esa url...

                        arcGuardarBd.create(arcAgregar);//se inserta en la base de dtos*/
                        ///guarda carpetas en la memo cuando se crea el proyecto
                        for (int i = 0; i < nuevo.getCarpetasCreadas().size(); i++) {
                            //  archivoENMemo.add(nuevo.getCarpetasCreadas().get(i));
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    Object msj = "No exite un espacio de trabajo valido por favor cree uno valido en configuración de preferencias";
                    JOptionPane.showMessageDialog(null,
                        msj, //Mensaje
                        "Mensaje de Error", //Título
                        JOptionPane.ERROR_MESSAGE);

                }

            }
        });
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem4MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4MouseClicked

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        fileChooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "Archives", "lms");
        fileChooser.setFileFilter(filter);
        if (utils.Get_workspace().isDirectory()) {
            fileChooser.setCurrentDirectory(utils.Get_workspace());
        }
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            New_Proyecto ope = new New_Proyecto();
            String[] archivoLMS;
            try {
                archivoLMS = ope.getRead(selectedFile.toString());
                enabled_element();
                dni.setText(archivoLMS[0]);
                autoComplete(archivoLMS[0], 0);
                utils.setProject(archivoLMS[0]);
                String area = archivoLMS[1];
                areaOcu.setText(area);
                String url = utils.Get_workspace().toString() + "/" + utils.getProject() + "/";
                jTreeFiles.init(url);
                ///leer datos guardados de la bd por primera vez

                //antes de leer el metadata, mirar si el archivo existe , no necesario mirar en memoria..
                VerificacionArchivo verificaExistProye = new VerificacionArchivo(utils.Get_workspace().toString() + "\\", archivoLMS[0], this);

                    verificaExistProye.getArrExiste();

                    ///se actualiza el metadata en memora cuando se carga el proyecto
                    /*ArchivoDAO leerPrimeraVz = new ArchivoDAO();
                    List<ArchivoDTO> arrPrime = leerPrimeraVz.readAll();
                    for (ArchivoDTO archivoDTO : arrPrime) {
                        MetadataDao metTemp = new MetadataDao();
                        archivoENMemo.add(archivoDTO);
                        System.out.println(archivoDTO.getId_arc());
                        MetadataDTO tempMa = metTemp.read(archivoDTO.getId_arc());
                        if (tempMa != null) {
                            //System.out.println(tempMa);
                            archivoENMemo.get(archivoENMemo.size() - 1).setMeta(tempMa);//el ultimo q se ingresa actual
                            archivoENMemo.get(archivoENMemo.size() - 1).setIdMeta(archivoDTO.getId_arc());
                            //                        System.out.println("si tiene "+archivoENMemo.get(archivoENMemo.size() - 1));
                        } else {

                            archivoENMemo.get(archivoENMemo.size() - 1).getMeta().setIdarc_met(archivoDTO.getId_arc());//setea el id del meta en memoria
                            //                        archivoENMemo.get(archivoENMemo.size()-1).getMeta().setDatosMeta(getDatosForm());
                            //                                    System.out.println("no tiene"+archivoENMemo.get(archivoENMemo.size() - 1));
                        }
                    }*/
                    //                    for (ArchivoDTO archivoDTO : archivoENMemo) {//imprime para ver los archivos cargados
                        //                        System.out.println(archivoDTO);
                        //                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(frm_principal.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
        Crear registro = new Crear();
        registro.setVisible(true);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
        String jTreeVarSelectedPath = "";
        Object[] paths = jTree1.getSelectionPath().getPath();
        if (paths.length <= 3) {
            Object msj = "No puede eliminar archivos protegidos por el sistema";
            JOptionPane.showMessageDialog(null,
                    msj, //Mensaje
                    "Mensaje de Error", //Título
                    JOptionPane.ERROR_MESSAGE);
        } else {
            if (paths.length > 1) {
                for (int i = 1; i < paths.length; i++) {
                    jTreeVarSelectedPath += paths[i];
                    if (i + 1 < paths.length) {
                        jTreeVarSelectedPath += File.separator;
                    }
                }
            }
            File dir = new File(utils.Get_workspace() + File.separator + utils.getProject() + File.separator + jTreeVarSelectedPath);
            utils.deletedFolder(dir);
            System.out.println(dir);
            String url = utils.Get_workspace().toString() + "/" + utils.getProject() + "/";
            jTreeFiles.init(url);
        }

    }

    //clase interna para la generacin del formulario
    class Clasificacion {

        private int distancia = 0;
        private ArrayList<PanelCatalog> panelesCata = new ArrayList();
        private String calisifi;
        private JPanel panelPintar;
        private int aumenta = 0;
        private int index = 0;

        public Clasificacion(String calisifi, JPanel panelPintar, int tab) {
            this.calisifi = calisifi;
            this.panelPintar = panelPintar;
            this.index = tab;
            crerCatalgo();
        }

        private void crerCatalgo() {

            int agregar = 0;
            LomGeneral lom = new LomGeneral(this.calisifi, res);
            lom.leeJson();
            //lom.getCatalogos().size()
            for (int i = 0; i < lom.getCatalogos().size(); i++) {

                this.panelesCata.add(new PanelCatalog(lom.getCatalogos().get(i).getId(), lom.getCatalogos().get(i).getLabel(), lom.getCatalogos().get(i).getType(), lom.getCatalogos().get(i).getHelp(), lom.getCatalogos().get(i).getOpciones(), lom.getCatalogos().get(i).getRequired(), this.index));
                //System.out.println(distancia+" - di: "+ofSetPanels+ " au: "+this.aumenta+" ,su: "+agregar);  
                this.panelesCata.get(panelesCata.size() - 1).setLocation(0, distancia);
                this.panelPintar.add(this.panelesCata.get(this.panelesCata.size() - 1));
                this.aumenta++;
                //si es text area aumenta el tamaño                
                distancia = distancia + ofSetPanels + this.panelesCata.get(panelesCata.size() - 1).getTamaño()[1];
                // System.out.println(distancia);             
                arrPanel.add(this.panelesCata.get(i));

            }
            this.panelPintar.getAutoscrolls();
            this.panelPintar.setPreferredSize(new Dimension(200, distancia + 200));//<- hace q se escale dinamico....
            jScrollPane1.repaint();
            jScrollPane1.revalidate();
            this.panelPintar.repaint();
            this.panelPintar.revalidate();

        }

        //obtiene datos de la agrupacion de catalogos
        //sacamos del array ArrLom el primer lom q es general y sacamos los datos de ese lom general
        //ejemplo arrLom.get(0).getDato();
        public Map getDato() {
            Map mapTEmp = new HashMap();
            // System.out.println(this.panelesCata.size());
            for (PanelCatalog jPanelT : this.panelesCata) {

                mapTEmp.put(jPanelT.getId(), jPanelT.obtenerDato());
            }
            //System.out.println(mapTEmp);   
            return mapTEmp;
        }

        ///////prub
        public void setDatos(Map matp) {
            for (PanelCatalog jPanelT : this.panelesCata) {
                Iterator it = matp.entrySet().iterator();

                while (it.hasNext()) {
                    Map.Entry next = (Map.Entry) it.next();
                    if (next.getKey().toString().equals(jPanelT.getId())) {
                        if (next.getValue() == null) {//algunos datos salen nulll.....
                            jPanelT.setDato("");
                        } else {
                            jPanelT.setDato(next.getValue().toString());
                        }

                    }
                }
            }
        }

        public Map getDatoType() {
            Map mapType = new HashMap();
            // System.out.println(this.panelesCata.size());
            for (PanelCatalog jPanelT : this.panelesCata) {

                mapType.put(jPanelT.getId(), jPanelT.getType());
            }
            //System.out.println(mapType);   
            return mapType;
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frm_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frm_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frm_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frm_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new frm_principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel areaOcu;
    public javax.swing.JLabel dni;
    public javax.swing.JLabel grupo;
    private javax.swing.JLabel id;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    public javax.swing.JMenuItem jMenuItem2;
    public javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    public javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTree jTree1;
    private javax.swing.JLabel ruta;
    private javax.swing.JButton saveGroup;
    private javax.swing.JButton saveOnly;
    public javax.swing.JLabel tamanoA;
    private javax.swing.JLabel tipoA;
    private javax.swing.JLabel tipoA1;
    private javax.swing.JLabel tipoA2;
    private javax.swing.JLabel tipoA3;
    // End of variables declaration//GEN-END:variables
}
