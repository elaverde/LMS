/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jframes;

import clases.DAO.UsersDAO;
import clases.DTO.UsersDTO;
import clases.conexion.SessionUsuario;
import clases.usuarios.Crear;
import clases.usuarios.Mail;
import clases.usuarios.RecuperarContrasena;
import clases.usuarios.Validacion;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Multimedia
 */
public class mainFrame extends javax.swing.JFrame {

    /**
     * Creates new form mainFrame
     */
    public mainFrame() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/img/logo_sena.png")).getImage());
        setLocationRelativeTo(null);
        mouseactionlabel();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        correo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        contrasena = new javax.swing.JPasswordField();
        jButton3 = new javax.swing.JButton();
        fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1085, 584));
        setMinimumSize(new java.awt.Dimension(1085, 584));
        setPreferredSize(new java.awt.Dimension(1085, 584));
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("INICIO DE SESION");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(110, 150, 153, 22);

        correo.setText("prueba@prueba.com");
        getContentPane().add(correo);
        correo.setBounds(90, 210, 210, 19);

        jLabel6.setText("CORREO ELECTRÓNICO:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(90, 190, 210, 15);

        jLabel3.setText("CONTRASEÑA:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(90, 240, 210, 15);

        jButton1.setText("INGRESO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIngresoEvento(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(90, 290, 210, 39);

        contrasena.setText("prueba");
        contrasena.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contrasenaActionPerformed(evt);
            }
        });
        getContentPane().add(contrasena);
        contrasena.setBounds(90, 260, 210, 19);

        jButton3.setText("RECORDAR CONTRASEÑA");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passRestoreEvent(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(310, 260, 165, 25);

        fondo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/template.png"))); // NOI18N
        fondo.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        fondo.setAlignmentY(0.0F);
        getContentPane().add(fondo);
        fondo.setBounds(0, 0, 1085, 557);

        pack();
    }// </editor-fold>//GEN-END:initComponents
 private Map mapRegistoForm = new HashMap();
    
    private void botonIngresoEvento(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIngresoEvento
        
        Validacion validarForm = new Validacion();
        mapRegistoForm.put("correo", correo.getText());
        mapRegistoForm.put("contrasena", contrasena.getPassword());

        if (validarForm.camposValidacion(mapRegistoForm) && validarForm.vacioValidacionPass(contrasena.getPassword().length)) {
            System.out.println(mapRegistoForm);
            String passText = new String(contrasena.getPassword());

            //comprueba usuario
            UsersDAO compruebaPass = new UsersDAO();
            boolean validaPass = compruebaPass.readLogin(correo.getText(), passText);
            
            
            if (validaPass) {
                compruebaPass= new UsersDAO();
                UsersDTO usu= compruebaPass.readNombreUsu(correo.getText());
                SessionUsuario sesion= SessionUsuario.iniciarSession(usu);
                
                this.dispose();
                frm_principal frm = new frm_principal();
                frm.setVisible(true);
            } else {
                System.out.println("registrese");
                JOptionPane.showMessageDialog(this,
                                "Usuario no se encuentra registrado o no coincide el password y el nombre de usuario", //Mensaje
                                "Mensaje de Error", //Título
                                JOptionPane.ERROR_MESSAGE); //Tipo de mensaje
            }

        }
    }//GEN-LAST:event_botonIngresoEvento

    private void contrasenaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contrasenaActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_contrasenaActionPerformed

    private void passRestoreEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passRestoreEvent
        
        RecuperarContrasena frRC = new RecuperarContrasena();
        frRC.setVisible(true);
    }//GEN-LAST:event_passRestoreEvent
    void mouseactionlabel() {
        contrasena.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent arg0) {
                contrasena.setText("");
                //alert.setVisible(true);
            }

            public void mouseEntered(MouseEvent arg0) {
            }

            public void mouseExited(MouseEvent arg0) {
            }

            public void mousePressed(MouseEvent arg0) {
            }

            public void mouseReleased(MouseEvent arg0) {
            }
        });
    }

    public Map getDataForm() {
        return mapRegistoForm;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mainFrame().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField contrasena;
    private javax.swing.JTextField correo;
    private javax.swing.JLabel fondo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}
