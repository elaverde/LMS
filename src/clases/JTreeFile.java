/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author edi
 */
import clases.fileSize;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author coby
 */
public class JTreeFile implements TreeExpansionListener{

    private String urlMetadata;
    private JButton nuevo;
    private JTree jTree1;
    private DefaultTreeModel modelo;
    public String urlsBase;
    public String dirsList[][];
    DefaultMutableTreeNode top2 = new DefaultMutableTreeNode("Recurso SENA");
    DefaultTreeModel model = new DefaultTreeModel(top2);

    public void setUrlMetadata(String urlMetadata) {
        this.urlMetadata = urlMetadata;
    }

    public DefaultTreeModel getModelo() {
        return modelo;
    }

    public JTreeFile() {
    }

    public JTreeFile(JTree jTree1) {
        this.jTree1 = jTree1;
    }

    public void setJTree(JTree jTree1) {
        this.jTree1 = jTree1;
    }

    /**
     * Metodo que permite enlazar los escuchas de eventos y permite actualizar
     */
    
    public void listme(ArrayList<String> archivos) {

        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Recurso SENA");
        //creamos un modelo con el nodo que creamos principal

        jTree1.setModel(new DefaultTreeModel(top));
        jTree1.addTreeExpansionListener(this);

        for (int x = 0; x < archivos.size(); x++) {
            File archis = new File(archivos.get(x)); // current directory
            File[] files = archis.listFiles();
            DefaultMutableTreeNode insert = new DefaultMutableTreeNode(archis);
            top.add(insert);
            ///por esta linea no se expande
            jTree1.expandRow(x);
            jTree1.setSelectionRow(x);
            System.out.println(archis.getPath());
            //jTree1.setSelectionPath(insert);
        }
        
    
        

        

    }

    public void setButton(JButton b) {
        nuevo = b;
        nuevo.setEnabled(true);
    }

    public boolean validate_archivo(ArrayList<String> archivos, String url) {
        for (int x = 0; x < archivos.size(); x++) {
            if (archivos.get(x).trim().equals(url.trim())) {
                System.out.print("Duplicado");
                return false;
            }
        }
        return true;
    }
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
    
    public void init(String url) {
        //creamos el nodo principal
       
        File fi = new File(url); // current directory
        File[] files = fi.listFiles();
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Recurso SENA");
        //creamos un modelo con el nodo que creamos principal
        modelo = new DefaultTreeModel(top);
        jTree1.setModel(modelo);
        jTree1.addTreeExpansionListener(this);
        //extraemos todas las unidades disponibles en caso que tengamos C, D o otra
        if (fi.isDirectory()) {
            // seteamos el modelo y el escucha al componente 

            System.out.println(fi);
            fileSize tamano = new fileSize();
            //tamano.size(fi);
            System.out.println(fi.getParent() + " Get Parent");// /home/jigar/Desktop
            System.out.println(fi.getName() + " Get Name");
            urlsBase = fi.getParent();

            for (File f : files) {
                DefaultMutableTreeNode raiz = new DefaultMutableTreeNode(f.getName());
                //añadimos el nodo a la raiz
                if (f.isDirectory()) {

                    top.add(raiz);
                    //hacemos un recorrido de dos niveles a partir de cada una unidad
                    actualizaNodo(raiz, f);
                }
            }
            for (File f : files) {
                DefaultMutableTreeNode raiz = new DefaultMutableTreeNode(f.getName());
                //añadimos el nodo a la raiz
                if (f.isFile()) {
                    String ext =  FilenameUtils.getExtension(f.getAbsolutePath());
                    if(!ext.equals("lms")){
                    top.add(raiz);
                    //hacemos un recorrido de dos niveles a partir de cada una unidad
                    actualizaNodo(raiz, f);
                    }
                }
            }

        } else {
            //creamos un modelo con el nodo que creamos principal
            DefaultMutableTreeNode raiz = new DefaultMutableTreeNode(fi.getName());
            String ext =  FilenameUtils.getExtension(fi.getAbsolutePath());
                    System.out.println("esta es"+ext);
                    if(!ext.equals("lms")){
                    top.add(raiz);
                    }

        }
        jTree1.expandRow(0);
        jTree1.setSelectionRow(0);
    }

    
    
    private boolean actualizaNodo(DefaultMutableTreeNode nodo, File f) {
        //quitamos lo que tenga el nodo 
        nodo.removeAllChildren();
        //recursivamente mandamos actualizar
        return actualizaNodo(nodo, f, 2);
    }

    /**
     * A partir de un nodo enlista los archivos y los agrega como nodo
     *
     * @param nodo Es el nodo que tenemos parcialmente como raiz
     * @param f es el archivo que se enlaza con la raiz
     * @param profundidad el numero de subdirectorios que se quiere que escarbe
     * @return
     */
    private boolean actualizaNodo(DefaultMutableTreeNode nodo, File f, int profundidad) {
        Vector onlyfiles = new Vector();
        File[] files = f.listFiles(); // de el nodo que llega listamos todos sus archivos
        if (files != null && profundidad > 0) //permite detener la recursividad si ya llego al limite 
        {
            for (File file : files) // recorre todos los caroeta
            {

                DefaultMutableTreeNode nuevo = new DefaultMutableTreeNode(file.getName());
                if (file.isDirectory()) {
                    nodo.add(nuevo);
                }
                actualizaNodo(nuevo, file, profundidad - 1);
            }
            for (File file : files) // recorre todos los archivos 
            {
                if (file.isFile()) {
                    String ext =  FilenameUtils.getExtension(file.getAbsolutePath());
                    if(!ext.equals("lms")){
                    DefaultMutableTreeNode insert = new DefaultMutableTreeNode(file.getName());
                    nodo.add(insert);
                    }
                }
            }

        }
        return true;
    }

    /**
     * Metodo que se ejecuta al expandir alguno de los nodos
     *
     * @param event
     */
    

    @Override
    public void treeCollapsed(TreeExpansionEvent event) {
    }

    @Override
    public void treeExpanded(TreeExpansionEvent event) {
        TreePath path = event.getPath(); // Se obtiene el path del nodo
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
        //System.out.println(path);
        String jTreeVarSelectedPath = "";
        for (int i = 1; i < path.getPathCount(); i++) {
            jTreeVarSelectedPath += path.getPathComponent(i);
            if (i + 1 < path.getPathCount()) {
                jTreeVarSelectedPath += File.separator;
            }
        }
        utileriasLMS read =new utileriasLMS();
        // verifica que sea nodo valido
      //  if(node==null || !(node.getUserObject() instanceof File) ) return; 
        String url=read.Get_workspace()+File.separator+read.getProject()+File.separator+jTreeVarSelectedPath;
        File f =new File(url);
        actualizaNodo(node, f);  //actualiza la estructura
        JTree tree = ( JTree) event.getSource(); 
        DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
        model.nodeStructureChanged(node);
    }

}
