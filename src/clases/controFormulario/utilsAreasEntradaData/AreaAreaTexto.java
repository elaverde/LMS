/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import clases.MyVerifier;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

/**
 *
 * @author caano
 */
public class AreaAreaTexto extends JPanel implements AreaEntrada{
     private int[] tamaño = {960, 120};
     JTextArea longArea;
     private MyVerifier verifier = new MyVerifier();
    public AreaAreaTexto(String tipoDato) {
               // System.out.println(tipoDato);
        verifier.setTipo(tipoDato);
        construirPanel();
    }
    private void construirPanel() {
        this.setSize(tamaño[0], tamaño[1]);
        this.setLayout(null);
        this.setBackground(colorGeneral);
        longArea = new JTextArea(5, 20);
        longArea.setBounds(10, 10, 600, 100);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        longArea.setBorder(BorderFactory.createCompoundBorder(border, 
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
       // verifier.setFocusTextArea(longArea);
       // longArea.setInputVerifier(verifier);
        this.add(longArea);
      
    }

    @Override
    public String getDato() {
      return longArea.getText();
    }

    @Override
    public int[] getTamaño() {
       return tamaño;
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
    public void setDato(String dato){
        longArea.setText(dato);
    }
    
}
