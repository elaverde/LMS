/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import clases.MyVerifier;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author caano
 */
public class AreaAnadirOpciones extends JPanel implements AreaEntrada{
    JButton btonAgre;
    JTextField textfield1;
    JPanel opciAnadir;
    JButton btonElimi;
    private ArrayList<JCheckBox> nombGuar = new ArrayList<>();
    private  int[] tamaño= {960,200};
    private MyVerifier verifier = new MyVerifier();
    public AreaAnadirOpciones(String tipoDato) {
        verifier.setTipo(tipoDato);
        construirPanel();
    }
    private void construirPanel(){
          this.setSize(tamaño[0], tamaño[1]);   
        this.setLayout(null);
        this.setBackground(colorGeneral);
        btonAgre = new JButton("Añadir");
                btonAgre.setBounds(480, 10, 90, 30);
                textfield1 = new JTextField("", 20);
                textfield1.setBounds(10, 10, 450, 30);
                opciAnadir = new JPanel(new GridLayout(5, 5));
                opciAnadir.setBounds(10, 60, 500, 100);
                opciAnadir.setBackground(new Color(242,242,242));
                btonElimi = new JButton("Eliminar");
                btonElimi.setBounds(530, 90, 90, 30);
                this.add(textfield1);
                verifier.setFocusTextArea(textfield1);
                textfield1.setInputVerifier(verifier);
                this.add(btonAgre);
                this.add(opciAnadir);
                this.add(btonElimi);

//                this.add(textfield1);    
        
    }
    @Override
    public String getDato() {
        String temp="";
        for (int i = 0; i < nombGuar.size(); i++) {
             temp=temp+""+nombGuar.get(i).getText()+"::";              
            }         
        return temp;
    }
    public String getDatoText() {       
        return textfield1.getText();
    }
    
    public void setDato(String dato){
                String[] tokens= dato.split("::");
                //se elimina los que estaban
               opciAnadir.removeAll();
               nombGuar.clear();
               for (int i = 0; i < tokens.length; i++) {
                   agregarElmento(tokens[i]);
                }
               opciAnadir.repaint();
               opciAnadir.revalidate();
               textfield1.setText("");
    }

    @Override
    public int[] getTamaño() {
        return tamaño;
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
    public JButton getButtonAgregar() {    
            return this.btonAgre;       
    }

    public JButton getButonEliminar() {       
            return this.btonElimi;
    }

    public void agregarElmento(String opcion) {
        boolean agraga=true;
         for (int i = 0; i < nombGuar.size(); i++) {
             if (nombGuar.get(i).getText().equals(opcion)) {
                 agraga=false;
             }         
            }    
         if (agraga) {
             if (!opcion.equals("")) {
                 nombGuar.add(new JCheckBox(opcion));
                opciAnadir.add(nombGuar.get(nombGuar.size() - 1));
             }
        }
        
        opciAnadir.repaint();
        opciAnadir.revalidate();
        textfield1.setText("");
        //System.out.println("se añadio");
    }

    public void eliminarElemento() {
        System.out.println(nombGuar.size());;
        Boolean selec = false;
        do {
            selec = false;
            for (int i = 0; i < nombGuar.size(); i++) {
                JCheckBox jcheTmp = nombGuar.get(i);
                if (jcheTmp.isSelected()) {
                    System.out.println("se elimino" + nombGuar.get(i).getText());
                    opciAnadir.remove(jcheTmp);
                    nombGuar.remove(i);
                    selec = true;
                }
            }
        } while (selec);
        opciAnadir.repaint();
        opciAnadir.revalidate();
    }    
}
