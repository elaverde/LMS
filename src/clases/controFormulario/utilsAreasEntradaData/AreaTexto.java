/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JTextField;
import clases.MyVerifier;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author caano
 */
public class AreaTexto extends JPanel implements AreaEntrada {

    private JTextField textfield1;
    private MyVerifier verifier = new MyVerifier();
    private int[] tamaño = {960, 50};
    private String tipoDato = "";

    public AreaTexto(String tipoDato) {
        this.tipoDato = tipoDato;
        //System.out.println(tipoDato);
        verifier.setTipo(tipoDato);
        construirPanel();
    }

    @Override
    public String getDato() {
        return textfield1.getText();
    }

    public void setDato(String dato) {
        textfield1.setText(dato);
    }

    private void construirPanel() {
        this.setSize(tamaño[0], tamaño[1]);
        this.setLayout(null);
        this.setBackground(colorGeneral);
        textfield1 = new JTextField("", 20);
        textfield1.setBounds(10, 10, 600, 30);
        verifier.setFocusTextArea(textfield1);
        textfield1.setInputVerifier(verifier);
        this.add(textfield1);
        functiontipos(textfield1);
    }

    public int[] getTamaño() {
        return tamaño;
    }

    private void functiontipos(JTextField text) {

        switch (tipoDato) {
            case "Integer":
                text.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        char c = e.getKeyChar();
                        // System.out.println(c);
                        if (!Character.isDigit(c)) {
                            getToolkit().beep();
                            e.consume();
                        }
                    }
                });
                break;
            case "Float":
                text.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {

                        char c = e.getKeyChar();
                        // System.out.println(c);
                        if (textfield1.getText().contains(".") && c == '.') {
                            if (e.getKeyChar() != ('\b')) {
                                getToolkit().beep();
                            }

                            e.consume();
                        }
                        if (c != '.') {
                            if (!Character.isDigit(c)) {
                                if (e.getKeyChar() != ('\b')) {
                                    getToolkit().beep();
                                }
                                e.consume();
                            }
                        }

                    }
                });
                break;

        }

    }

    @Override
    public JPanel getPanel() {
        return this;
    }
}
