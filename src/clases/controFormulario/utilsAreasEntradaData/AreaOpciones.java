/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author caano
 */
public class AreaOpciones extends JPanel implements AreaEntrada {

    private int[] tamaño = {960, 50};
    private JComboBox entry;
    private Map opciones = new Hashtable();

    public AreaOpciones(Map opciones) {
        
        this.opciones=opciones;// 
        this.opciones.put("", "Seleccione");
        construirPanel();
    }

    private void construirPanel() {
        this.setSize(tamaño[0], tamaño[1]);
        this.setLayout(null);
        this.setBackground(colorGeneral);
        ArrayList<String> arrTemp = new ArrayList<>();
        arrTemp.add("Seleccione");
        
        for (Object string : opciones.values().toArray()) {
            if (!string.toString().equals("Seleccione")) {
                arrTemp.add(string.toString());
            }     
        }

//        System.out.println(arrTemp);
        entry = new JComboBox(arrTemp.toArray());
        entry.setBounds(10, 10, 400, 30);
        this.add(entry);
    }

    @Override
    public String getDato() {
        String temp = "";
        String tmpValue = "";

        tmpValue = entry.getSelectedItem().toString();
        if (!tmpValue.equals("Seleccione")) {
            for (Object key : opciones.keySet()) {
                if (opciones.get(key).equals(tmpValue)) {
                    temp = key.toString();
                }
            }
        }

        return temp;
    }

    @Override
    public int[] getTamaño() {
        return tamaño;
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    public void setDato(String key) {

        entry.setSelectedItem(opciones.get(key));

    }

}
