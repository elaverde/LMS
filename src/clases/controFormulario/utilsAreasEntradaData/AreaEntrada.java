/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author caano
 */
public interface AreaEntrada {
    String getDato();
    void setDato(String dato);
    int[] getTamaño();
    JPanel getPanel();
   Color colorGeneral= new Color(205,217,243);
    
}
