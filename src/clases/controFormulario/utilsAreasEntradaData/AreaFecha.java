/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario.utilsAreasEntradaData;

import static clases.controFormulario.utilsAreasEntradaData.AreaEntrada.colorGeneral;

import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author caano
 */
public class AreaFecha extends JPanel implements AreaEntrada{
     private int[] tamaño = {960, 50};
     JDateChooser jDateChooser1;
    public AreaFecha() {
        construirPanel();
    }
     
     private void construirPanel() {
        this.setSize(tamaño[0], tamaño[1]);
        this.setLayout(null);
        this.setBackground(colorGeneral);
          jDateChooser1 = new com.toedter.calendar.JDateChooser();
          jDateChooser1.setBounds(10, 10, 150, 30);
          Date temp= new Date();
          jDateChooser1.setDate(temp);
        this.add(jDateChooser1);
      
    }
     @Override
    public String getDato() {
        Date d= jDateChooser1.getDate();
        SimpleDateFormat formato= new SimpleDateFormat("dd/MM/yyyy");
//        System.out.println("en get dato"+formato.format(d));
      return formato.format(d);
    }

    @Override
    public int[] getTamaño() {
       return tamaño;
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
    public void setDato(String dato){
//                System.out.println("string dato "+dato);
        String[] tokens= dato.split("/");
//        System.out.println("in set en Fecha año: " + Integer.parseInt(tokens[2])+","+Integer.parseInt(tokens[1])+","+Integer.parseInt(tokens[0]));
        Calendar cal= Calendar.getInstance();
        //calendario enero es 0.........
        cal.set(Integer.parseInt(tokens[2]), Integer.parseInt(tokens[1])-1, Integer.parseInt(tokens[0]));
//        System.out.println(cal.getTime());
        jDateChooser1.setDate(cal.getTime());
    }
}
