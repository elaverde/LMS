/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author caano
 */
public class Catalogo {
    private String id;
    private String langString;
    private String type;
    private String label;
    private String obligatorio;
    private String restriccion;
    private String required;
     private String help;
     
    private Map opciones= new HashMap();
    public Catalogo(String id, String langString, String type, String help, String label, String required) {
        this.id = id;
        this.langString = langString;
        this.type = type;
        this.label = label;
        this.required = required;
        this.help = help;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLangString() {
        return langString;
    }

    public void setLangString(String langString) {
        this.langString = langString;
    }

    public String getType() {
        return type;
    }
    
    public String getHelp() {
        return help;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    @Override
    public String toString(){
    
    return "id:"+this.id+",lang:"+this.langString+"labe:"+this.label+"type:"+this.type+", opciones:"+this.opciones+", required:"+this.required;
    }
    public void setOpciones(Map opciones){
        this.opciones=opciones;        
    }
    public Map getOpciones(){

        return opciones;        
    }
    public void setObligatorio(String obligatorio) {
        this.obligatorio = obligatorio;
    }

    public void setRestriccion(String restriccion) {
        this.restriccion = restriccion;
    }   
    public String isObligatorio() {
        return obligatorio;
    }

    public String getRestriccion() {
        return restriccion;
    }
    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }
    
    
}
