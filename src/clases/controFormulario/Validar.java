/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import javax.swing.JOptionPane;

/**
 *
 * @author Usuario
 */
public class Validar {
    
    private int index;
    
    public boolean validoInfo(URL res, Map al, Map alType, String rol) {
        
        //JOptionPane.showMessageDialog(null, "Ente a la funcion valido");
        
        //BufferedWriter meta = null;
        //inicio lectura JSON//
        
	try {
                //JOptionPane.showMessageDialog(null, "Ente a la funcion valido");
                //preparamos archivo y lo parceamos para posterio lectura//
                Hashtable<String,String> titulo=new Hashtable<String,String>();
                titulo.put("General", "General");
                titulo.put("LifeCycle", "Ciclo de Vida"); 
                titulo.put("Meta-Metadata", "Meta-Metadata");
                titulo.put("Technical", "Técnico");
                titulo.put("Educational", "Educativo"); 
                titulo.put("Rights", "Derechos");
                titulo.put("Relation", "Relación");
                titulo.put("Annotation", "Anotación"); 
                titulo.put("Classification", "Clasificación");
                
               
                JSONParser parser = new JSONParser();
                InputStream inputStream = res.openConnection().getInputStream();
                Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
                JSONObject jsonObject = (JSONObject) obj;
                
                //creamos objetos pricipal//
                JSONObject metadatas = (JSONObject) jsonObject.get("metadatas");
                
                String catalog =(String) metadatas.get("catalog");
                //datos del metadato//
                JSONArray values= (JSONArray)  metadatas.get("datos");
                //apertura head y lom//
                //meta.write(header+lomO);
                //recorremos la arquitectura de los campos// 
                for (int i = 0; i < values.size(); i++) {
                    JSONObject dates = (JSONObject) values.get(i); 
                    //adicion del identificador//
                    String ide = (String) dates.get("id_meta");////<----------
                    String identificador =  catalog.replace("($)", ide); 
                    //adicion del valor de ingresado//
                    long index =   (Long) dates.get("index");
                    String descripcion = (String) dates.get("description");
                    String vals = (String) dates.get("value");////<---------
                    String req = (String) dates.get("required");
                    String rolVal = (String) dates.get("rol");
                    Boolean required = Boolean.valueOf(req);
                                        
                    //System.out.print(required);
                                     
                    String valor =  al.get(ide).toString();
                    String datoType =  alType.get(ide).toString(); 
                    if(valor.isEmpty() && required ==true && rolVal.indexOf(rol)>=0){
                        //System.out.print("es requerido");
                        this.index = i;
                        JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" es requerido",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                        return false;
                    }
                    
                   
                    
                    if(datoType.equals("Float")){
                        //System.out.println("es Float");
                        //JOptionPane.showMessageDialog(null, "es Float");
                        if(!isFloat(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" debe ser un numero de menos de 9 digitos",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                    if(datoType.equals("Integer")){
                        //System.out.println("es Integer");
                        //JOptionPane.showMessageDialog(null, "es Float");
                        if(!isInt(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" debe ser un numero de menos de 9 digitos",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                    
                    if(datoType.equals("ShortString")){
                        //System.out.println("es ShortString");
                        //JOptionPane.showMessageDialog(null, "es ShortString");
                        if(!isShortString(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" solo permite 100 caracteres",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                    if(datoType.equals("MediumString")){
                        //System.out.println("es MediumString");
                        //JOptionPane.showMessageDialog(null, "es MediumString");
                        if(!isMediumString(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" solo permite 255 caracteres",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                    if(datoType.equals("LongString")){
                        //System.out.println("es LongString");
                        //JOptionPane.showMessageDialog(null, "es LongString");
                        if(!isLongString(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" solo permite 1.000 caracteres",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                    
                    if(datoType.equals("Date")){
                        //System.out.println("es Date");
                        //JOptionPane.showMessageDialog(null, "es Date");
                        if(!isDate(valor)){
                            this.index = i;
                            JOptionPane.showMessageDialog(null, "Error: el campo "+ide+" es fecha en formato DD/MM/YYYY ejemplo '31/10/2016'",titulo.get(dates.get("lom")), JOptionPane.INFORMATION_MESSAGE);
                            return false;
                        }
                    }
                    
                
                }
                inputStream.close();
                 
                 
        } catch (FileNotFoundException e) {
                
		e.printStackTrace();
                return false;
	} catch (IOException e) {
		e.printStackTrace();
                return false;
	} catch (ParseException e) {
		e.printStackTrace();
                  return false;
	}
        //fin lectura JSON//
        
    return true;
     
    }
    
    public int getIndex() {
        
       
        return this.index;
    }
    
    
    public static boolean isInt(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        /*if (length == 0) {
            return false;
        }*/
        if (length > 9) {
            return false;
        }
        
        int i = 0;
        /*if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }*/
        
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                System.out.println(c);
                return false;
            }
        }
        return true;
    }

    public static boolean isFloat(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        /*if (length == 0) {
            return false;
        }*/
        if (length > 9) {
            return false;
        }
        
        int i = 0;
        /*if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }*/
        
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                if(c=='.' || c==','){
                    System.out.println(c);
                    return true;
                }
                else{
                    System.out.println(c);
                    return false;
                }
            }
        }
        return true;
    }
    
    public static boolean isShortString(String str) {
        
        int length = str.length();
        if (length > 100) {
            return false;
        }        
        return true;
    }
    
    
    public static boolean isMediumString(String str) {
        
        int length = str.length();
        if (length > 225) {
            return false;
        }        
        return true;
    }
    
    public static boolean isLongString(String str) {
        
        int length = str.length();
        if (length > 1000) {
            return false;
        }        
        return true;
    }
    
    public static boolean isDate(String str) {
        
        int length = str.length();
        System.out.println(length);
        if (length != 10) {
            return false;
        }
        
        for (int i=0; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                if(c=='/'){
                    System.out.println(c);
                    //return true;
                }
                else{
                    System.out.println(c);
                    return false;
                }
            }
        }
        
        System.out.println("str.charAt(2)-->"+str.charAt(2));
        System.out.println("str.charAt(5)-->"+str.charAt(5));
        if(str.charAt(2)=='/' && str.charAt(5)=='/'){
            //System.out.println("/ estan donde es");
        }
        else{
            return false;
        }
        
        if(str.charAt(0)>'3'){
            System.out.println("str.charAt(0)>3--->"+str.charAt(0));
            return false;
        }
        
        if(str.charAt(3)>'1'){
            System.out.println("str.charAt(3)>1--->"+str.charAt(3));
            return false;
        }
        
        if(str.charAt(3)=='1'){
            if(str.charAt(4)>'2'){
                System.out.println("str.charAt(4)>2--->"+str.charAt(4));
                return false;
            }
        }
        
        return true;
    }

    public boolean validoInfo(File u, HashMap datos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
