/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author caano
 */
public class PanelCatalogAnterior extends JPanel {

    private String idMetaText;
    private String idTituloText;
    private String idTypoText;
    private JButton btonAgre;
    private JButton btonElimi;
    private ArrayList<JCheckBox> nombGuar = new ArrayList<>();
    private JPanel opciAnadir;
    private Map opciones;
    private JComboBox entry;
    private JTextField textfield1;
    private JTextArea longArea;
    private Boolean tieneAreaGrande = false;

    public PanelCatalogAnterior(String idMetaText, String idTituloText, String idTypoText, Map opciones) {
        this.idMetaText = idMetaText;
        this.idTituloText = idTituloText;
        this.idTypoText = idTypoText;
        this.opciones = opciones;
        crearCatalogo();
    }

    private void crearCatalogo() {
        ///obtioene los datos de las opcions
//        String[] temp={ "Bird", "Cat", "Dog", "Rabbit", "Pig" };;

        this.setLayout(null);

        JLabel idTe = new JLabel("id meta:");
        idTe.setBounds(50, 0, 300, 30);
        JLabel idTeEntry = new JLabel(this.idMetaText);
        idTeEntry.setBounds(200, 0, 300, 30);

        JLabel tituTe = new JLabel("titulo:");
        tituTe.setBounds(65, 20, 300, 30);
        JLabel tituTeentry = new JLabel(this.idTituloText);
        tituTeentry.setBounds(200, 20, 300, 30);

        JLabel tipote = new JLabel("tipo dato:");
        tipote.setBounds(45, 40, 300, 30);
        JLabel tipoteEntry = new JLabel(this.idTypoText);
        tipoteEntry.setBounds(200, 40, 300, 30);
        JLabel entrada = new JLabel("entrada:");
        entrada.setBounds(0, 70, 300, 30);

        //genera la entrada de dato
        if (this.opciones.isEmpty()) {//sin o tiene opciones se una entrada de texto             
            if (this.idTypoText.equals("LongString")) {//si es textarea
                // System.out.println("long in "+ this.idMetaText);
                longArea = new JTextArea(5, 20);
                longArea.setBounds(50, 70, 600, 100);
                tieneAreaGrande = true;
                this.add(longArea);
            } else if (this.idTypoText.equals("")) {//si seleccion
                // System.out.println("long in "+ this.idMetaText);
                btonAgre = new JButton("Añadir");
                btonAgre.setBounds(510, 70, 90, 30);
                textfield1 = new JTextField("", 20);
                textfield1.setBounds(50, 70, 400, 30);
                opciAnadir = new JPanel(new GridLayout(5, 2));
                opciAnadir.setBounds(600, 10, 250, 100);
                opciAnadir.setBackground(Color.red);
                btonElimi = new JButton("Eliminar");
                btonElimi.setBounds(880, 70, 90, 30);
                this.add(textfield1);
                this.add(btonAgre);
                this.add(opciAnadir);
                this.add(btonElimi);

            } else {

                textfield1 = new JTextField("", 20);
                textfield1.setBounds(50, 70, 600, 30);
                this.add(textfield1);
            }
        } else {
            entry = new JComboBox(opciones.values().toArray());
            entry.setBounds(50, 70, 400, 30);
            this.add(entry);
        }
        if (tieneAreaGrande) {
            this.setSize(960, 200);
        } else {
            this.setSize(960, 120);
        }

        this.setBackground(Color.CYAN);
        this.add(idTe);
        this.add(idTeEntry);
        this.add(tituTe);
        this.add(tituTeentry);
        this.add(tipote);
        this.add(tipoteEntry);
        this.add(entrada);
//                    this.btonAgre.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("uno");
//            }         
//        });
    }

    //obetener la info del text
    public JTextField getTextfield1() {
        return textfield1;
    }

    //obetner la info de combobox
    public JComboBox getEntry() {
        return entry;
    }

    public String obtenerSoloDato() {
        String temp = "";
        String tmpValue = "";
        
        if (this.opciones.isEmpty()) {
            temp = textfield1.getText();
        } else {
            tmpValue = entry.getSelectedItem().toString();
            for (Object key : opciones.keySet()) {
                if (opciones.get(key).equals(tmpValue)) {
                    temp = key.toString();
                }
            }
        }
        return temp;
    }
    
    public String obtenerDato() {
        String temp = "";
        String tmpValue = "";
        
        if (this.opciones.isEmpty()) {
            temp = textfield1.getText();
        } else {
            tmpValue = entry.getSelectedItem().toString();
            for (Object key : opciones.keySet()) {
                if (opciones.get(key).equals(tmpValue)) {
                    temp = key.toString();
                }
            }
        }
        if (this.idTypoText.equals("")) {
            for (int i = 0; i < nombGuar.size(); i++) {
             temp=temp+""+nombGuar.get(i).getText()+"::";              
            }         
        }
        
        return temp;
    }

    public String getId() {
        return idMetaText;
    }

    public JButton getButtonAgregar() {
        if (this.btonAgre != null) {
            return this.btonAgre;
        }
        return null;
    }

    public JButton getButonEliminar() {
        if (this.btonElimi != null) {
            return this.btonElimi;
        }
        return null;
    }

    public void agregarElmento(String opcion) {
        nombGuar.add(new JCheckBox(opcion));
        opciAnadir.add(nombGuar.get(nombGuar.size() - 1));
        opciAnadir.repaint();
        opciAnadir.revalidate();
        textfield1.setText("");
        //System.out.println("se añadio");
    }

    public void eliminarElemento() {
        System.out.println(nombGuar.size());;
        Boolean selec = false;
        do {
            selec = false;
            for (int i = 0; i < nombGuar.size(); i++) {
                JCheckBox jcheTmp = nombGuar.get(i);
                if (jcheTmp.isSelected()) {
                    System.out.println("se elimino" + nombGuar.get(i).getText());
                    opciAnadir.remove(jcheTmp);
                    nombGuar.remove(i);
                    selec = true;
                }
            }
        } while (selec);
        opciAnadir.repaint();
        opciAnadir.revalidate();
    }
    

}
