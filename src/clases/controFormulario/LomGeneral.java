/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario;

import clases.controFormulario.Lom;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author caano
 */
//se le puede pasar el archivo.... mejor pasar el archivo y ya
public class LomGeneral implements Lom {

    private ArrayList<Catalogo> catalogos = new ArrayList();
    private Map listPorCatalogo = new HashMap();
    

    private String calificacion;
    private URL res;
    public LomGeneral(String calificacion,URL res) {
        
        this.res=res;
        this.calificacion=calificacion;
    }

  
    @Override
    public void leeJson() {
        JSONParser parser = new JSONParser();

        try {
            
            InputStream inputStream = this.res.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
            
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject metadatas = (JSONObject) jsonObject.get("metadatas");
            
            //System.out.println(metadatas);
            JSONArray values = (JSONArray) metadatas.get("datos");

            for (int i = 0; i < values.size(); i++) {
                JSONObject dates = (JSONObject) values.get(i);
                //adicion del identificador//
                String ide = (String) dates.get("lom");

                Pattern pat = Pattern.compile(this.calificacion);

                Matcher mat = pat.matcher(ide);
                String textLan = "";
                String type = "";
                String label = "";
                String idMeta = "";
                String required ="";
                String help ="";
                if (mat.find()) {
                    // System.out.println("id: " + ide);
                    String texto = (String) dates.get("value");
                    String val = (String) dates.get("description");
                    idMeta = (String) dates.get("id_meta");
                    required = (String) dates.get("required");
                    help = (String) dates.get("help");
                     //System.out.println(idMeta);
                     //System.out.println(idMeta+" ----->"+required);

                    //Pattern pattern = Pattern.compile("((<)|(</))(\\w*)(>)");
                    //encuentra lanstring
                    Pattern pattern = Pattern.compile("(>)(?!<)(.*)(</l)");
                    Matcher matcher = pattern.matcher(texto);
                    while (matcher.find()) {
                        textLan = matcher.group(2);
                    }
                    //encuentra type
                    Pattern pattern2 = Pattern.compile("(>)(?!<)(.*)(</t)");
                    Matcher matcher2 = pattern2.matcher(val);
                    while (matcher2.find()) {
                        type = matcher2.group(2);
                    }
                    //encuentra label
                    Pattern pattern3 = Pattern.compile("(l>)(?!<)(.*)(</l)");
                    Matcher matcher3 = pattern3.matcher(val);
                    while (matcher3.find()) {
                        label = matcher3.group(2);
                        //System.out.println(type);
                    }
                    catalogos.add(new Catalogo(idMeta, textLan, type, help, label, required));
                    //lista por catalogo
                    
                    ((Catalogo)(catalogos.get((catalogos.size()-1)))).setOpciones(this.getListPorCatalogo(idMeta));
                }
                 //System.out.println(ide+","+textLan+","+type+","+label);

            }
            //verificar datos catalogo
            for (int i = 0; i < catalogos.size(); i++) {
               // System.out.println(catalogos.get(i));
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            //manejo de error
            JOptionPane.showMessageDialog(null,
                    e, //Mensaje
                    "Direccion", //Título
                    JOptionPane.PLAIN_MESSAGE);
        } catch (IOException e) {
            //manejo de error
            JOptionPane.showMessageDialog(null,
                    e, //Mensaje
                    "Direccion", //Título
                    JOptionPane.PLAIN_MESSAGE);
        } catch (ParseException e) {
            //manejo de error
            JOptionPane.showMessageDialog(null,
                    e, //Mensaje
                    "Direccion", //Título
                    JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * @return the catalogos
     */
    public ArrayList<Catalogo> getCatalogos() {
        return catalogos;
    }

    /**
     * @return the listPorCatalogo
     */
    public Map getListPorCatalogo(String id) {
        JSONParser parser = new JSONParser();
        Map mapTEmp= new HashMap();
        try {
            InputStream inputStream = this.res.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));

            JSONObject jsonObject = (JSONObject) obj;
            JSONObject metadatas = (JSONObject) jsonObject.get("opciones");

            JSONArray values = (JSONArray) metadatas.get(id);
            if (values != null) {
               
                for (int i = 0; i < values.size(); i++) {
                    JSONObject dates = (JSONObject) values.get(i);

                    //System.out.println("encontro "+dates.values());
                    //System.out.println("encontro "+dates.keySet());
                    
                    for (int j = 0; j < dates.values().size(); j++) {
                        
                        mapTEmp.put(dates.keySet().toArray()[j], dates.values().toArray()[j]);
                    }
                  
                }
                
                //System.out.println(this.listPorCatalogo.get((this.listPorCatalogo.size()-1)).size());
                Iterator it = mapTEmp.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = (Map.Entry) it.next();
                    //System.out.println("[" + e.getKey() + "=" + e.getValue() + "]");
                }
            }
            

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LomGeneral.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LomGeneral.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(LomGeneral.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mapTEmp;
    }

}
