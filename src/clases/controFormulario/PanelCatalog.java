/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.controFormulario;

import clases.controFormulario.utilsAreasEntradaData.AreaTexto;
import clases.controFormulario.utilsAreasEntradaData.AreaEntrada;
import clases.controFormulario.utilsAreasEntradaData.AreaAnadirOpciones;
import clases.controFormulario.utilsAreasEntradaData.AreaAreaTexto;
import clases.controFormulario.utilsAreasEntradaData.AreaFecha;
import clases.controFormulario.utilsAreasEntradaData.AreaOpciones;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;

/**
 *
 * @author caano
 */
public class PanelCatalog extends JPanel {

    private String idMetaText;
    private String idTituloText;
    private String idTypoText;
    private String idHelpText;
    private String usuModi="";
    private Date fechamod= new Date();
    private Map opciones;
    private Boolean required;
    AreaEntrada entradaUsu;
    private int[] tamaño=new int[2];
    private int tab;

    

    public PanelCatalog(String idMetaText, String idTituloText, String idTypoText, String idHelpText, Map opciones, String required,int tab) {
        this.idMetaText = idMetaText;
        this.idTituloText = idTituloText;
        this.idTypoText = idTypoText;
        this.idHelpText = idHelpText;
        this.opciones = opciones;
        this.required = Boolean.valueOf(required);
        this.tab = tab;
        crearCatalogo();
    }

    private void crearCatalogo() {
        ///obtioene los datos de las opcions
//        String[] temp={ "Bird", "Cat", "Dog", "Rabbit", "Pig" };;

        this.setLayout(null);
        String entra="";
        if(this.required == true){
      //  JLabel reqLabel = new JLabel("Requerido *");
       // reqLabel.setBounds(0,90,300,30);
       // this.add(reqLabel);
       
        entra = " * Entrada:";
        }else{
         entra = " Entrada:";    
        }

        JLabel idTe = new JLabel("ID:",SwingConstants.LEFT);
        idTe.setBounds(50, 0, 300, 30);
        JLabel idTeEntry = new JLabel(this.idMetaText);
        idTeEntry.setBounds(200, 0, 300, 30);

        JLabel tituTe = new JLabel("Título:",SwingConstants.LEFT);
        tituTe.setBounds(50, 20, 300, 30);
        JLabel tituTeentry = new JLabel(this.idTituloText);
        tituTeentry.setBounds(200, 20, 300, 30);

        JLabel tipote = new JLabel("Tipo de dato:",SwingConstants.LEFT);
        tipote.setBounds(50, 40, 300, 30);
        JLabel tipoteEntry = new JLabel(this.idTypoText);
        tipoteEntry.setBounds(200, 40, 300, 30);
        
        JLabel entrada = new JLabel(entra,SwingConstants.LEFT);
        entrada.setBounds(0, 70, 300, 30);
        
       ImageIcon image = new ImageIcon(getClass().getResource("/img/ayudaIcon.png"));
       
       if(!"".equals(this.idHelpText)){
            JLabel ayuda = new JLabel("", image, JLabel.LEFT);
            ayuda.setBounds(60, 70, 300, 30);
            ayuda.setCursor(new Cursor(Cursor.HAND_CURSOR));
            this.add(ayuda);
           // System.out.println(idHelpText);
            ayuda.addMouseListener(new MouseAdapter()  
            {  
                 public void mouseClicked(MouseEvent e)  
                 {  
                     JFrame jf=new JFrame("Ayuda");
                     jf.setBackground(Color.BLACK);
                     jf.setSize(new Dimension(600,350));
                     Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                     jf.setLocation(dim.width/2-jf.getSize().width/2, dim.height/2-jf.getSize().height/2);
                     JLabel textoAyuda;
                     String[] arrHelpText = idHelpText.split(";");
                     if(arrHelpText.length>1){
                         textoAyuda = new JLabel("<html><body style='margin:10px;'><p style='font-size:12px'>Descripción:</p> "+arrHelpText[0]+"<br/><br/><p style='font-size:12px'>Descripción del carácter:</p> "+arrHelpText[1]+"</body></html>");
                     }else{
                         textoAyuda = new JLabel("<html><body style='margin:10px;'><p style='font-size:12px'>Descripción:</p> "+arrHelpText[0]+"<br/></html>");
                     }
                     JScrollPane scrollPane1 = new JScrollPane(textoAyuda);
                     scrollPane1.setSize(new Dimension(550,350));
                     JLabel labelBeingUsed = textoAyuda;
                     View view = (View) labelBeingUsed.getClientProperty(BasicHTML.propertyKey);
                     view.setSize(scrollPane1.getWidth(), 0.0f);
                     float w = view.getPreferredSpan(View.X_AXIS);
                     float h = view.getPreferredSpan(View.Y_AXIS);
                     labelBeingUsed.setSize((int) w, (int) h);

                     textoAyuda.setVerticalAlignment(JLabel.TOP);
                     jf.add(scrollPane1);

                     jf.setVisible(true);
                     //jf.setDefaultCloseOperation(EXIT_ON_CLOSE);
                     //System.out.println("Evento: "+ e);

                 }  
             });
        }
        //JPanel ayuda = new JPanel();
        
        String tipoArea="";
        if (this.opciones.isEmpty()) {
            if (this.idTypoText.equals("LongString")) {
                tipoArea="AreaAreaTexto";
            }else if (this.idTypoText.equals("")) {//cambiar para saber lo de añadir
               tipoArea="AreaAnadir"; 
            }else if(this.idTypoText.equals("Date")){
                tipoArea="AreaFecha";
            }else{
                tipoArea="AreaTexto";
            }
        }else{
            tipoArea="AreaOpciones";
        }   
        switch(tipoArea){        
            case "AreaTexto":
                entradaUsu= new AreaTexto(this.idTypoText);
                ((JPanel)entradaUsu).setLocation(0, 100);
                //System.out.println(entradaUsu);
                tamaño[0]= entradaUsu.getTamaño()[0];
                tamaño[1]= entradaUsu.getTamaño()[1]+110;
                
                break;
            case "AreaOpciones":
                entradaUsu= new AreaOpciones(opciones);
                ((JPanel)entradaUsu).setLocation(0, 100);
                tamaño[0]= entradaUsu.getTamaño()[0];
                tamaño[1]= entradaUsu.getTamaño()[1]+110;
                break;
            case "AreaAnadir":
                 entradaUsu= new AreaAnadirOpciones(this.idTypoText);
                ((JPanel)entradaUsu).setLocation(0, 100);
               // System.out.println(entradaUsu);
                tamaño[0]= entradaUsu.getTamaño()[0];
                tamaño[1]= entradaUsu.getTamaño()[1]+110;
                break;
            case "AreaAreaTexto":
                entradaUsu= new AreaAreaTexto(this.idTypoText);
                ((JPanel)entradaUsu).setLocation(0, 100);
               // System.out.println(entradaUsu);
                tamaño[0]= entradaUsu.getTamaño()[0];
                tamaño[1]= entradaUsu.getTamaño()[1]+110;
                
                break;
            case "AreaFecha":
                entradaUsu= new AreaFecha();
                ((JPanel)entradaUsu).setLocation(0, 100);
               // System.out.println(entradaUsu);
                tamaño[0]= entradaUsu.getTamaño()[0];
                tamaño[1]= entradaUsu.getTamaño()[1]+110;
                
                break;
        }
       // System.out.println(tipoArea);
        this.add((JPanel)entradaUsu);  
        this.setSize(960,tamaño[1]);
        
//        this.setBackground(new Color(255,255,255));//
        this.setBackground(new Color(205,217,243));//
        this.add(idTe);
        this.add(idTeEntry);
        this.add(tituTe);
        this.add(tituTeentry);
        this.add(tipote);
        this.add(tipoteEntry);
        this.add(entrada);
       
        
        if (entradaUsu instanceof AreaAnadirOpciones) {
            //System.out.println("si es");
            //evento botones
            ((AreaAnadirOpciones)entradaUsu).getButtonAgregar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                System.out.println("uno");

                ((AreaAnadirOpciones)entradaUsu).agregarElmento(((AreaAnadirOpciones)entradaUsu).getDatoText());
            }         
        });
            ((AreaAnadirOpciones)entradaUsu).getButonEliminar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                System.out.println("dos");
                ((AreaAnadirOpciones)entradaUsu).eliminarElemento();
            }         
        });
        }

    }
    
    
    public void cambioFondo(boolean value) {
      
        if(value)
            setBackground(new Color(255,50,50));
        else
             setBackground(new Color(205,217,243));      
    }
    
    public int obtenerTab() {
       
        return this.tab;
    }
    
    //obetener la info del text

    
    public int[] getTamaño() {
        return tamaño;
    }
    //obetner la info de combobox
 
    public JPanel getBotones(){
        return this;
    } 
   
    
    public String obtenerDato() {
       String temp; 
        ///devuelve campo como requerido.
       // if(this.required == true && entradaUsu.getDato().isEmpty()){
        //        temp = "Requerido";
        //       return temp;
         //   }
         //no modificar por favor... crear su funcino par adevolver ese campo, esta funcion debe retornar el dato del formulario solo......
         /// usada para la opción de guardar el metadata y volverlo a cargar, la función en ningún momento puede retornar "Requerido", o en la carga del metadata, cargará esa palabra..
        return entradaUsu.getDato();
    }
    public void setDato(String dato){
        entradaUsu.setDato(dato);
    }

    public JPanel obtenerPanel() {
        
        return entradaUsu.getPanel();
    }
    
    public String getId() {
        return idMetaText;
    }
    
    public String getType() {
        return idTypoText;
    }
    
     public String getHelp() {
        return idHelpText;
    }
    //campos usuario y fecha
    public String getUsuModi() {
        return usuModi;
    }

    public void setUsuModi(String usuModi) {
        this.usuModi = usuModi;
    }

    public Date getFechamod() {
        return fechamod;
    }

    public void setFechamod(Date fechamod) {
        this.fechamod = fechamod;
    }

   
    

}
