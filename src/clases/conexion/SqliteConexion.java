
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.conexion;
import clases.configuration;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author caano
 */
public class SqliteConexion {
//     Statement stmt = null;
//            SqliteConexion nombre= SqliteConexion.iniConexion();
//            stmt = nombre.getConexion().createStatement();   
//            ResultSet rs = stmt.executeQuery("SELECT name, sql FROM sqlite_master WHERE type='table ORDER BY name;");
//    
    //pool de conexiones.... actalizar
    private  Connection con=null;
    private static SqliteConexion instancia;

    private SqliteConexion() {
        configuration leer =new  configuration();
        String[] myconfig;
        try {
            myconfig = leer.getConfig();
            Class.forName("org.sqlite.JDBC");//driver
            con= DriverManager.getConnection("jdbc:sqlite:"+ myconfig[1]);// direccion base de datos
//            System.out.println("conectado");
        } catch (Exception e) {
            System.out.println("no se conecto"+e);
        } 
    }
    
    public synchronized static SqliteConexion iniConexion(){
        if (instancia==null) {
            instancia= new SqliteConexion();
        }
        return instancia;
    }
    public  Connection getConexion(){
    return con;
    }
    public void cerrarConexion(){//revisar......
        try {
            con.close();
//            System.out.println("cierra conecc");
        } catch (Exception e) {
            System.out.println("conexion no cerr");
        }
    instancia=null;
    }
    
}

