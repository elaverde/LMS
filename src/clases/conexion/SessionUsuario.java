/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.conexion;

import clases.DTO.UsersDTO;

/**
 *
 * @author caano
 */
public class SessionUsuario {
    private static UsersDTO usuarioSesion;
    private static SessionUsuario instancia;
    private SessionUsuario() {
    }
    
    public static SessionUsuario iniciarSession(UsersDTO nuevaSeSsion){
        if (instancia==null) {
            usuarioSesion=nuevaSeSsion;
            instancia= new SessionUsuario();
        }else{
            System.out.println("sesion ya iniciada,cerrar");
        }
            
        return instancia;
    }
    public static UsersDTO getSession(){
        if (usuarioSesion==null) {
            System.out.println("usuario no iniciado");
        }
        return usuarioSesion;
    }
    public void cerrarSession(){
        this.instancia=null;
        this.usuarioSesion=null;
    }
}
