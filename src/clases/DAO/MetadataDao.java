/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DAO.DaoImpleme;
import clases.DTO.ArchivoDTO;
import clases.DTO.MetadataDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author caano
 */
public class MetadataDao implements DaoImpleme<MetadataDTO> {

    private String SQL_INSERT = "INSERT INTO metadatas (idarc_met, ";
    private static final String SQL_DELETE = "DELETE FROM metadatas WHERE idarc_met = ?";
    private String SQL_UPDATE = "UPDATE metadatas SET ";
    private static final String SQL_READ = "SELECT * FROM metadatas WHERE idarc_met = ?";
    private static final String SQL_READALL = "SELECT * FROM metadatas";
    private static final String SQL_READIFExist = "SELECT id_met FROM metadatas WHERE idarc_met = ?";
    private static  SqliteConexion conec = SqliteConexion.iniConexion();

    public MetadataDao() {
        conec = SqliteConexion.iniConexion();
    }
    
    
    //comprueba si existe el id del archivo
    public boolean ifExis(Object key,boolean cerrarConexion){/// cerrar la conexion manual o utilizar otro metodo q lo haga
        PreparedStatement st=null; 
        ResultSet res;
        try {       
            st= conec.getConexion().prepareStatement(SQL_READIFExist);
            st.setString(1, key.toString());//llave un entero
            res= st.executeQuery();
            if (res.next()) {
//                System.out.println("existe");
                return true;               
            }
            
        } catch (SQLException ex) {
            System.out.println("Error en if exist metadatoDao");
        }finally{
            if (cerrarConexion) {
                conec.cerrarConexion();//revisar o.O..... o hacer dos instancias...
            }
 
////            
        }
        return false;
    }    
    @Override
    public boolean create(MetadataDTO c) {
        PreparedStatement st = null;
        Iterator itKEy = c.getDatosMeta().entrySet().iterator();
        String llaves = "";
        String valores = "";

        while (itKEy.hasNext()) {//se puede hacer el query de una....
            Map.Entry next = (Map.Entry) itKEy.next();
            llaves = llaves + next.getKey().toString();
            valores = valores + "?";
            next.getKey();
            next.getValue();
            if (itKEy.hasNext()) {
                llaves = llaves + ", ";
                valores = valores + ", ";
            }
        }
        //el primero para el id del archivo
        SQL_INSERT = SQL_INSERT + llaves + ") VALUES (?,"+ valores + ")";
       // System.out.println(SQL_INSERT);
        try {
            st = conec.getConexion().prepareStatement(SQL_INSERT);
            Iterator itValues = c.getDatosMeta().entrySet().iterator();
            int cont=2;
            
            st.setInt(1, c.getIdarc_met());
            while (itValues.hasNext()) {//se puede hacer el query de una....
                Map.Entry next = (Map.Entry) itValues.next();                
                st.setString(cont, next.getValue().toString());           
                if (cont<(c.getDatosMeta().size()-1)) {
                    cont++;
                }               
            }
            if (st.executeUpdate()> 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error en MetadataDao Create"+ e);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(Object key) {
        PreparedStatement st= null;
            
        try {
            
            st= conec.getConexion().prepareStatement(SQL_DELETE);
            st.setString(1, key.toString());
            if (st.executeUpdate() >0 ) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("error en MetadataDao Delete"+ ex);
        }finally{
            conec.cerrarConexion();
        }
       return false;
    }

    
    public boolean updateInGroup(MetadataDTO c,ArchivoDTO arc) {
         PreparedStatement st = null;
        Iterator itKEy = c.getDatosMeta().entrySet().iterator();
        String valorCambiar = "";
        String valores = "";

        while (itKEy.hasNext()) {//se puede hacer el query de una....
            Map.Entry next = (Map.Entry) itKEy.next();
            valorCambiar = valorCambiar + next.getKey().toString();
            valorCambiar = valorCambiar + " = ?";
            next.getKey();
            next.getValue();
            if (itKEy.hasNext()) {
                valorCambiar = valorCambiar + ", ";

            }
        }
        //el primero para el id del archivo
        SQL_UPDATE = SQL_UPDATE + valorCambiar + "WHERE idarc_met IN (SELECT id_arc FROM Archivos WHERE idGbF_arc = "+arc.getIdGbF_arc()+")";
        //System.out.println(SQL_UPDATE);
        try {
            st = conec.getConexion().prepareStatement(SQL_UPDATE);
            Iterator itValues = c.getDatosMeta().entrySet().iterator();
            int cont=1;
 
            while (itValues.hasNext()) {//se puede hacer el query de una....
                Map.Entry next = (Map.Entry) itValues.next();                
                st.setString(cont, next.getValue().toString());           
                if (cont<(c.getDatosMeta().size()-1)) {
                    cont++;
                }               
            }
            if (st.executeUpdate()> 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error en MetadataDao Update"+ e);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }
    
    @Override
    public boolean update(MetadataDTO c){
        
        PreparedStatement st = null;
        Iterator itKEy = c.getDatosMeta().entrySet().iterator();
        String valorCambiar = "";
        String valores = "";

        while (itKEy.hasNext()) {//se puede hacer el query de una....
            Map.Entry next = (Map.Entry) itKEy.next();
            valorCambiar = valorCambiar + next.getKey().toString();
            valorCambiar = valorCambiar + " = ?";
            next.getKey();
            next.getValue();
            if (itKEy.hasNext()) {
                valorCambiar = valorCambiar + ", ";

            }
        }
        //el primero para el id del archivo
        SQL_UPDATE = SQL_UPDATE + valorCambiar + " WHERE idarc_met =  "+c.getIdarc_met();
       // System.out.println("My sql"+SQL_UPDATE);
        try {
            st = conec.getConexion().prepareStatement(SQL_UPDATE);
            Iterator itValues = c.getDatosMeta().entrySet().iterator();
            int cont=1;
 
            while (itValues.hasNext()) {//se puede hacer el query de una....
                Map.Entry next = (Map.Entry) itValues.next();                
                st.setString(cont, next.getValue().toString());   
               // System.out.println(cont+", "+ next.getValue().toString());
                if (cont<(c.getDatosMeta().size())) {
                    cont++;
                }               
            }
          //   System.out.println(st.toString());
            if (st.executeUpdate()> 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error en MetadataDao Update"+ e);
        }finally{
            conec.cerrarConexion();
        }
    return false;
    }
    
    @Override
    public MetadataDTO read(Object key) {
        MetadataDTO metaTemp=null;
        PreparedStatement st=null;
        ResultSetMetaData  metada;
        Map mapTemp= new HashMap<String, String>();
        try { 
            ResultSet res;
          
            st= conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());//lave un entero
            res= st.executeQuery();
            metada=res.getMetaData();
            int cont=1;
//            System.out.println(metada.getColumnCount());
            while (res.next()) {
                for (int i = 3; i <= metada.getColumnCount(); i++) {                  
                    mapTemp.put(metada.getColumnLabel(i),res.getString(i));
                }
                 metaTemp= new MetadataDTO(Integer.parseInt(key.toString()), mapTemp);
            }           
//            System.out.println(mapTemp.size());
            return metaTemp;
        } catch (SQLException ex) {
            System.out.println("error en read MetadataDao"+ ex);
            
        }finally{
//            System.out.println("cierra en meta read");
            conec.cerrarConexion();
        }
        return null;
    }
    
    @Override
    public List<MetadataDTO> readAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    public MetadataDTO readSinCerrar(Object key) {
        MetadataDTO metaTemp=null;
        PreparedStatement st=null;
        ResultSetMetaData  metada;
        Map mapTemp= new HashMap<String, String>();
        try { 
            ResultSet res;
          
            st= conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());//lave un entero
            res= st.executeQuery();
            metada=res.getMetaData();
            int cont=1;
//            System.out.println(metada.getColumnCount());
            while (res.next()) {
                for (int i = 3; i <= metada.getColumnCount(); i++) {                  
                    mapTemp.put(metada.getColumnLabel(i),res.getString(i));
                }
                metaTemp= new MetadataDTO(Integer.parseInt(key.toString()), mapTemp);
            }           
//            System.out.println(mapTemp.size()+" en read sin ");
            return metaTemp;
        } catch (SQLException ex) {
            System.out.println("error en read MetadataDao"+ ex);
            
        }
        return null;
    }
    public void cerrarConec(){
         conec.cerrarConexion();
    }

}
