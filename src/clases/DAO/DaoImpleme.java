/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import java.util.List;

/**
 *
 * @author caano
 */
public interface DaoImpleme <General>{
    public boolean create(General c);
    boolean delete(Object key);
    boolean update(General c);    
    General read(Object key);
    List<General> readAll();
}
