/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DTO.ProjectDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edi
 */
public class ProjectDAO implements DaoImpleme<ProjectDTO> {
    private String SQL_INSERT = "INSERT INTO proyectos (id_pro,name_pro,tipo_pro) VALUES (null,?,?) ";
    private static final String SQL_DELETE = "DELETE FROM proyectos WHERE id_pro = ?";
    private String SQL_UPDATE = "UPDATE  proyectos SET name_pro = ?, tipo_pro = ? WHERE id_pro = ?";
    private static final String SQL_READ = "SELECT * FROM  proyectos WHERE name_pro  = ?";
    private static final String SQL_READALL = "SELECT * FROM  proyectos ";
    private static final String SQL_READIFExist = "SELECT id_pro FROM  proyectos WHERE id_pro  = ?";
    private static  SqliteConexion conec = SqliteConexion.iniConexion();
    public ProjectDAO() {
        conec = SqliteConexion.iniConexion();
    }

    @Override
    public boolean create(ProjectDTO c) {
        PreparedStatement st = null;
        try {
            st = conec.getConexion().prepareStatement(SQL_INSERT);
            st.setString(1, c.getname_pro());
            st.setString(2, c.gettipo_pro());
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en ProjectDaO create "+e);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(Object key) {
      try {
            //delete metadata hijo
            MetadataDao met= new MetadataDao();
            if (met.delete(key)) {//sip uede borrar a los hijos
               PreparedStatement st = null;
                st = conec.getConexion().prepareStatement(SQL_DELETE);
                st.setString(1, key.toString());
                if (st.executeUpdate() > 0) {
                return true;
            } 
            }
        } catch (SQLException ex) {
           System.out.println("Error en ArchivoDaO delete "+ex);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }
    @Override
    public boolean update(ProjectDTO c) {
        try {
            PreparedStatement ps = null;
            ps = conec.getConexion().prepareStatement(SQL_UPDATE);
            ps.setString(1,c.getname_pro());
            ps.setString(2, c.gettipo_pro());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error en ArchivoDaO update "+ex);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public ProjectDTO read(Object key) {
        PreparedStatement st = null;
        ResultSet res;
        ProjectDTO ar = null;
        try {
            System.out.println(SQL_READ);
            st = conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());
            res = st.executeQuery();
            while (res.next()) {
                ar = new ProjectDTO(res.getInt(1), res.getString(2), res.getString(3));
            }
            return ar;
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ar;
    }
   @Override
    public List<ProjectDTO> readAll() {
           PreparedStatement st= null;
            ResultSet res;
             ArrayList<ProjectDTO> proyectos = new ArrayList<>();
        try {
            st = conec.getConexion().prepareStatement(SQL_READALL);
            res= st.executeQuery();
            while (res.next()) {
                proyectos.add(new ProjectDTO(res.getInt(1), res.getString(2), res.getString(3)));               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            conec.cerrarConexion();
        }
        return proyectos;
    }
}