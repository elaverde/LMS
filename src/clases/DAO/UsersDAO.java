/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DTO.UsersDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author caano
 */
public class UsersDAO implements DaoImpleme<UsersDTO>{
    
    private String SQL_INSERT= "INSERT INTO users (name_usu,lastname_usu,user_usu,pass_usu,tipo_usu,linea) VALUES (?,?,?,?,?,?)";
    private static final String SQL_DELETE= "DELETE FROM users WHERE id_usu= ?";///cambiar segun la busqueda
    private String SQL_UPDATE="UPDATE users SET name_usu=?,lastname_usu=?,user_usu=?,pass_usu=?,tipo_usu=?,linea=?";
    private String SQL_READ="SELECT * FROM users WHERE id_usu=?";
    private String SQL_READUSUNAME="SELECT * FROM users WHERE user_usu=?";
    private String SQL_READALL="SELECT * FROM users";
    private String SQL_LOGIN="SELECT pass_usu FROM users WHERE user_usu=?";
    private String SQL_VALIDAUSERNAME="SELECT name_usu FROM users WHERE user_usu=?";

    private static SqliteConexion conec= SqliteConexion.iniConexion();

    public UsersDAO() {
        conec= SqliteConexion.iniConexion();
    }
    
    @Override
    public boolean create(UsersDTO c) {
        try {
            PreparedStatement st= null;
            st= conec.getConexion().prepareStatement(SQL_INSERT);
            st.setString(1, c.getName());
            st.setString(2, c.getLastName());
            st.setString(3, c.getUsuario());
            st.setString(4, c.getPass());
            st.setString(5, c.getTipoUsuario());
            st.setString(6, c.getLinea());
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error en userDao create "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(UsersDTO c) {
        PreparedStatement st= null;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_UPDATE);
            st.setString(1, c.getName());
            st.setString(2, c.getLastName());
            st.setString(3, c.getUsuario());
            st.setString(4, c.getTipoUsuario());
            st.setString(5, c.getLinea());
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error en userDao update "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public UsersDTO read(Object key) {
        PreparedStatement st=null;
            ResultSet res=null;
            UsersDTO us=null;
        try {
            
            st=conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());
            res= st.executeQuery();
            while (res.next()) {
                us= new UsersDTO(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7));  
            }
            return us;
        } catch (SQLException ex) {
           System.out.println("Error en userDao read "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return us;
    }
    public UsersDTO readNombreUsu(String userName) {
        PreparedStatement st=null;
            ResultSet res=null;
            UsersDTO us=null;
        try {
            
            st=conec.getConexion().prepareStatement(SQL_READUSUNAME);
            st.setString(1, userName);
            res= st.executeQuery();
            while (res.next()) {
                us= new UsersDTO(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7));  
            }
            return us;
        } catch (SQLException ex) {
           System.out.println("Error en userDao read "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return us;
    }

    @Override
    public List<UsersDTO> readAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean readLogin(String userName,String pass){
        PreparedStatement st=null;
            ResultSet res=null;
            String tmpPass="";
           // System.out.println(userName+","+pass);
        try {
            
            st= conec.getConexion().prepareStatement(SQL_LOGIN);
            st.setString(1, userName);
            res= st.executeQuery();
            if (res.next()) {                
               tmpPass=res.getString(1);
                
            }else{
            System.out.println("no existe el nombre de usuario");
            }
            if (tmpPass.equals(pass)) {
                return true;
            }else{
                System.out.println("password no coincide");
            }   
        } catch (SQLException ex) {
            System.out.println("Error en userDao readlogin "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }
    
    public boolean readCorreo(String userName){
        PreparedStatement st=null;
            ResultSet res=null;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_LOGIN);
            st.setString(1, userName);
            res= st.executeQuery();
            if (res.next()) {                
               return true;
                
            }else{
                return false;
            }
            
        } catch (SQLException ex) {
            System.out.println("Error en userDao readlogin "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }
    
    public boolean validaPorUserName(String correo){
        PreparedStatement st=null;
            ResultSet res=null;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_VALIDAUSERNAME);
            st.setString(1, correo);
            res= st.executeQuery();
            if (res.next()) {                 
               System.out.println("el usuario ya existe");
               return true;
            }            
        } catch (SQLException ex) {
            System.out.println("Error en userDao validaExiste "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }
    
}
