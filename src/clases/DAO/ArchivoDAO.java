/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DTO.ArchivoDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author caano
 */
public class ArchivoDAO implements DaoImpleme<ArchivoDTO> {

    private String SQL_INSERT = "INSERT INTO Archivos (idGBF_arc,urlComp_arc) VALUES (?,?) ";
    private static final String SQL_DELETE = "DELETE FROM Archivos WHERE id_arc = ?";
    private static final String SQL_DELETEONG = "DELETE FROM Archivos WHERE id_arc = ? ";
    private String SQL_UPDATE = "UPDATE Archivos SET idGBF_arc = ?, urlComp_arc = ? WHERE id_arc = ? ";
    private static final String SQL_READ = "SELECT * FROM Archivos WHERE id_arc = ? ";
    private static final String SQL_READGrupo = "SELECT compuesto_GbF,idGbF_arc, id_arc , tipo_Gbf FROM Archivos , GruopByfiles WHERE  (idGbF_arc =id_GbF  and urlComp_arc =? )";
    private static final String SQL_READALL = "SELECT * FROM Archivos";
    private static final String SQL_READIFExist = "SELECT id_met FROM Archivos WHERE idarc_met = ? ";
    private static final String SQL_GETIDPORPATHLIKE  = "SELECT * FROM Archivos WHERE urlComp_arc LIKE ? ";
    private static final String SQL_GETIDPORPATH = "SELECT id_arc FROM Archivos WHERE urlComp_arc = ? ";
    private static final String SQL_DELETEG = "DELETE FROM GruopByfiles WHERE id_GbF = ?";
    private static final String SQL_FILESIFExist = "SELECT * FROM Archivos where idGbF_arc = ? ";
    private static final String SQL_ReadArchivosProyec = "SELECT * FROM Archivos where idGbF_arc IN(SELECT id_GbF FROM GruopByfiles where idpro_GbF = ? ) ";
   
     
    private static  SqliteConexion conec = SqliteConexion.iniConexion();//final????

    public ArchivoDAO() {
        conec = SqliteConexion.iniConexion();//se inicia cuando se crea
    }

    @Override
    public boolean create(ArchivoDTO c) {
        PreparedStatement st = null;
        try {
             conec = SqliteConexion.iniConexion();
            st = conec.getConexion().prepareStatement(SQL_INSERT,Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, c.getIdGbF_arc());
            st.setString(2, c.getUrlComp_arc());
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en ArchivoDaO create "+e);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }
    
      public boolean ifExis(Object key, boolean cerrarConexion){/// cerrar la conexion manual o utilizar otro metodo q lo haga
        PreparedStatement st=null; 
        ResultSet res;
        try {  
            conec = SqliteConexion.iniConexion();
            st= conec.getConexion().prepareStatement(SQL_FILESIFExist);
            st.setString(1, key.toString());//llave un entero
            res= st.executeQuery();
            if (res.next()) {
                System.out.println("existe el grupo");
                return true;               
            }
            
        } catch (SQLException ex) {
            System.out.println("---->"+ex);
        }finally{
            if (cerrarConexion) {
                conec.cerrarConexion();//revisar o.O..... o hacer dos instancias...
            }
 
////            
        }
        return false;
    } 
    public boolean deleteGroup(Object key) {
        if(!ifExis(key, true)){
            PreparedStatement st= null;
            try {
                conec = SqliteConexion.iniConexion();
                st= conec.getConexion().prepareStatement(SQL_DELETEG);
                st.setString(1, key.toString());
                if (st.executeUpdate() >0 ) {
                    return true;
                }
            } catch (SQLException ex) {
                System.out.println("error en GroupbF Delete"+ ex);
            }finally{
                conec.cerrarConexion();
            }
            return false;
        }else{
            return false;
        }
    }
    
    public int insertAutoIndex(ArchivoDTO c) {
        PreparedStatement st = null;
        int auto_id =0;
        conec= SqliteConexion.iniConexion();
        try {
            st = conec.getConexion().prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);

            st.setInt(1, c.getIdGbF_arc());
            st.setString(2, c.getUrlComp_arc());
            if (st.executeUpdate() > 0) {
                ResultSet rs = st.getGeneratedKeys();
                rs.next();
                auto_id = rs.getInt(1);
                return auto_id;
            }
        } catch (Exception e) {
            System.out.println("Error en ArchivoDaO create "+e);
        } finally {
            conec.cerrarConexion();
        }
        return auto_id;

    }

    @Override
    public boolean delete(Object key) {
        try {
            //delete metadata hijo
            MetadataDao met= new MetadataDao();
            if (met.delete(key)) {//sip uede borrar a los hijos
               PreparedStatement st = null;
               
                st = conec.getConexion().prepareStatement(SQL_DELETE);
                st.setString(1, key.toString());
                if (st.executeUpdate() > 0) {
                return true;
            } 
            }

            
        } catch (SQLException ex) {
           System.out.println("Error en ArchivoDaO delete "+ex);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(ArchivoDTO c) {
        try {
            PreparedStatement ps = null;
            ps = conec.getConexion().prepareStatement(SQL_UPDATE);
            ps.setInt(1, c.getIdGbF_arc());
            ps.setString(1, c.getUrlComp_arc());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error en ArchivoDaO update "+ex);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public ArchivoDTO read(Object key) {
        PreparedStatement st = null;
        ResultSet res;
        ArchivoDTO ar = null;
        try {
            st = conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());
            res = st.executeQuery();
            while (res.next()) {
                ar = new ArchivoDTO(res.getInt(1), res.getInt(2), res.getString(3));
            }
            return ar;
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ar;
    }
    
    public static ArchivoDTO readGroup(Object key) {
        PreparedStatement st = null;
        ResultSet res;
        ArchivoDTO ar = null;
        try {
            conec= SqliteConexion.iniConexion();
            st = conec.getConexion().prepareStatement(SQL_READGrupo);
            st.setString(1, key.toString());
            res = st.executeQuery();
            while (res.next()) {
           
                ar = new ArchivoDTO(res.getBoolean(1), res.getInt(2), res.getInt(3), res.getString(4));
            }
           
            return ar;
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ar;
    }
    
    
    
    public boolean deleteonGroup(Object key) {
        try {
            //delete metadata hijo
            MetadataDao met= new MetadataDao();
            if (met.delete(key)) {//sip uede borrar a los hijos
                conec= SqliteConexion.iniConexion();
                PreparedStatement st = null;
                st = conec.getConexion().prepareStatement(SQL_DELETEONG);
                st.setString(1, key.toString());
                if (st.executeUpdate() > 0) {
                return true;
                } 
            }
        } catch (SQLException ex) {
           System.out.println("Error en ArchivoDaO delete "+ex);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public List<ArchivoDTO> readAll() {
        PreparedStatement st= null;
            ResultSet res;
             ArrayList<ArchivoDTO> archivos = new ArrayList<>();
        try {
            st = conec.getConexion().prepareStatement(SQL_READALL);
            
            res= st.executeQuery();
            while (res.next()) {
                archivos.add(new ArchivoDTO(res.getInt(1), res.getInt(2), res.getString(3)));               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            conec.cerrarConexion();
        }
        return archivos;
    }
    public ArrayList<ArchivoDTO> readArchivosProyecto(String idProyecto) {
        PreparedStatement st= null;
            ResultSet res;
             ArrayList<ArchivoDTO> archivos = new ArrayList<>();
        try {
            st = conec.getConexion().prepareStatement(SQL_ReadArchivosProyec);
            st.setString(1, idProyecto);
            res= st.executeQuery();
            while (res.next()) {
                archivos.add(new ArchivoDTO(res.getInt(1), res.getInt(2), res.getString(3)));               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            conec.cerrarConexion();
        }
        return archivos;
    }
    public ArrayList<ArchivoDTO> readArchivosInitURL(String urlInit) {
        PreparedStatement st= null;
            ResultSet res;
             ArrayList<ArchivoDTO> archivos = new ArrayList<>();
        try {
            st = conec.getConexion().prepareStatement(SQL_GETIDPORPATHLIKE);
            st.setString(1, "%" +urlInit+ "%" );
            res= st.executeQuery();
            while (res.next()) {
                archivos.add(new ArchivoDTO(res.getInt(1), res.getInt(2), res.getString(3)));               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            conec.cerrarConexion();
        }
        return archivos;
    }
      
    public int getIdPorPathArchivo(String path){
        PreparedStatement st=null;
            ResultSet res=null;
            int idtemp=0;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_GETIDPORPATH);
            st.setString(1, path);
            res= st.executeQuery();
            if (res.next()) {                 
               idtemp=res.getInt(1);
            }            
        } catch (SQLException ex) {
            System.out.println("Error en userDao validaExiste "+ex);
        }finally{
            conec.cerrarConexion();
        }
//        System.out.println("id desde dao "+idtemp);
        return idtemp;
    }
}
