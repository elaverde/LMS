/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DTO.GroupByFilesDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author caano
 * Modificacion edilson laverde
 */
public class GroupByFilesDAO implements DaoImpleme<GroupByFilesDTO>{
    
    private String SQL_INSERT = "INSERT INTO GruopByfiles (id_GbF,idpro_GbF,compuesto_GbF,tipo_Gbf) VALUES (null,?,?,?) ";
    private static final String SQL_DELETE = "DELETE FROM GruopByfiles WHERE id_GbF = ?";
    private String SQL_UPDATE = "UPDATE GruopByfiles SET idpro_GbF = ?, compuesto_GbF = ?, tipo_Gbf=? WHERE id_Gbf = ?";
    private static final String SQL_READ = "SELECT * FROM GruopByfiles WHERE id_Gbf = ?";
    private static final String SQL_READALL = "SELECT * FROM GruopByfiles";
    private static final String SQL_READIFExist = "SELECT id_Gbf FROM GruopByfiles WHERE id_Gbf = ?";
    
    private static SqliteConexion conec= SqliteConexion.iniConexion();
    @Override
    public boolean create(GroupByFilesDTO c) {
        PreparedStatement st = null;
        try {
            st = conec.getConexion().prepareStatement(SQL_INSERT);
            st.setInt(1, c.getIdpro_Gbf());
            st.setInt(2, c.isCompuesto_Gbf());
            st.setString(3, c.getTipo_Gbf());
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en GroupbF create "+e);
        } finally {
            conec.cerrarConexion();
        }
        return false;
    }
    
    public int insertAutoIndex(GroupByFilesDTO c) {
        PreparedStatement st = null;
        int auto_id =0;
        conec= SqliteConexion.iniConexion();
        try {
            st = conec.getConexion().prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, c.getIdpro_Gbf());
            st.setInt(2, c.isCompuesto_Gbf());
            st.setString(3, c.getTipo_Gbf());
            System.out.println("Mi query "+st.getResultSet());
            if (st.executeUpdate() > 0) {
                ResultSet rs = st.getGeneratedKeys();
                rs.next();
                auto_id = rs.getInt(1);
                return auto_id;
            }
        } catch (Exception e) {
            System.out.println("Error en GroupbF create "+e);
        } finally {
            conec.cerrarConexion();
        }
        return auto_id;
    }

    @Override
    public boolean delete(Object key) {
        PreparedStatement st= null;
            
        try {
            
            st= conec.getConexion().prepareStatement(SQL_DELETE);
            st.setString(1, key.toString());
            if (st.executeUpdate() >0 ) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("error en GroupbF Delete"+ ex);
        }finally{
            conec.cerrarConexion();
        }
       return false;
    }

    @Override
    public boolean update(GroupByFilesDTO c) {
        PreparedStatement st= null;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_UPDATE);
            st.setInt(1, c.getIdpro_Gbf());
            st.setInt(2, c.isCompuesto_Gbf());
            st.setString(3, c.getTipo_Gbf());
         
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("error en GroupbF update"+ ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public GroupByFilesDTO read(Object key) {
            PreparedStatement st=null;
            ResultSet res=null;
            GroupByFilesDTO gbf=null;
        try {
            
            st=conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());
            res= st.executeQuery();
            while (res.next()) {
                gbf= new GroupByFilesDTO(res.getInt(1), res.getInt(2), res.getString(3));  
            }
            return gbf;
        } catch (SQLException ex) {
           System.out.println("Error en GroupbF read "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return gbf;
    }

    @Override
    public List<GroupByFilesDTO> readAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
