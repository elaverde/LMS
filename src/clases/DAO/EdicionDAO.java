/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DAO;

import clases.DTO.EdicionDTO;
import clases.conexion.SqliteConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author caano
 */
public class EdicionDAO implements DaoImpleme<EdicionDTO>
{
     private String SQL_INSERT = "INSERT INTO edicion (idmet_edi,ideUse_edi,campo_edi,fecha_edi) VALUES (?,?,?,?) ";
    private static final String SQL_DELETE = "DELETE FROM edicion WHERE id_edi = ?";
    private String SQL_UPDATE = "UPDATE edicion SET idmet_edi = ?, ideUse_edi = ?, campo_edi=?,fecha_edi=? WHERE id_edi = ?";
    private static final String SQL_READ = "SELECT * FROM edicion WHERE id_edi = ?";
    private static final String SQL_READALL = "SELECT * FROM edicion";
    private static final String SQL_READIFExist = "SELECT id_edi FROM edicion WHERE id_edi = ?";

        private static SqliteConexion conec= SqliteConexion.iniConexion();
    public EdicionDAO() {
        conec= SqliteConexion.iniConexion();
    }
    
    
    @Override
    public boolean create(EdicionDTO c) {
        try {
            PreparedStatement st= null;
            st= conec.getConexion().prepareStatement(SQL_INSERT);
            st.setInt(1, c.getIdmet_edi());
            st.setInt(2, c.getIdeUse_edi());
            st.setInt(3, c.getCampo_edi());
            st.setString(4, c.getFecha_edi());

            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error en Edicion create "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(Object key) {
       PreparedStatement st= null;
            
        try {
            
            st= conec.getConexion().prepareStatement(SQL_DELETE);
            st.setString(1, key.toString());
            if (st.executeUpdate() >0 ) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("error en Edicion Delete"+ ex);
        }finally{
            conec.cerrarConexion();
        }
       return false;
    }

    @Override
    public boolean update(EdicionDTO c) {
        PreparedStatement st= null;
        try {
            
            st= conec.getConexion().prepareStatement(SQL_UPDATE);
            st.setInt(1, c.getIdmet_edi());
            st.setInt(2, c.getIdeUse_edi());
            st.setInt(3, c.getCampo_edi());
            st.setString(4, c.getFecha_edi());
         
            if (st.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("error en Edicion update"+ ex);
        }finally{
            conec.cerrarConexion();
        }
        return false;
    }

    @Override
    public EdicionDTO read(Object key) {
        PreparedStatement st=null;
            ResultSet res=null;
            EdicionDTO edi=null;
        try {
            
            st=conec.getConexion().prepareStatement(SQL_READ);
            st.setString(1, key.toString());
            res= st.executeQuery();
            while (res.next()) {
                edi= new EdicionDTO(res.getInt(1), res.getInt(2), res.getInt(3),res.getInt(4),res.getString(5));  
            }
            return edi;
        } catch (SQLException ex) {
           System.out.println("Error en Edicion read "+ex);
        }finally{
            conec.cerrarConexion();
        }
        return edi;
    }

    @Override
    public List<EdicionDTO> readAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
