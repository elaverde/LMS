/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Jframes.frm_principal;
import clases.DAO.ArchivoDAO;
import clases.DAO.MetadataDao;
import clases.DTO.ArchivoDTO;
import clases.DTO.MetadataDTO;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import javax.swing.JTextField;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author edi
 */
public class metadataCreator  {

    public String source = "";
    public String base = "";
    utileriasLMS utils = new utileriasLMS();
    report_Required reporte = new report_Required();
    URL res = getClass().getResource("/templateJson/template.json");
    public report_Required metadataCreatorAllProject(String project) {
        source = "";
        base=utils.Get_workspace() + File.separator + utils.getProject() + File.separator;
        ArrayList<ArchivoDTO> arrArchivo = null;
        ArchivoDAO consultaAr = new ArchivoDAO();
        arrArchivo = consultaAr.readArchivosProyecto(project);
        if (arrArchivo != null) {
            int cont = 0;
            for (ArchivoDTO archivoDTO : arrArchivo) {
                Map mapDatoBd;
                MetadataDTO metTp;
                MetadataDao met = new MetadataDao();
                metTp = met.read(archivoDTO.getId_arc());
                mapDatoBd = metTp.getDatosMeta();
                reporte.set(true);
                String results = addsources(base,res, mapDatoBd, utils.Get_workspace() + File.separator + archivoDTO.getUrlComp_arc());
                if (results.equals("incomplete")) {
                    return reporte;
                }
                //Create_metadata(String UrlWrite, URL res, Map al, ArrayList<String> archivos, String operacion);
            }   
            if(reporte.data_valid()){
                Metadata_Post();
            }
        }
        return reporte;
    }
    public report_Required metadataCreatorSelectPartProject(String url) {
        source = "";
        base=file_is_folder(utils.Get_workspace() + File.separator + url);
      
        ArrayList<ArchivoDTO> arrArchivo = null;
        ArchivoDAO consultaAr = new ArchivoDAO();

        arrArchivo = consultaAr.readArchivosInitURL(url);
        System.out.println("MY Base"+base);
          
        if (arrArchivo != null) {
            int cont = 0;
            for (ArchivoDTO archivoDTO : arrArchivo) {
                Map mapDatoBd;
                MetadataDTO metTp;
                MetadataDao met = new MetadataDao();
                metTp = met.read(archivoDTO.getId_arc());
                mapDatoBd = metTp.getDatosMeta();
                reporte.set(true);
                String results = addsources(base,res, mapDatoBd, utils.Get_workspace() + File.separator + archivoDTO.getUrlComp_arc());
                if (results.equals("incomplete")) {
                    return reporte;
                }
                //Create_metadata(String UrlWrite, URL res, Map al, ArrayList<String> archivos, String operacion);
            }   
            if(reporte.data_valid()){
                Metadata_Post();
            }
        }
        return reporte;
    }

    public void Metadata_Post(){
        String xml="";
        String ruta = utils.Get_workspace()+ File.separator +utils.getProject()+ File.separator + "metadata.xml";
        File archivo = new File(ruta);
        BufferedWriter meta = null;
        //inicio lectura JSON//
        JSONParser parser = new JSONParser();
        try {
            //preparamos archivo y lo parceamos para crear la estructura y adicionarle los sources//
            InputStream inputStream = res.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject metadatas = (JSONObject) jsonObject.get("metadatas");
            //creamos objeto con etiquetas principales de los datos//
            String header = (String) metadatas.get("header");
            String lomO = (String) metadatas.get("lom-open");
            String lomC = (String) metadatas.get("lom-close");
            String relation = (String) metadatas.get("relation");
            xml+=header;
            xml+=lomO;
            xml+=relation.replace("($)",source);
            xml+=lomC;
            meta = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(archivo), "UTF-8"));
            meta.write(xml);
            meta.close();
       
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public String addsources(String base,URL jsonEstruct, Map datos, String archivo) {
        //name metadata ims
        String urlFile="";
        File meroot = new File(base);
        BufferedWriter meta = null;
        //inicio lectura JSON//
        JSONParser parser = new JSONParser();
        try {
            //preparamos archivo y lo parceamos para posterio lectura//
            InputStream inputStream = jsonEstruct.openConnection().getInputStream();
            Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, "UTF-8")));
            JSONObject jsonObject = (JSONObject) obj;
            //creamos objetos pricipal//
            JSONObject metadatas = (JSONObject) jsonObject.get("metadatas");
            //creamos objeto con 3 etiquetas principales de las datos//
            String catalogentry = (String) metadatas.get("catalogentry");
            String catalog = (String) metadatas.get("catalog");
            String iden = (String) metadatas.get("identifier");
            String recurso = (String) metadatas.get("resource");
            //datos del metadato ha recorrer 71 datos//
            JSONArray values = (JSONArray) metadatas.get("datos");
            //apertura head y lom//
            //INICIO SOURCE//
            //revisamos si es una caroeta o un folder para traer el nombre
            File dirObj = new File(archivo);
          
            urlFile = estructura(meroot, dirObj.getAbsolutePath());
            System.out.println(meroot+"  ====================  "+dirObj.getAbsolutePath()+"==========="+urlFile);
            String nameFile="";
            if (dirObj.isFile()) {
            nameFile=dirObj.getName();
            } 
            if (nameFile.equals("metadata.xml")) {
            } else {
                //id del proyecto pendiente------------------------------------->>>>>>>>>>>>>>>>>>>>>>>//
                String id = iden.replace("($)", datos.get("ims_adv_general_identifier").toString() + "_" + "1" + "#" + utils.Convert_Slash(urlFile));
                //recorremos la arquitectura de los campos// 
                String bloques = id;
                //GENERACION BLOQUE 1 a 1//
                for (int i = 0; i < values.size(); i++) {
                    JSONObject dates = (JSONObject) values.get(i);
                    //adicion del identificador//
                    String ide = (String) dates.get("id_meta");
                    //revisamos si esta vacio no lo ponemos para disminuir peso del xml//
                    if (!datos.get(ide).toString().equals("")) {
                        String identificador = catalog.replace("($)", ide);
                        //adicion del valor de ingresado//
                        
                        String descripcion = (String) dates.get("description");
                        String vals = (String) dates.get("value");
                        String valor = vals.replace("($)", datos.get(ide).toString());
                        //creacion del bloque con la info//
                        String bloque = catalogentry.replace("($)", identificador + valor + descripcion);
                        bloques += bloque;
                    } else {
                        String req = (String) dates.get("required");
                        
                        try {
                            if (req.equals("true")) {
                                String lom = (String) dates.get("lom");
                                String descripcion = (String) dates.get("description");
                                String help = (String) dates.get("help");
                                long index = (Long) dates.get("index");
                                System.out.println("Mi primer reporte"+ide+"value ="+datos.get(ide).toString());
                                reporte.set(ide, descripcion,help,urlFile,lom, index );
                                return "incomplete";
                            }
                        } catch (Exception e) {
                            System.out.println("No tiene required");
                        } finally {
                            System.out.println("Proceso");
                        }
                    }
                }
                nameFile="";
                source += recurso.replace("($)", bloques);
            }
            //FIN SOURCE//
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        //fin lectura JSON//
        return source;
    }
    public String file_is_folder(String file){
        File carpeta=new File(file);
        if(carpeta.isDirectory()){
            return file;
        }else{
        file=carpeta.getAbsolutePath().replace(carpeta.getName(), "");
        return file;
        }
    }
    public static String estructura(File dirObj, String absolute) {
        absolute = absolute.replace(dirObj.toString()+File.separator, "");
        return absolute;
    }
}
