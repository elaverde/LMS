/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.guardarCargar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author caano
 */
public class ImportarMetadata {

    private File direccion;
    private String nombreArchivo;
    private String metaArchivo;
    private Map mapOb = new HashMap();
    private InfoFormulario formula = new InfoFormulario();

    public ImportarMetadata(File direccion) {
        this.direccion = direccion;
    }

    public void obtenerArchivo() {

        try {//acedemos al archivo
            FileReader fileReader = new FileReader(direccion);
            //leer el archivo
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String cadena = "";
            int lineas = 0;

            //lee la linea mientras la linea no sea vacia y guarda la lineavacia en la variable cadena
            while ((cadena = bufferedReader.readLine()) != null) {
                if (lineas == 0) {
                    //leer el equipo
                    metaArchivo = cadena;
                }///primera linea
                if (metaArchivo.equals("xml_exportado_lms")) {
                    if (lineas >= 5) {
                        //leer el equipo
                         formula.obtenerDesdeArchivo(cadena);
                    } 
                    lineas++;
                }else{
                JOptionPane.showMessageDialog(null,
         "Archivo no valido",
             "Error",JOptionPane.INFORMATION_MESSAGE);
                    break;
                }

            }
//            bufferedReader.close();
//            fileReader.close();

        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        }
    }

    public Map getMap() {
        return formula.getInfoDesdeArchivo();
    }

}
