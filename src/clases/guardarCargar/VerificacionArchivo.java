/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.guardarCargar;



import clases.DAO.ArchivoDAO;

import clases.DTO.ArchivoDTO;
import clases.utileriasLMS;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author caano
 */
public class VerificacionArchivo {

    private String direccionArchivoPATHProyecto = "";
    private String idProyecto = "";
    private boolean ifExiste = false;
    private ArrayList<Files> arrayDeArchivos = new ArrayList<Files>();
    private File file;

    ArrayList<Boolean> arrExiste = new ArrayList<>();
    JFrame parent;

    public VerificacionArchivo() {

    }

    public VerificacionArchivo(String direccionArchivoPATHProyecto, String idProyecto, JFrame parent) {
        //coneccion......
        this.parent = parent;
        this.idProyecto = idProyecto;
        this.direccionArchivoPATHProyecto = direccionArchivoPATHProyecto;
        ArrayList<ArchivoDTO> arrArchivo = null;
        ArchivoDAO consultaAr = new ArchivoDAO();
        arrArchivo = consultaAr.readArchivosProyecto(idProyecto);

        validarExistencia(arrArchivo);

    }

    public String getDireccionArchivo() {
        return direccionArchivoPATHProyecto;
    }

    public void setDireccionArchivo(String direccionArchivo) {
        this.direccionArchivoPATHProyecto = direccionArchivo;
    }

    public ArrayList<Files> getArrayDeArchivos() {
        return arrayDeArchivos;
    }

    public void setArrayDeArchivos(ArrayList<Files> arrayDeArchivos) {
        this.arrayDeArchivos = arrayDeArchivos;
    }

    public ArrayList<Boolean> getArrExiste() {
        return arrExiste;
    }

    private void validarExistencia(ArrayList<ArchivoDTO> arrARchi) {

        if (arrARchi != null) {
            int cont = 0;
            for (ArchivoDTO archivoDTO : arrARchi) {
                File nuevoTemp = new File(direccionArchivoPATHProyecto + archivoDTO.getUrlComp_arc());
//                System.out.println(nuevoTemp);
                // System.out.println(direccionArchivoPATHProyecto+archivoDTO.getUrlComp_arc());
                if (nuevoTemp.exists()) {
                    arrExiste.add(true);
                   // System.out.println("si existe ->" + archivoDTO.getUrlComp_arc());
//                    
                } else {
                    arrExiste.add(false);
                    System.out.println("no existe ->" + direccionArchivoPATHProyecto+archivoDTO.getUrlComp_arc());
//
                    crearDialogo(nuevoTemp.getName(),archivoDTO);
//                    JLabel label1 = new JLabel(nuevoTemp.getName() + " no esta en la carpeta del proyecto");
//
//                    Object[] message = {"Recurso no encontrado: ", label1};
//                    Object[] options = {"Eliminar"};
//                    int sele = JOptionPane.showOptionDialog(null, message, "Warning",
//                            JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
//                            null, options, options[0]);
                    //  carga1.jTextPane1.setText(archivoDTO.getUrlComp_arc());
                    cont++;
                }
            }
        }

    }
    public void borrarArchivo(final File file){
    JLabel label1 = new JLabel(file.getName() + " no esta en el sistema");
//
        String[] buttons = {"Aceptar",};
        Object[] message = {"Recurso no encontrado: ", label1};
        JOptionPane optionPane = new JOptionPane(message,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_OPTION, null, buttons);

        JDialog dialog = optionPane.createDialog(parent, "Advertencia");
        dialog.setDefaultCloseOperation(
                JDialog.DO_NOTHING_ON_CLOSE);
        
        
        optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.println(evt.getNewValue());
                if (evt.getNewValue()!=null) {
                    if (evt.getNewValue().equals("Aceptar")) {
                        //borra                       
                        utils.deletedFile(file);
                    }
                }

               
            }
        });
        dialog.setVisible(true);
    
    
    }
    public void setParent(JFrame frame) {
        parent = frame;
    }
        utileriasLMS utils = new utileriasLMS();
    private void crearDialogo(String arch,final ArchivoDTO archiBorrar) {
        JLabel label1 = new JLabel(arch + " no esta en la carpeta del proyecto");
//
        String[] buttons = {"Aceptar",};
        Object[] message = {"Recurso no encontrado: ", label1};
        JOptionPane optionPane = new JOptionPane(message,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_OPTION, null, buttons);

        JDialog dialog = optionPane.createDialog(parent, "Advertencia");
        dialog.setDefaultCloseOperation(
                JDialog.DO_NOTHING_ON_CLOSE);
        
        
        optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.println(evt.getNewValue());
                if (evt.getNewValue()!=null) {
                    if (evt.getNewValue().equals("Aceptar")) {
                        //borra
                        File dir = new File(direccionArchivoPATHProyecto + archiBorrar.getUrlComp_arc());
                        utils.deletedFolderNoexiste(dir);
                        System.out.println("borra..."+dir);
                    }
                }

               
            }
        });
        dialog.setVisible(true);
        
    }
}
