/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.guardarCargar;

import clases.DTO.MetadataDTO;
import clases.DAO.MetadataDao;
import java.util.Map;

/**
 *
 * @author caano
 */
public class CargaMetadataBDArchivo {
    private String idarch_met;
    private Map mapDatoBd;

    public CargaMetadataBDArchivo(String idarch_met) {
        this.idarch_met = idarch_met;
        carga();
    }

   
    private boolean carga(){
        MetadataDTO metTp;
         MetadataDao met= new MetadataDao();
        metTp=  met.read(idarch_met);
        mapDatoBd=metTp.getDatosMeta();
        return false;
    }

    public Map getMapDatoBd() {
        return mapDatoBd;
    }
    
}
