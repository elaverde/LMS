/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.guardarCargar;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author caano
 */
public class InfoFormulario {
    private Map infoForm= new HashMap();
    private Map infoDesdeArchivo= new HashMap();

    public InfoFormulario() {
   
    }
    
    public void unirHashMap(Map unir){
        infoForm.putAll(unir);        
    }

    public Map getInfoForm() {
        return infoForm;
    }
    public void obtenerDesdeArchivo(String linea){
        String claveTmp="";
        String valortemp="";
       boolean encontro=false;
       Pattern pattern = Pattern.compile("(<catalog>)(.*)(</catalog>)");
                    Matcher matcher = pattern.matcher(linea);
                     while (matcher.find()) {
                        claveTmp = matcher.group(2);
                        encontro=true;
                    }
         Pattern pattern2 = Pattern.compile("(<langstring>)(.*)(</langstring>)");
                    Matcher matcher2 = pattern2.matcher(linea);
                     while (matcher2.find()) {
                        valortemp = matcher2.group(2);
                    }           
        if (encontro) {
           infoDesdeArchivo.put(claveTmp, valortemp);  
        }
            
    }

    public Map getInfoDesdeArchivo() {
        return infoDesdeArchivo;
    }
    
}
