/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.guardarCargar;

/**
 *
 * @author caano
 */
import java.io.File;
import java.io.FileWriter;//puede acceder la memoria del disco duro
import java.io.IOException;
import java.io.PrintWriter;//puede moodificar un archivo
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ExportarMetadata {

    private String usuarioModifi;
    private Date fechaModificacion;
    private Map datosModificados;
    private String direc;
    private File nomArchivo;
    

    public ExportarMetadata(String usuarioModifi,Map datosModificados,String direc,File nomArchivo) {
        this.usuarioModifi = usuarioModifi;
        this.datosModificados = datosModificados;
        this.direc=direc;
        this.nomArchivo=nomArchivo;
        //System.out.println("entra"+datosModificados);
    }
    
    public void generarArchivo (){      
            FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(nomArchivo+".xml");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            Iterator it= datosModificados.entrySet().iterator();
            printWriter.println("xml_exportado_lms");
            printWriter.println("<relation>");
            printWriter.println("<resource>");          
            printWriter.println("<identifier>"+nomArchivo.getName()+"</identifier>");
            
            while (it.hasNext()) {
                
                Map.Entry next = (Map.Entry)it.next();
                next.getKey();
                next.getValue();
                //System.out.println(next.getValue().toString());
                if (!next.getValue().toString().equals("")) { //si tiene informacion la escribr
                    printWriter.println("<catalogentry>");
                   printWriter.println(organizarInfo(usuarioModifi,next.getKey().toString(),next.getValue().toString())); 
                   printWriter.println("</catalogentry>");
                }  
                
            }
            printWriter.println("</resource>");
            printWriter.println("</relation>");
            
            System.out.println("Guardado");
            printWriter.close(); 
            fileWriter.close();
            
            JOptionPane.showMessageDialog(null,
         "El archivo se a guardado Exitosamente",
             "Guardado",JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
           JOptionPane.showMessageDialog(null,
         "El archivo se a guardado Exitosamente",
             ex.toString(),JOptionPane.INFORMATION_MESSAGE);
             
        }
            
    }
    private String organizarInfo(String usu, String id_meta, String datoModi) {
        Date d= new Date();
       // SimpleDateFormat formato= new SimpleDateFormat("dd/MM/yyyy");
        String temp="";    
             temp = "<catalog>" + id_meta + "</catalog><entry><langstring>" + datoModi + "</langstring></entry><extension><type/><label></label></extension>";
        return temp;
    }

}
