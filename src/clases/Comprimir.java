package clases;

/**
 *
 * @author edi
 */
import Jframes.ProgressBarGUI;
import Jframes.frm_principal;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
public class Comprimir {
    private static ZipOutputStream zos;
    private int progres = 0;
    private static String OS = System.getProperty("os.name").toLowerCase();
    public void UsersMetas() {
        File folder = new File(System.getProperty("user.home") + "/SENA");
        if (folder.isDirectory()) {
           
           // System.out.print("Folder Exist");
        } else {
             //System.out.print("Folder New");
           folder.mkdirs();
        }
    }
    public String GetUserFolder(){
        File folder = new File(System.getProperty("user.home") + "/SENA/");
        return  folder.getAbsolutePath();
    }
    
    public static String estructura(File dirObj, String absolute) {
        return absolute.replace(dirObj.toString()+File.separator, "");
    }
    
    public int getProgres()
    {
        return progres;
    }
        
    public void addFolders(File dirAbs, ArrayList<String> archivos,ZipOutputStream out) throws IOException {
        byte[] tmpBuf = new byte[1024];
        for (int i = 0; i < archivos.size(); i++) {
            File ar = new File(archivos.get(i));
            if(ar.isFile()){
            FileInputStream in = new FileInputStream(ar.getAbsolutePath());
            File arch=new File(archivos.get(i));
            System.out.println(" Adding: " + arch.getAbsolutePath());
            String dirp = estructura(dirAbs, arch.getAbsolutePath());
            out.putNextEntry(new ZipEntry(dirp));
            int len=0;
            long length= arch.length();
            int current=0;
            ProgressBarGUI bar = new ProgressBarGUI();
            while ((len = in.read(tmpBuf)) > 0) {
                current += len;
                progres=(int)(((double)current/(double)length)*100);
                System.out.println(progres);
                bar.setDataUpdate(progres);
                out.write(tmpBuf, 0, len);
            }
            bar.dispose();
            out.closeEntry();
            in.close();
            }
         /*   else{
            utileriasLMS u=new utileriasLMS();
            String url=ar.getAbsolutePath();
            url=url.replace(u.Get_workspace().toString()+File.separator+u.getProject()+File.separator, "");
            out.putNextEntry(new ZipEntry(url+File.separator));
            }*/
        }
         out.close();
    }
    public void addFoldersSelect(String Select,File dirAbs, ArrayList<String> archivos,ZipOutputStream out) throws IOException {
        byte[] tmpBuf = new byte[1024];
        for (int i = 0; i < archivos.size(); i++) {
            File ar = new File(archivos.get(i));
            if(ar.isFile()){
            FileInputStream in = new FileInputStream(ar.getAbsolutePath());
            File arch=new File(archivos.get(i));
            System.out.println(" Adding: " + arch.getAbsolutePath());
            if(arch.getName().equals("metadata.xml")){
            out.putNextEntry(new ZipEntry(arch.getName()));    
            }else{
            String dirp = estructura(dirAbs, arch.getAbsolutePath());
            out.putNextEntry(new ZipEntry(dirp));
            }
            int len=0;
            long length= arch.length();
            int current=0;
            ProgressBarGUI bar = new ProgressBarGUI();
            while ((len = in.read(tmpBuf)) > 0) {
                current += len;
                progres=(int)(((double)current/(double)length)*100);
                System.out.println(progres);
                bar.setDataUpdate(progres);
                out.write(tmpBuf, 0, len);
            }
            bar.dispose();
            out.closeEntry();
            in.close();
            }
         /*   else{
            utileriasLMS u=new utileriasLMS();
            String url=ar.getAbsolutePath();
            url=url.replace(Select+File.separator, "");
            out.putNextEntry(new ZipEntry(url+File.separator));
            }*/
        }
         out.close();
    }
    
     public void addFiles(ArrayList<String> archivos,ZipOutputStream out) throws IOException {
        byte[] tmpBuf = new byte[1024];
        for (int i = 0; i < archivos.size(); i++) {
            File ar = new File(archivos.get(i));
            FileInputStream in = new FileInputStream(ar.getAbsolutePath());
            File arch=new File(archivos.get(i));
            System.out.println(" Adding: " + arch.getAbsolutePath());
            out.putNextEntry(new ZipEntry(arch.getName()));
            int len=0;
            long length= arch.length();
            int current=0;
            ProgressBarGUI bar = new ProgressBarGUI();
            while ((len = in.read(tmpBuf)) > 0) {
                current += len;
                progres=(int)(((double)current/(double)length)*100);
                System.out.println(progres);
                bar.setDataUpdate(progres);
                out.write(tmpBuf, 0, len);
            }
            bar.dispose();
            out.closeEntry();
            in.close();
        }
         out.close();
    }
}
