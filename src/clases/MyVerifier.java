/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Usuario
 */
public class MyVerifier extends InputVerifier
        implements ActionListener {
        String tipo="";
        private JTextField FocusTextArea;
        
        public void setTipo(String tipoData){
            tipo = tipoData;
        }
        
        public void setFocusTextArea(JTextField info){
            FocusTextArea = info;
        }
         
        public boolean shouldYieldFocus(JComponent input) {
            String valor =FocusTextArea.getText();
            if(tipo.equals("Float")){
                //System.out.println("es Float");
                //JOptionPane.showMessageDialog(null, "es Float");
                if(!isFloat(valor)){
                    JOptionPane.showMessageDialog(null, "Error: el campo debe ser un numero de menos de 9 digitos","Error de validacion", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }

            if(tipo.equals("Integer")){
                //System.out.println("es Integer");
                //JOptionPane.showMessageDialog(null, "es Float");
                if(!isInt(valor)){
                    JOptionPane.showMessageDialog(null, "Error: el campo debe ser un numero de menos de 9 digitos","Error de validacion", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }


            if(tipo.equals("ShortString")){
                //System.out.println("es ShortString");
                //JOptionPane.showMessageDialog(null, "es ShortString");
                if(!isShortString(valor)){
                    JOptionPane.showMessageDialog(null, "Error: el campo solo permite 100 caracteres","Error de validacion", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }

            if(tipo.equals("MediumString")){
                //System.out.println("es MediumString");
                //JOptionPane.showMessageDialog(null, "es MediumString");
                if(!isMediumString(valor)){
                    JOptionPane.showMessageDialog(null, "Error: el campo solo permite 255 caracteres","Error de validacion", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }

            if(tipo.equals("LongString")){
                //System.out.println("es LongString");
                //JOptionPane.showMessageDialog(null, "es LongString");
                if(!isLongString(valor)){                    
                    JOptionPane.showMessageDialog(null, "Error: el campo solo permite 1.000 caracteres","Error de validacion", JOptionPane.INFORMATION_MESSAGE);
                    return false;
                }
            }
            
            System.out.println(FocusTextArea.getText()+"<--------------------------------------validador");
            System.out.println(tipo);
            return true;
        }
         
        //This method checks input, but should cause no side effects.
        public boolean verify(JComponent input) {
            return checkField(input, false);
        }
         
        protected void makeItPretty(JComponent input) {
            checkField(input, true);
        }
         
        protected boolean checkField(JComponent input, boolean changeIt) {
            /*if (input == amountField) {
                return checkAmountField(changeIt);
            } else if (input == rateField) {
                return checkRateField(changeIt);
            } else if (input == numPeriodsField) {
                return checkNumPeriodsField(changeIt);
            } else {*/
                return true; //shouldn't happen
            //}
        }
         
        
         
        public void actionPerformed(ActionEvent e) {
            JTextField source = (JTextField)e.getSource();
            //System.out.println("hola asd");
            shouldYieldFocus(source); //ignore return value
            source.selectAll();
        }
        
        
        public static boolean isInt(String str) {
            if (str == null) {
                return false;
            }
            int length = str.length();
            /*if (length == 0) {
                return false;
            }*/
            if (length > 9) {
                return false;
            }

            int i = 0;
            /*if (str.charAt(0) == '-') {
                if (length == 1) {
                    return false;
                }
                i = 1;
            }*/

            for (; i < length; i++) {
                char c = str.charAt(i);
                if (c < '0' || c > '9') {
                    System.out.println(c);
                    return false;
                }
            }
            return true;
        }

        public static boolean isFloat(String str) {
            if (str == null) {
                return false;
            }
            int length = str.length();
            /*if (length == 0) {
                return false;
            }*/
            if (length > 9) {
                return false;
            }

            int i = 0;
            /*if (str.charAt(0) == '-') {
                if (length == 1) {
                    return false;
                }
                i = 1;
            }*/

            for (; i < length; i++) {
                char c = str.charAt(i);
                if (c < '0' || c > '9') {
                    if(c=='.' || c==','){
                        System.out.println(c);
                        return true;
                    }
                    else{
                        System.out.println(c);
                        return false;
                    }
                }
            }
            return true;
        }

        public static boolean isShortString(String str) {

            int length = str.length();
            if (length > 100) {
                return false;
            }        
            return true;
        }


        public static boolean isMediumString(String str) {

            int length = str.length();
            if (length > 225) {
                return false;
            }        
            return true;
        }

        public static boolean isLongString(String str) {

            int length = str.length();
            if (length > 1000) {
                return false;
            }        
            return true;
        }
    }
