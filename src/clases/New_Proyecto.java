/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
/**
 *
 * @author edi
 */
public class New_Proyecto   implements Serializable  {
    private static final long serialVersionUID = 1L;
    private String id;
    private String tipo;
    private String key;
    public New_Proyecto(String id, String tipo, String key) {
        super();
        this.id = id;
        this.tipo = tipo;
        this.key = key;
    }
    public New_Proyecto() {
        super();
    }
    public String getId() {
        return id;
    }
    public String getTipo() {
        return tipo;
    }    
    public String getKey() {
        return key;
    }    
    public  void setWritten(String projejt) throws IOException {
        utileriasLMS url = new utileriasLMS();
        System.out.println(url.Get_workspace()+"/"+projejt+"/Project.lms");
        File archivo = new File(url.Get_workspace()+"/"+projejt+"/Project.lms");
        BufferedWriter confi = null;
        FileOutputStream file = new FileOutputStream(archivo);
        ObjectOutputStream escribir = new ObjectOutputStream(file);
        escribir.writeObject(new New_Proyecto(id, tipo, key));
        escribir.close();
        
        
        
    }
    public  String[] getRead(String uri) throws ClassNotFoundException, IOException {
        String id="",tipo="",key="";
         ObjectInputStream jom=null;
        try{
            utileriasLMS url = new utileriasLMS();
            File f=new File(uri);
            FileInputStream fis = new FileInputStream(f);
            jom=new ObjectInputStream(fis);
            
            while(true){
                New_Proyecto opened = (New_Proyecto) jom.readObject();
                id=opened.getId();
                tipo=opened.getTipo();
                key=opened.getKey();
            }
         
        }catch(IOException io){
        
        }finally{
              
        }
        try{
         jom.close();
        }catch(IOException io){
        
        }
        return new String[] {id,tipo,key} ;
     }
    
}
