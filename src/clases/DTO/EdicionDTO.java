/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DTO;

/**
 *
 * @author caano
 */
public class EdicionDTO {
    private int id_edi;
    private int idmet_edi;
    private int ideUse_edi;
    private int campo_edi;
    private String fecha_edi;

    public EdicionDTO() {
    }

    public EdicionDTO(int idmet_edi, int ideUse_edi, int campo_edi, String fecha_edi) {
        this.idmet_edi = idmet_edi;
        this.ideUse_edi = ideUse_edi;
        this.campo_edi = campo_edi;
        this.fecha_edi = fecha_edi;
    }

    public EdicionDTO(int id_edi, int idmet_edi, int ideUse_edi, int campo_edi, String fecha_edi) {
        this.id_edi = id_edi;
        this.idmet_edi = idmet_edi;
        this.ideUse_edi = ideUse_edi;
        this.campo_edi = campo_edi;
        this.fecha_edi = fecha_edi;
    }

    public int getId_edi() {
        return id_edi;
    }

    public void setId_edi(int id_edi) {
        this.id_edi = id_edi;
    }

    public int getIdmet_edi() {
        return idmet_edi;
    }

    public void setIdmet_edi(int idmet_edi) {
        this.idmet_edi = idmet_edi;
    }

    public int getIdeUse_edi() {
        return ideUse_edi;
    }

    public void setIdeUse_edi(int ideUse_edi) {
        this.ideUse_edi = ideUse_edi;
    }

    public int getCampo_edi() {
        return campo_edi;
    }

    public void setCampo_edi(int campo_edi) {
        this.campo_edi = campo_edi;
    }

    public String getFecha_edi() {
        return fecha_edi;
    }

    public void setFecha_edi(String fecha_edi) {
        this.fecha_edi = fecha_edi;
    }
    
    
}
