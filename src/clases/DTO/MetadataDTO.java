/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DTO;

import Jframes.frm_principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author caano
 */
public class MetadataDTO {
    
    private int id_met;
    private int idarc_met;
    private Map datosMeta=new HashMap();
    private ArchivoDTO arch;
    //inicializador
    {
        datosMeta=frm_principal.getDatosForm();
//        System.out.println(datosMeta);
    }
    public MetadataDTO() {
    }

    public MetadataDTO(int idarc_met) {
        this.idarc_met = idarc_met;
    }
    
    public MetadataDTO(int idarc_met, Map datosMeta) {
        this.idarc_met = idarc_met;
        this.datosMeta = datosMeta;
    }

    public MetadataDTO(int id_met, int idarc_met, Map datosMeta) {
        this.id_met = id_met;
        this.idarc_met = idarc_met;
        this.datosMeta = datosMeta;
    }
    

    public int getIdarc_met() {
        return idarc_met;
    }

    public void setIdarc_met(int idarc_met) {
        this.idarc_met = idarc_met;
    }
    //asegurar no null
    public Map getDatosMeta() {       
             Iterator it = this.datosMeta.entrySet().iterator();                
                while (it.hasNext()) {
                    Map.Entry next = (Map.Entry) it.next();                   
                        if (next.getValue() == null) {//algunos datos salen nulll.....
                            this.datosMeta.put(next.getKey(), "");//si sale null lo ponemos String vacio                      
                        }   
                }                 
        return datosMeta;
    }

    public void setDatosMeta(Map datosMetaentra) {
//        System.out.println("entra aca");
        for (Object object : this.datosMeta.keySet()) {
             Iterator it = datosMetaentra.entrySet().iterator();
                
                while (it.hasNext()) {
                    Map.Entry next = (Map.Entry) it.next();
                    if (next.getKey().toString().equals(object.toString())) {
                        if (next.getValue() == null) {//algunos datos salen nulll.....
                            this.datosMeta.put(object, "");
                        } else {
                            this.datosMeta.put(object, next.getValue());
                        }
                        
                    }
                }           
        }        
    }

    public ArchivoDTO getArch() {
        return arch;
    }

    public void setArch(ArchivoDTO arch) {
        this.arch = arch;
    }

    public int getId_met() {
        return id_met;
    }

    public void setId_met(int id_met) {
        this.id_met = id_met;
    }
    @Override
    public String toString(){
        return "met id:"+ idarc_met+",met map:"+this.datosMeta;
    }
    
}
