/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DTO;

/**
 *
 * @author caano
 * Modificacion edilson laverde
 */
public class GroupByFilesDTO {
    private int id_GbF;
    private int idpro_Gbf;
    private int compuesto_Gbf;
    private String tipo_Gbf;
    public GroupByFilesDTO() {
    }
    public GroupByFilesDTO(int idpro_Gbf, int compuesto_Gbf, String tipo_Gbf) {
        this.idpro_Gbf = idpro_Gbf;
        this.compuesto_Gbf = compuesto_Gbf;
        this.tipo_Gbf = tipo_Gbf;
    }
  
    public int getId_GbF() {
        return id_GbF;
    }
    public void setId_GbF(int id_GbF) {
        this.id_GbF = id_GbF;
    }
    public int getIdpro_Gbf() {
        return idpro_Gbf;
    }
    public void setIdpro_Gbf(int idpro_Gbf) {
        this.idpro_Gbf = idpro_Gbf;
    }
    public int isCompuesto_Gbf() {
        return compuesto_Gbf;
    }
    public void setCompuesto_Gbf(int compuesto_Gbf) {
        this.compuesto_Gbf = compuesto_Gbf;
    }
    public String getTipo_Gbf() {
        return tipo_Gbf;
    }
    public void setTipo_Gbf(String tipo_Gbf) {
        this.tipo_Gbf = tipo_Gbf;
    }
    
}
