package clases.DTO;
/**
 * @author edi
 */
public class ProjectDTO {
    private int id_pro;
    private String name_pro;
    private String tipo_pro;
    public ProjectDTO() {
    }
    public ProjectDTO(String name_pro, String tipo_pro) {
        this.name_pro = name_pro;
        this.tipo_pro = tipo_pro;
    }
    public ProjectDTO(int id_pro,String name_pro, String tipo_pro) {
        this.id_pro = id_pro;
        this.name_pro = name_pro;
        this.tipo_pro = tipo_pro;
    }
    public int getid_pro() {
        return id_pro;
    }
    public void setid_pro(int id_pro) {
        this.id_pro = id_pro;
    }
    public String getname_pro() {
        return name_pro;
    }
    public void setname_pro(String name_pro) {
        this.name_pro = name_pro;
    }
    public String gettipo_pro() {
        return tipo_pro;
    }
    public void settipo_pro(String tipo_pro) {
        this.tipo_pro= tipo_pro;
    }    
}