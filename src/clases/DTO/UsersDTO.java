/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DTO;

/**
 *
 * @author caano
 */
public class UsersDTO {
    private int id;
    private String name;
    private String lastName;
    private String linea;
    private String usuario;
    private String pass;
    private String tipoUsuario;

    public UsersDTO() {
    }
    //genera el id auto
    public UsersDTO(String name, String lastName, String usuario, String pass, String tipoUsuario, String linea) {
        this.name = name;
        this.lastName = lastName;
        this.linea = linea;
        this.usuario = usuario;
        this.pass = pass;
        this.tipoUsuario = tipoUsuario;
    }
    //para el retorno del id
    public UsersDTO(int id, String name, String lastName, String usuario, String pass, String tipoUsuario, String linea) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.linea = linea;
        this.usuario = usuario;
        this.pass = pass;
        this.tipoUsuario = tipoUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    @Override
    public String toString(){
        return "id: "+this.id+", name: "+this.name+", lastname: "+this.lastName+", pass: "+this.pass+", tipo: "+this.tipoUsuario+", usu: "+this.usuario+", linea: "+this.linea;
    }
}
