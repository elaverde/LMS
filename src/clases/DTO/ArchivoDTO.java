/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.DTO;

/**
 *
 * @author caano
 */
public class ArchivoDTO {

    private int id_arc;
    private int tipo;
    private int idGbF_arc;
    private String urlComp_arc;
    private String nombre;
    private String tipoR;
    
    private MetadataDTO meta = new MetadataDTO();

    public ArchivoDTO() {
    }

    public ArchivoDTO(String nombre) {
        this.nombre = nombre;
    }
 
    public ArchivoDTO(int idGbF_arc, String urlComp_arc) {
        this.idGbF_arc = idGbF_arc;
        this.urlComp_arc = urlComp_arc;
    }
    
    
    public ArchivoDTO(boolean tipo, int idgrupo, int idarchivo, String tip) {
        this.tipo = (tipo) ? 1 : 0;
        this.idGbF_arc =idgrupo;
        this.id_arc = idarchivo;
        this.tipoR=tip;
    }

    public ArchivoDTO(int id_arc, int idGbF_arc, String urlComp_arc) {
        this.id_arc = id_arc;
        this.idGbF_arc = idGbF_arc;
        this.urlComp_arc = urlComp_arc;
    }

    public int getIdGbF_arc() {
        return idGbF_arc;
    }
    public int gettipo() {
        return tipo;
    }
    public String gettipoSource() {
        return tipoR;
    }

    public void setIdGbF_arc(int idGbF_arc) {
        this.idGbF_arc = idGbF_arc;
    }

    public String getUrlComp_arc() {
        return urlComp_arc;
    }

    public void setUrlComp_arc(String urlComp_arc) {
        this.urlComp_arc = urlComp_arc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_arc() {
        return id_arc;
    }

    public void setId_arc(int id_arc) {
        this.id_arc = id_arc;
    }
    
    public void setIdMeta(int id_arc) {//solo setea los datos 
        this.meta.setIdarc_met(id_arc);
    }
    public void setMeta(MetadataDTO meta) {//solo setea los datos 
        this.meta.setDatosMeta(meta.getDatosMeta());
    }

    public MetadataDTO getMeta() {
        return meta;
    }
    
    @Override
    public String toString(){
    
        return "id: "+this.id_arc+", url: "+this.urlComp_arc+" meta:"+meta;
    }
    
}
