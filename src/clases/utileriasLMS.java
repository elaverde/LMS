/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Jframes.ProgressBarGUI;
import Jframes.add_resourse;
import Jframes.frm_principal;
import static Jframes.frm_principal.getDatosForm;
import clases.DAO.ArchivoDAO;
import clases.DTO.ArchivoDTO;
import clases.DAO.GroupByFilesDAO;
import clases.DTO.GroupByFilesDTO;
import clases.DTO.MetadataDTO;
import clases.DAO.MetadataDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author edi
 */
public class utileriasLMS {
    public static ArrayList<ArchivoDTO> archivoENMemo = new ArrayList<>();//archivos en memoria
    public static String Project = "";
    public static String idGroupFiles = "";
    public static String idFiles = "";
    private int progres=0;
    
    public static int total=0;
    public static double increment=0;
    public static double progresbar1=0;
    public void set_publish_project(){
        File folder = new File(System.getProperty("user.home") + "/Publicaciones SENA/");
        if(!folder.isDirectory()){
          folder.mkdirs();  
        }
    }
    public String get_publish_project(){
       set_publish_project(); 
       return System.getProperty("user.home") + "/Publicaciones SENA/";
    }
    
    public String getURLIOConfig() {
        return "config.obj";
    }
    public String getProject() {
        return Project;
    }
    public void setProject(String value) {
        Project = value;
    }
    public String getGroupFiles() {
        return idGroupFiles;
    }
    public void setGroupFiles(String value) {
        idGroupFiles = value;
    }
    public String getFiles() {
        return idFiles;
    }
    public void setFiles(String value) {
        idFiles = value;
    }
    public String Convert_Slash(String res){
        if (res==null) return null;
        if (File.separatorChar=='\\') {
            // From Windows to Linux/Mac
            return res.replace( File.separatorChar,'/');
            
        } else {
            // From Linux/Mac to Windows
            return res.replace( File.separatorChar,'\\');
        }
    }
    public File Get_workspace() {
        File folder = null;
        try {
            configuration path = new configuration();
            String[] myconfig;
            myconfig = path.getConfig();
            folder = new File(myconfig[0]);
            if (!folder.exists()) {
                folder = null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(utileriasLMS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(utileriasLMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return folder;
    }
    public static void permitions(String url) {
        final File file = new File(url);
        file.setReadable(true, false);
        file.setExecutable(true, false);
        file.setWritable(true, false);
    }
    public String partial_path(String url) {
        url = url.replace(Get_workspace().toString()+File.separator, "");
        return url;
    }
    public static  ArchivoDTO info_element( String url) {
        ArchivoDAO arcGuardarBd = new ArchivoDAO();
        ArchivoDTO arcRead;
        arcRead = (ArchivoDTO) arcGuardarBd.readGroup(url);
        return arcRead;
    }

    /**
     *
     * @param dir
     * @return
     */
    public boolean deletedFolder(File dir) {
        ArchivoDTO arcRead2=null;
        if (!dir.exists()) {
            return false;
        }
        if (!dir.isDirectory()) {
            ArchivoDAO arcDeleteBd = new ArchivoDAO();
            ArchivoDTO arcRead;
            arcRead = (ArchivoDTO) arcDeleteBd.readGroup( partial_path(dir.getAbsoluteFile().toString()));
            arcDeleteBd.deleteonGroup(arcRead.getId_arc());
            arcDeleteBd.deleteGroup(arcRead.getIdGbF_arc());
            dir.delete();
            return true;
        }else{
        String[] files = dir.list();
       
        
        ArchivoDAO arcDeleteBd[]=new ArchivoDAO[files.length]; 
        ArchivoDTO arcRead[]=new ArchivoDTO[files.length];
        for (int i = 0, len = files.length; i < len; i++) {
            File f = new File(dir, files[i]);
            if (f.isDirectory()) {
                arcDeleteBd[i] = new ArchivoDAO();
                arcRead[i] = (ArchivoDTO) arcDeleteBd[i].readGroup( partial_path(f.getAbsoluteFile().toString()));
                System.out.println("Itearacion "+i+" ID "+arcRead[i].getId_arc());
                arcDeleteBd[i].deleteonGroup(arcRead[i].getId_arc());
                arcDeleteBd[i].deleteGroup(arcRead[i].getIdGbF_arc());
                dir.delete();
                deletedFolder(f);
            } else {
                arcDeleteBd[i] = new ArchivoDAO();
                System.out.println("path " + partial_path(f.getAbsoluteFile().toString()));
                arcRead[i] = (ArchivoDTO) arcDeleteBd[i].readGroup( partial_path(f.getAbsoluteFile().toString()));
                System.out.println("Itearacion "+i+" ID "+arcRead[i].getId_arc());
                arcDeleteBd[i].deleteonGroup(arcRead[i].getId_arc());
                arcDeleteBd[i].deleteGroup(arcRead[i].getIdGbF_arc());
                dir.delete();
                f.delete();
            }
        }
        return dir.delete();
        }
    }
    public boolean deletedFile(File dir) {
        ArchivoDTO arcRead2=null;
        if (!dir.exists()) {
            System.out.println("aaa??");
            return false;
        }    
        System.out.println("path " +dir.getName() );
        return dir.delete();    
    }
    public boolean deletedFolderNoexiste(File dir) {///solo copie y pegue no comprueba si el archivo no existe
        ArchivoDTO arcRead2=null;
        if (dir.exists()) {
            return false;
        }
        if (!dir.isDirectory()) {
            ArchivoDAO arcDeleteBd = new ArchivoDAO();
            ArchivoDTO arcRead;
            arcRead = (ArchivoDTO) arcDeleteBd.readGroup( partial_path(dir.getAbsoluteFile().toString()));
            arcDeleteBd.deleteonGroup(arcRead.getId_arc());
            arcDeleteBd.deleteGroup(arcRead.getIdGbF_arc());
            dir.delete();
            return true;
        }else{
        String[] files = dir.list();
       
        
        ArchivoDAO arcDeleteBd[]=new ArchivoDAO[files.length]; 
        ArchivoDTO arcRead[]=new ArchivoDTO[files.length];
        for (int i = 0, len = files.length; i < len; i++) {
            File f = new File(dir, files[i]);
            if (f.isDirectory()) {
                arcDeleteBd[i] = new ArchivoDAO();
                arcRead[i] = (ArchivoDTO) arcDeleteBd[i].readGroup( partial_path(f.getAbsoluteFile().toString()));
                System.out.println("Itearacion "+i+" ID "+arcRead[i].getId_arc());
                arcDeleteBd[i].deleteonGroup(arcRead[i].getId_arc());
                arcDeleteBd[i].deleteGroup(arcRead[i].getIdGbF_arc());
                dir.delete();
                deletedFolder(f);
            } else {
                arcDeleteBd[i] = new ArchivoDAO();
                System.out.println("path " + partial_path(f.getAbsoluteFile().toString()));
                arcRead[i] = (ArchivoDTO) arcDeleteBd[i].readGroup( partial_path(f.getAbsoluteFile().toString()));
                System.out.println("Itearacion "+i+" ID "+arcRead[i].getId_arc());
                arcDeleteBd[i].deleteonGroup(arcRead[i].getId_arc());
                arcDeleteBd[i].deleteGroup(arcRead[i].getIdGbF_arc());
                dir.delete();
                f.delete();
            }
        }
        return dir.delete();
        }
    }
    public Map importExcel(String input){
               Map<String, String> datos = new HashMap<String, String>();
        try
    {
        FileInputStream file = new FileInputStream(new File(input));
        //Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
 
        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            //For each row, iterate through all the columns
            Iterator<Cell> cellIterator = row.cellIterator();
            int i=0;
            String url="",id="",values="";
            while (cellIterator.hasNext()) 
            {
                Cell cell = cellIterator.next();
                
                //Check the cell type after eveluating formulae
                //If it is formula cell, it will be evaluated otherwise no change will happen
                i+=1;
                switch (evaluator.evaluateInCell(cell).getCellType()) 
                {
                   case Cell.CELL_TYPE_NUMERIC:
                        break;
                    case Cell.CELL_TYPE_STRING:
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        //Not again
                        break;
                }
                
                switch (i) 
                {
                    //url//
                    case 1:
                        url=cell.getStringCellValue();
                        break;
                    //id input//    
                    case 2:
                        id=cell.getStringCellValue();
                        break;
                    //values //    
                    case 3:
                        System.out.println();
                            
                        if (cell.getCellType()!=0) {
                            values=cell.getStringCellValue();
                            datos.put(id, values);
                            datos.put("url-file", url);
                        }else{
                            Calendar cal= Calendar.getInstance();
                            cal.setTime(cell.getDateCellValue());
                            
                            SimpleDateFormat formato= new SimpleDateFormat("dd/MM/yyyy");
                           values= formato.format(cal.getTime());
                            System.out.println(values);
                            datos.put(id, values);
                            datos.put("url-file", url);
                        }
                        
                        
                        //Not again
                        break;
                }
            }
           
        }
        
       
        file.close();
          return(datos);
    } 
    catch (Exception e) 
    {
        e.printStackTrace();
    }
          return(datos);
    }
    public void exportExcel(String namefile,Map datos,String output){
           //Blank workbook
                XSSFWorkbook workbook = new XSSFWorkbook(); 
                //Create a blank sheet
                XSSFSheet sheet = workbook.createSheet("LMS_SENA");
               
                Iterator it = datos.entrySet().iterator();
                //This data needs to be written (Object[])
                Map<String, Object[]> data = new TreeMap<String, Object[]>();
                int i=1;
                data.put(Integer.toString(i)  , new Object[] {"identifier", "catalog", "langstring"});

                while (it.hasNext()) {
                        Map.Entry e = (Map.Entry)it.next();
                        i+=1;
                        data.put(Integer.toString(i), new Object[] {namefile, e.getKey(), e.getValue()});
                } 
                //Iterate over data and write to sheet
                Set<String> keyset = data.keySet();
                int rownum = 0;
                for (String key : keyset)
                {
                    Row row = sheet.createRow(rownum++);
                    Object [] objArr = data.get(key);
                    int cellnum = 0;
                    for (Object obj : objArr)
                    {
                       Cell cell = row.createCell(cellnum++);
                       if(obj instanceof String)
                            cell.setCellValue((String)obj);
                        else if(obj instanceof Integer)
                            cell.setCellValue((Integer)obj);
                    }
                }
                try
                {
                    //Write the workbook in file system
                    FileOutputStream out = new FileOutputStream(new File(output+"/ims.xlsx"));
                    workbook.write(out);
                    out.close();

                } 
                catch (Exception e) 
                {
                    e.printStackTrace();
                }
    }
    public void set_total (int n){
        this.total=n;
        set_intcrement(n);
    }
    public int get_total (){
        return this.total;
    }
    public void set_intcrement(double n){
        this.increment=100/n;
        System.out.print(this.increment+" mi operacion "+n);
    }
    public double get_intcrement(){
        return this.increment;
    }
    public void set_progress (){
       this.progresbar1=get_intcrement()+this.progresbar1;
    }
    public double get_progress (){
       return this.progresbar1;
    }
    public static int getFilesCount(File file) {
        File[] files = file.listFiles();
        int count = 0;
        for (File f : files)
          if (f.isDirectory())
            count += getFilesCount(f);
          else
            count++;
     return count;
    }
    public  void copyFolder(File src, File dest, int groupFilesID)
    {
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
                System.out.println("Directory copied from "
                        + src + "  to " + dest);
               
            }
            String files[] = src.list();
            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile, groupFilesID);
            }
            //listar archivos
            System.out.println(dest.listFiles());

        } else {
            InputStream in = null;
            try {
                //if file, then copy it
                in = new FileInputStream(src);
                OutputStream out = new FileOutputStream(dest);
                byte[] buffer = new byte[1024];
                int length;
                int current=0;
            //    add_resourse bar = new add_resourse();
                //copy the file content in bytes
                long len= src.length();
                System.out.println(len);
                while ((length = in.read(buffer)) > 0) {
                    current += length;
                //    progres=(int)(((double)current/(double)len)*100);
                //    bar.setDataUpdate(progres);
                    out.write(buffer, 0, length);
                }   set_progress ();
                in.close();
                out.close();
                System.out.println("File copied from " + src + " to " + dest);
                 //////////////////copia a bd
                utileriasLMS utilname = new utileriasLMS();
                ArchivoDTO arcAgregar = new ArchivoDTO();
                ArchivoDAO arcGuardarBd = new ArchivoDAO();
                String nuevoPat = partial_path (dest.getAbsolutePath().toString());
                arcAgregar.setIdGbF_arc(groupFilesID);//guarda id grup by files
                arcAgregar.setUrlComp_arc(nuevoPat);//url desde el proyecto , crear funcion para dividirla o pasarle solo esa url...
                int result=arcGuardarBd.insertAutoIndex(arcAgregar);
                if(result!=0){

                    MetadataDTO datos=new MetadataDTO(result, frm_principal.getDatosForm()); 
                    MetadataDao insert= new MetadataDao();
                    insert.create(datos);

                }
            }
            
            catch (FileNotFoundException ex) {
                Logger.getLogger(utileriasLMS.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(utileriasLMS.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(utileriasLMS.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
   

      
    }
    public boolean openFile(JTree tree,List<String>datos ) {
        boolean encontrado=false;
        for (int row = 0; row < tree.getRowCount(); row++) {
            boolean igual=true;
            TreePath paths = tree.getPathForRow(row);
            tree.expandRow(row);
            int i=paths.getPathCount();
            System.out.print(i+" nnnnnnnnn "+datos.size());
            if(i!=datos.size()){
                igual=false;
            }else{
                for(int j=0;j<i;j++){
                    if(!datos.get(j).equals(paths.getPathComponent(j).toString())){
                    igual=false;
                    }
                }
            }
            if(igual==true){
            tree.setSelectionRow(row);
            tree.expandRow(row);
            encontrado=true;
            return encontrado;
            
            }       
        }
        return encontrado;
    }
}
