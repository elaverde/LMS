/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.usuarios;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Multimedia
 */
public class Validacion {
    
    private Pattern pattern;
    private Matcher matcher;
    
    private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public Validacion(){
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
    
    public boolean correoValidacion(final String data){
        matcher = pattern.matcher(data);
        return matcher.matches();
    }
    
    public boolean vacioValidacion(final String data){
        return data.equals("");
    }
    
    public boolean vacioValidacionPass(final int data){
        if(data == 0){
            JOptionPane.showMessageDialog(null, "Error: el campo Contraseña es Obligatorio");
            return false;
        }
        return true;
    }
    
    public boolean camposValidacion(final Map data){
       
        
        if(vacioValidacion(String.valueOf(data.get("nombre")))){
            JOptionPane.showMessageDialog(null, "Error: el campo Nombre es Obligatorio");
             return false;
        }
        if(vacioValidacion(String.valueOf(data.get("apellido")))){
            JOptionPane.showMessageDialog(null, "Error: el campo Apellido es Obligatorio");
             return false;
        }
        
        if(vacioValidacion(String.valueOf(data.get("correo")))){
            JOptionPane.showMessageDialog(null, "Error: el campo Correo Electrónico es Obligatorio");
             return false;
        }
         if(!correoValidacion(String.valueOf(data.get("correo")))){
            JOptionPane.showMessageDialog(null, "Error: el formato del Correo Electrónico ");
             return false;
        }
       
        return true;
    }
    
}
