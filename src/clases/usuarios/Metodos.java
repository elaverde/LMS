/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.usuarios;

/**
 *
 * @author HP
 */
public class Metodos {
    
    public int convertRol(String rol)
    {
        int rolInt=0;
        switch(rol)
        {
            case"Gestor repositorio":
                rolInt=1;
            break;
            case"Líder de línea de producción":
                rolInt=2;
            break;
            case"Experto técnico/asesor pedagógico":
                rolInt=3;
            break;
            case"Programador/diseñador":
                rolInt=4;
            break;
        }
        return rolInt;
    }
    
}
