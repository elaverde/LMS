/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;
import java.io.Serializable;
import java.io.Reader;
import javax.swing.JOptionPane;

/**
 *
 * @author edi
 */
public class configuration implements Serializable {

    private static final long serialVersionUID = 1L;
    private String WordSpace;
    private String bd;

    public configuration(String WordSpace, String bd) {
        super();
        this.WordSpace = WordSpace;
        this.bd = bd;
    }

    public configuration() {
        super();
    }
    public String getWordSpace() {
        return WordSpace;
    }
    public String getbd() {
        return bd;
    }
    public  void setConfig() throws IOException {
        utileriasLMS url = new utileriasLMS();
        File archivo = new File(url.getURLIOConfig());
        BufferedWriter confi = null;
        FileOutputStream file = new FileOutputStream(archivo);
        ObjectOutputStream escribir = new ObjectOutputStream(file);
        escribir.writeObject(new configuration(WordSpace, bd));
        escribir.close();
    }
    public  String[] getConfig() throws ClassNotFoundException, IOException {
        utileriasLMS leer2 = new utileriasLMS();
          
        File conf = new File(leer2.getURLIOConfig());
        if (!conf.exists()){
         return new String[] {"",""} ;
        }else{
        String WordSpace="",bd="";
         ObjectInputStream jom=null;
        try{
            utileriasLMS url = new utileriasLMS();
            File f=new File(url.getURLIOConfig());
            FileInputStream fis = new FileInputStream(f);
            jom=new ObjectInputStream(fis);
            
            while(true){
                configuration confi = (configuration) jom.readObject();
                WordSpace=confi.getWordSpace();
                bd=confi.getbd();
            }
         
        }catch(IOException io){

            
        }finally{
              
        }
        try{
         jom.close();
        }catch(IOException io){

            
        }
        
        return new String[] {WordSpace,bd} ;
        }
     }
    
}
