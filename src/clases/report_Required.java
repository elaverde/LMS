package clases;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author edils
 */
public class report_Required {
    public boolean valid=true; 
    public String id =  "";
    public String tipo ="";
    public String help ="";
    public String url = "";
    public String lom = "";
    public String title = "";
    public String index = "";
    private enum items {
    id, tipo, help,url,lom,title,index;
    }
    public void set(boolean valid) {
        this.valid=valid;
    }
       public void set(String id, String decription, String help, String url,String lom,long index) {
        this.valid=false;
        this.id = id;
        //obtener tipo
        Pattern patternTipo = Pattern.compile("(>)(?!<)(.*)(</t)");
        Matcher matcher = patternTipo.matcher(decription);
        while (matcher.find()) {
            this.tipo = matcher.group(2);
        }
        //obtener label titulo
        Pattern patternTitle =  Pattern.compile("(l>)(?!<)(.*)(</l)");
        Matcher matcher2 = patternTitle.matcher(decription);
        while (matcher2.find()) {
            this.title = matcher2.group(2);
        }
        this.help = help;
        this.url = url;
        this.lom = lom;
        this.index = Long.toString(index);
    }
    public boolean data_valid(){
        return this.valid;
    }
    public String get(String dato) {
        items dat=items.valueOf(dato);
        String info="";
        switch (dat) {
            case id:
                info = this.id;
                break;
            case tipo:
                info = this.tipo;
                break;
            case title:
                info = this.title;
                break;    
            case help:
                info = this.help;
                break;
            case url:
                info = this.url;
                break; 
            case lom:
                info = this.lom;
                break; 
            case index:
                info = this.index;
                break;     
            default:
                info =this.id;
                break;
        }
        return info;
    }
}