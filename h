[33mcommit 13d7d10508df67b9ebcfcea99de729ad30a0fdec[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 28 15:40:19 2016 -0500

    arreglo conflito private

[33mcommit 43f6bd674e9f70341d7504119531affda5d24e7b[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 28 12:03:25 2016 -0500

    !!Dos botones nuevos!!
    - 1 Guardar
    - 2 Guardar en grupo
    - Consulta en la base de datos si el archivo tiene mas archivos asociados activa el boton 2 de no ser asi lo oculta

[33mcommit c728ea12c0eeb98710e67268a1ad8a574f8a55a5[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 28 11:53:38 2016 -0500

    !!Nuevo Monitoreo de archivos!!
    - Arreglo para que no se muestre el archivo lms
    - Arreglo bug guardar archivos copiados
    - Nuevo metodo dao archivos para lectura de id grupo

[33mcommit 77fb997220fa04bc19d5a42346bb0ea8f359aaad[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 27 23:56:23 2016 -0500

    se guarda un archivo a la vez... continua guardado general de todos los archivos en la bd

[33mcommit 05e8c766206115a10dbe83fcf52c24f562302768[m
Merge: 3b96244 e532a35
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 27 23:51:54 2016 -0500

    arreglo conflic

[33mcommit 3b9624477c031f6149744eb914eaef1f4063d320[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 27 23:49:47 2016 -0500

    mirar lo botones para guardado geral de todos los metadatas q se esten diligenciados, revisar si el metadata se comienza de 0 o lo q tenia...

[33mcommit a89af0a0d8712e3b0d720b7c42aeee8be8a1ae07[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 27 23:43:39 2016 -0500

    guarda y carga metadata guardado en memoria y en BD

[33mcommit 146ab29b47caabd9b7bb3e209d1036c31e86182a[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 27 23:29:32 2016 -0500

    carga y guardado de datos con el jtree , se leeciona el archivo en el jtree se seleciona los datos

[33mcommit e532a35532645bcc2a6739c511e54e98693e5725[m
Author: Multimedia <Multimedia@Admin>
Date:   Wed Apr 27 22:11:47 2016 -0500

    Template de fondo

[33mcommit f6624cf8a0f5634f280070f47e155c6a3b64b17a[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Wed Apr 27 12:21:44 2016 -0500

    - Correccion Bugs en las clases DAO y DTO de group files
    - Agregacion en la clase utileriasLMS de metodos para establecer proyecto el id del grupo y del archivos falta un concetor tiempo real
    - Agragacion de DTO groufiles un metodo que inserta y ademas devuelve el valor insertado para relacionarlo con los archivos
    - Codificacion para que guarde los grupos de archivos con el id del proyecto

[33mcommit c22fb8975a33186541874de26dc0cfc34ad7ac5c[m
Merge: 8f15a88 f69d7e4
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Tue Apr 26 19:17:44 2016 -0500

    Merge branch 'master' of https://gitlab.com/elaverde/LMS
    Arreglo Conflicts

[33mcommit 8f15a889a47284f1417d0af9a45eeedfdce41b60[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Tue Apr 26 19:06:30 2016 -0500

    - Correcion bug Jtree
    - Creacion de clases projectdao projectdto para la tabla proyectos
    - Guarda los datos del proyecto en la base de datos
    - Al Guardar Verifica si el id existe para no repetir id

[33mcommit f69d7e470dde03b152e0b74ce9e63d7f93339619[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Tue Apr 26 18:30:48 2016 -0500

    implementacion de registro e ingreso de usuario con bd, se modifico mainframe y crear

[33mcommit 2c1b943b5bed487f2f006b9256711a1c5bc62d08[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Tue Apr 26 18:03:11 2016 -0500

    creacion de clases de conexion

[33mcommit 5444aec77cad977b1f8fedd3b4fe152de6083b4d[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Tue Apr 26 00:18:01 2016 -0500

    arreglo funcino delete metadataDao

[33mcommit 4cd1cf1c7787f6458cfbd29509d0f11b7b284444[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Tue Apr 26 00:07:34 2016 -0500

    complemeto clase archivo dao funcion readall retorna todos los archivos de la bd

[33mcommit 195ec2b6967af6b326f2034e221603d9aaea1793[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 25 23:37:16 2016 -0500

    correcion conexion.... optimizar pool conexiones..

[33mcommit c65b07d85bfc57567428de799252922470cee08d[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 25 23:01:29 2016 -0500

    modifico clase utileriaLMS funcion copyfolder, para agregar archivo a la bd

[33mcommit dc6375d2270ea9d019cc3efcdb13f7986f2f488c[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 25 22:19:33 2016 -0500

    delte de archivo borra meta hijos

[33mcommit 6d9485679e7d12c5bfeb0958e0ec08a862a9160e[m
Merge: 2853a9d 5607391
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 25 20:01:31 2016 -0500

    conflito..

[33mcommit 2853a9d1a0939c8289e580cdfad464fac658c9db[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 25 19:58:32 2016 -0500

     archivodao persistencia de archivo

[33mcommit 56073914ce542076231e068bc99f91790101ef67[m
Author: Nicolás Gómez <hagane117@gmail.com>
Date:   Mon Apr 25 19:49:15 2016 -0500

    jTree1ValueChanged asociacion con autocomplete

[33mcommit 2b8ed63a88dacb84b1aa1b810a653a8948669798[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 25 15:33:45 2016 -0500

    Correcion bug varible staica utils para que se mantega valor del proyecto

[33mcommit 4cc4828bed6d698116cb7a367da7be53405c1bdb[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 25 15:12:09 2016 -0500

    !!MEJORAS!!
    	- Anulacion de direciones en jtree para dar mayor expericia app usuario
    		-Nicolas jTree1ValueChanged le doy la url completa de archivo para que lo asocie a sus clases
    	-Apertura del proyecto refresh en el jtree
    	-Craer proyecto refresh en el jtree

[33mcommit 68dfd13d16de1cca208259575e301b96df77f26d[m
Author: Multimedia <Multimedia@Admin>
Date:   Sun Apr 24 23:54:38 2016 -0500

    Inicio de usuario y registro.... falta hacer la el query de insercion de usuario y el query de validacion si ya esta registrado, del mismo modo la validacion de acceso al sistema.

[33mcommit 78803222a956ce3023c1cf2608457bbda14e42cb[m
Author: Multimedia <Multimedia@Admin>
Date:   Sat Apr 23 20:07:26 2016 -0500

    Arreglo del mensaje de ayuda

[33mcommit f6e2dceea9e672e0338f183b753a7e3676321730[m
Author: Multimedia <Multimedia@Admin>
Date:   Sat Apr 23 20:01:10 2016 -0500

    Adiccion de propiedad help para la descripcion de campos

[33mcommit 71eea8d88eecf0cd7fbadac955c8b2f283ef4d6e[m
Author: Multimedia <Multimedia@Admin>
Date:   Wed Apr 20 00:40:17 2016 -0500

    Creacion del sistema de ayuda de los campos

[33mcommit a0886c9e9f31afee0ba74b68d77205cf04c8619d[m
Author: christian <cristian31@gmail.com>
Date:   Sat Apr 23 23:50:00 2016 +0000

    Replace ayudaIcon.png

[33mcommit 14c53bbe16f2c9e0e1ff3f03d8481174249ee67f[m
Author: Nicolás Gómez <hagane117@gmail.com>
Date:   Sat Apr 23 10:08:36 2016 -0500

    Reconocimiento del formato del archivo y auto llenado del campo ims_adv_technical_format

[33mcommit 271a8235925bf07e6db3a325b623b9303afef87f[m
Author: Nicolás Gómez <hagane117@gmail.com>
Date:   Sat Apr 23 08:51:06 2016 -0500

    Modificacion validar y auto llenado del campo ims_adv_technical_size en bytes

[33mcommit c44b311f13f400d54ab5a7a34b0bb8119914f333[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Fri Apr 22 22:01:19 2016 -0500

    modificacion campo tabla metadatas

[33mcommit a8657ba450fd9b88bad3484df1e2f8e6cb5294a3[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Fri Apr 22 21:30:33 2016 -0500

    comentariado de unas salidas de consola...

[33mcommit a5ed4307fd1e27cac2203db1fd6e6117b25f21fe[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Fri Apr 22 21:26:33 2016 -0500

    creacion de las clases DAO para guardar los metadatos y recuperarlos , creacion de clases para guardar los metadatos por archivo, mirar lo del historial...o update

[33mcommit aa44c03083ab7a4231eee60eff8ae8a7a9d475a2[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 15:55:15 2016 -0500

    Validador el main este validador revisa 3 aspectos
    	-Que la base de datos exista
    	-Que word space exista
    	-Y que el archivo configuracion exista
    Si existen dejara iniciar el app normalmente en caso de no saldra menu y hasta que no lo haga no dejara iniciar esta app

[33mcommit 297a834d24dc52f3ba4c4ef92dda194e5048046d[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 14:48:10 2016 -0500

    Arreglos
    	- Clase SqliteConexion adaptacion de url para sea el usuario quien define donde queda la base de datos
    	- Arreglo del filtro configuracion de preferencias para cuando elige solo mostrar la bd

[33mcommit a278e751a26dadd3d67c6038eea79ded14c26210[m
Merge: 00cc391 1e82eeb
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 14:37:43 2016 -0500

    Arreglo conflitos private

[33mcommit 00cc391093dc4f8d03d2b9fbb91d316ae72f86a9[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 13:55:01 2016 -0500

    adicion 72 campos de metadata

[33mcommit 1e82eebe99c8bdd97e51c89a2c91adbf023cd2e0[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Fri Apr 22 12:46:29 2016 -0500

    inicio de creacion de las clases metadata para consulta bd

[33mcommit d8855a45a15bff236a9be20b5a016f08493e8ea7[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Fri Apr 22 10:29:12 2016 -0500

    filtro de archivo para clase importar y exportar

[33mcommit 4d84c9c4a31b61e83768e3d612c90775a4ae910b[m
Merge: b99c59e 43bcd92
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 09:05:56 2016 -0500

    arreglo conflictos

[33mcommit b99c59e7ba13a2404f78c7ef57cc3d41ce635771[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Fri Apr 22 08:56:05 2016 -0500

    base de datos parcial faltan los 72 metadatos no se como llamarlos si por id que tiene o por label de identificación

[33mcommit 43bcd9290902a4fd65f3764845551444da2635bb[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 18:25:37 2016 -0500

    cambio mensaje de importacion metadata en el form principal

[33mcommit 9212b24c770c2e1865301da3d80d54b6dc4522ce[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 18:21:10 2016 -0500

    cambio en la calse importarMetadata y exportarMetadata

[33mcommit f3694943972646da33004741cbcc370bc8c4ffdc[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 17:55:21 2016 -0500

    Se cambio guardar y cargar por, importar exportar

[33mcommit 13d519deaf6fa5d98743c99167f2a58023ab46e5[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 16:54:59 2016 -0500

    conexion sqllite single

[33mcommit 915bf6bbb09f9ec9d17e4269ebf1e35116090b44[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 21 12:49:31 2016 -0500

    !!NUEVO!!
    	Menu para agregar recursos al proyecto copy paste por code
    	Creacion de un archivo abrir el proyecto
    	Adicion de un botone y item al layaud principal

[33mcommit 380463f9233610efb8ea4ec760b50eab8ed07c54[m
Merge: ec3d41c 6d12f34
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 21 08:48:53 2016 -0500

    Merge branch 'master' of https://gitlab.com/elaverde/LMS

[33mcommit ec3d41cee6ff2b7c72496782d8787dc9e4cb4357[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Thu Apr 21 08:48:38 2016 -0500

    arreglo conflitos

[33mcommit 6d12f34b295ba65b188a55c7e82ad286a66c26d9[m
Merge: ad23030 9614c51
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 01:38:52 2016 -0500

    nbproyect/private.. properties ...

[33mcommit ad23030a202502a3a61c119ae2513301499b4daf[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 01:36:55 2016 -0500

    clase conexion sqlite

[33mcommit 9614c51136baa853c7945303fdfa9628091faaf3[m
Author: carlos Andres Orjuela lasso <lp.carlosorjuela@gmail.com>
Date:   Thu Apr 21 04:46:22 2016 +0000

    Update PanelCatalog.java

[33mcommit 2dd2ba731d4a097dfa69ce65fb6afcdec797d744[m
Author: Nicolás Gómez <hagane117@gmail.com>
Date:   Wed Apr 20 23:28:13 2016 -0500

    Prueba captura de peso carpetas

[33mcommit 1803891087d1db24a09ed62346bed34da600f262[m
Author: Usuario <Usuario@AMD>
Date:   Wed Apr 20 21:13:14 2016 -0500

    arreglo campos requerido

[33mcommit 67f32362f5290e448ac14e34b73bec8881cdbf97[m
Author: Usuario <Usuario@AMD>
Date:   Wed Apr 20 21:11:06 2016 -0500

    arreglo campos requerido

[33mcommit a78b027eb1126cee0d4d7de5eb2f83d2033b450b[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Wed Apr 20 16:27:56 2016 -0500

    Validaciones en preferencias
    	-Si se elimina el archivo
    	-Si la ruta no existe
    	-adicion path a la creacion de carpetas

[33mcommit 4825907f23d3c97415073d17d6db9e3d634e7b97[m
Merge: c8b0874 1a2c172
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 20 12:55:25 2016 -0500

    se modificaron las carpetas nbproject/private.. para revisar

[33mcommit c8b087407b2efc0370bdcd84283d054ef6317d24[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 20 12:27:53 2016 -0500

    JFileChooser para cargar y guardar, se modifico los eventos de los botones gargar y guardar del frm_principal.java

[33mcommit 1a2c172504525462540294dec2acc9059c4cf029[m
Merge: 509f5c0 605a7fc
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Wed Apr 20 12:23:14 2016 -0500

    conflicto con guarda lms

[33mcommit 509f5c045a834604b7b4049222f40c53aa798f85[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Wed Apr 20 11:59:38 2016 -0500

    Nuevo
    Frames de configuracion path de:
    	-url bd
    	-url word space

[33mcommit 605a7fc8c5d046f37b0322c13a95453c84ea69b6[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 20 10:45:43 2016 -0500

    cambio de AreaOpciones.java jcombobox valor por defecto

[33mcommit 674962796fc1a9a37f409b99acf2846421f58e13[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Wed Apr 20 10:26:56 2016 -0500

    clase guardar solo guarde campos llenados

[33mcommit a29faccef92e839bbb677361b64b282493d399de[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Wed Apr 20 08:14:19 2016 -0500

    Arreglo de conflicto por falta de imagen

[33mcommit 29d609f96613658ae402d46222eaa6fb05f6b58f[m
Author: Multimedia <Multimedia@Admin>
Date:   Wed Apr 20 00:39:27 2016 -0500

    Creacion del sistema de ayuda de los campos

[33mcommit 67ea64d7e0dab1987cb751839bfe6a018972a6ec[m
Author: Multimedia <Multimedia@Admin>
Date:   Tue Apr 19 23:13:18 2016 -0500

    Creacion del sistema de ayuda de los campos, este archivo esta en modificacion debido a que se esta llenado la Informacion

[33mcommit 113a5f179e96ed69ed2c510007278ecd5cc0ecd9[m
Author: Multimedia <Multimedia@Admin>
Date:   Tue Apr 19 23:11:33 2016 -0500

    Creacion del sistema de ayuda de los campos

[33mcommit 855236862934403c86210915ef7776cd0b37fb54[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Tue Apr 19 19:13:17 2016 -0500

    Adición
    -Menú nuevo proyecto
    -Creador de carpetas según un código aleatorio falta crear conexión bd
    - Adición Json
    	-Opciones del tipo de proyecto
    	-Crear estructura de carpetas
    - Creación de clase utileriasLMS que pretende generar métodos para cosas simples para hacer más reutilizable el código

[33mcommit 6ea28eabc902b12d95ab95aa0e6fc84c46562de3[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Tue Apr 19 12:56:47 2016 -0500

    Creacion de clases cargar y guardar, y primera prueba, modificacion de los paneles para settear el dato

[33mcommit 9d08dd7487d8d3e556333fcfbb99978ec34166e9[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 18 21:58:34 2016 -0500

    Coorrigiendo el error

[33mcommit 91d108061ed9281635b57908278d60ba1ff8a5c6[m
Merge: 28c345b cd38044
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 18 18:48:18 2016 -0500

    arreglo conflicto gitignore

[33mcommit 28c345b9a6cd5be485d1b0c76e4b9f80d2692df8[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 18 18:22:24 2016 -0500

    Inorar bulid para carpetas

[33mcommit cd38044d0c52ae35fa2e8bd1f8d1658f5bac8639[m
Author: carlos Andres Orjuela lasso <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 18 23:02:56 2016 +0000

    ignora carpeta build

[33mcommit bfdca5db1e12effb0cb27343d87dba16d377f40a[m
Author: Andres Orjuela <lp.carlosorjuela@gmail.com>
Date:   Mon Apr 18 17:56:26 2016 -0500

    ignora carpeta build

[33mcommit 5eba4c1cd1c016e19d92ff483e6d994a052ef448[m
Author: Edilson Laverde Molina <edilsonlaverde182@gmail.com>
Date:   Mon Apr 18 12:14:21 2016 -0500

    Versión beta 0.9
